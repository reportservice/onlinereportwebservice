﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

/// <summary>
/// 代理报表实体类
/// </summary>
public class AgentReport
{
    /// <summary>
    /// 会员ID
    /// </summary>
    [Description("user_id")]
    public int UserId { get; set; }
    /// <summary>
    /// 会员账号称
    /// </summary>
    [Description("user_name")]
    public string UserName { get; set; }
    /// <summary>
    /// 会员类型（会员或代理）
    /// </summary>
    [Description("UserType")]
    public string UserType { get; set; }
    /// <summary>
    /// 投注人数
    /// </summary>
    [Description("BetNum")]
    public int BetNum { get; set; }
    /// <summary>
    /// 投注总额
    /// </summary>
    [Description("Out_BettingAccount")]
    public decimal BetMoney { get; set; }
    /// <summary>
    /// 中奖总额
    /// </summary>
    [Description("Out_WinningAccount")]
    public decimal WinMoney { get; set; }
    /// <summary>
    /// 返点总额
    /// </summary>
    [Description("Out_RebateAccount")]
    public decimal RebateMoney { get; set; }
    /// <summary>
    /// 优惠总额
    /// </summary>
    [Description("DiscountMoney")]
    public decimal DiscountMoney { get; set; }
    /// <summary>
    /// 盈利总额
    /// </summary>
    [Description("AgentProfitLoss")]
    public decimal ProfitMoney { get; set; }
    /// <summary>
    /// 是否代理
    /// </summary>
    [Description("is_agent")]
    public bool IsAgent { get; set; }
}