﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///Config 的摘要说明
/// </summary>
public class Config
{
    /// <summary>
    /// 数据库文件名
    /// </summary>
    public static string SQLiteDatabaseFile = "C:\\Windows\\SysWOW64\\SQLiteCachData.db";
    /// <summary>
    /// 连接数据库源字符串
    /// </summary>
    public static string SQLiteDataSource
    {
        get
        {
            return string.Format("data source={0};Pooling=true;FailIfMissing=false;read only=true;", SQLiteDatabaseFile);
        }
    }
}