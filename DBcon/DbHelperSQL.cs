using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Text;
using System.Diagnostics;

namespace DBcon
{
    /// <summary>
    /// Copyright (C) 2004-2008 LiTianPing 
    /// 数据访问基础类(基于SQLServer)
    /// 用户可以修改满足自己项目的需要。
    /// </summary>
    public abstract class DbHelperSQL
    {
     
        
        public static string connectionString_share = DBcontring.Report_Share;
        public static string connectionString = DBcontring.connectionString;
        public static string connectionString_bei = DBcon.DBcontring.connectionString_bei;
        public static string connectionString_switch = DBcon.DBcontring.connectionString_switch;

        public DbHelperSQL()
        {
        }

        #region 公用方法
        /// <summary>
        /// 切換備庫
        /// </summary>
        /// <param name="o_con">主要比對數據庫的sqlcon</param>
        /// <returns>是否切換</returns>
        private static bool DBSwitch(string o_con)
        {
             bool switchsignal = false;
            string DBsql = "select top 1 * from dt_affairs";
            if (GetQueryFromReport(DBsql, o_con, null) != null)
                switchsignal = true;
            return switchsignal;
        }
        

        /// <summary>
        /// 取代跨資料庫字段
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <returns></returns>
        private static string SqlstrReplace(string sqlstr)
        {
            sqlstr = sqlstr.Replace("[srv_lnk_total_in].[dafacloud].", "");
            sqlstr = sqlstr.Replace("[srv_lnk_total_out].[dafacloud].", "");
            sqlstr = sqlstr.Replace("[srv_lnk_total_order].[dafacloud].", "");
            sqlstr = sqlstr.Replace("[srv_lnk_agent_in].[dafacloud].", "");
            sqlstr = sqlstr.Replace("[srv_lnk_agent_out].[dafacloud].", "");
            sqlstr = sqlstr.Replace("[srv_lnk_agent_order].[dafacloud].", "");

            return sqlstr;
        }

        /// <summary>
        /// 後台分庫切換跨資料庫字段
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <returns></returns>
        private static string SqlstrReplaceForSwitchDB(string sqlstr, int DBtype)
        {
            if (DBtype == 2)
            {
                sqlstr = sqlstr.Replace("[srv_lnk_total_in]", "[srv_lnk_total_in2]");
                sqlstr = sqlstr.Replace("[srv_lnk_total_out]", "[srv_lnk_total_out2]");
                sqlstr = sqlstr.Replace("[srv_lnk_total_order]", "[srv_lnk_total_order2]");
                sqlstr = sqlstr.Replace("[srv_lnk_agent_in]", "[srv_lnk_agent_in2]");
                sqlstr = sqlstr.Replace("[srv_lnk_agent_out]", "[srv_lnk_agent_out2]");
                sqlstr = sqlstr.Replace("[srv_lnk_agent_order]", "[srv_lnk_agent_order2]");
                sqlstr = sqlstr.Replace("[dt_user_newpay]", "[dt_user_newpay2]");

            }

            return sqlstr;
        }


        //public static int GetMaxID(string FieldName, string TableName)
        //{
        //    string strsql = "select max(" + FieldName + ")+1 from " + TableName;
        //    object obj = GetSingle(strsql);
        //    if (obj == null)
        //    {
        //        return 1;
        //    }
        //    else
        //    {
        //        return int.Parse(obj.ToString());
        //    }
        //}
        //public static bool Exists(string strSql, params SqlParameter[] cmdParms)
        //{
        //    object obj = GetSingle(strSql, cmdParms);
        //    int cmdresult;
        //    if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
        //    {
        //        cmdresult = 0;
        //    }
        //    else
        //    {
        //        cmdresult = int.Parse(obj.ToString());
        //    }
        //    if (cmdresult == 0)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}
        #endregion

        #region  执行简单SQL语句
        /// <summary>
        /// 切換dbcon連線字串
        /// </summary>
        /// <param name="DBtype"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string SwitchConnStr(int DBtype, string conn)
        {
            string connStr = "";
            if (DBtype == 1)
            {
                switch (conn.ToLower())
                {
                    case "report_in_conn":
                        connStr = DBcontring.Report_In_Conn;
                        break;
                    case "report_out_conn":
                        connStr = DBcontring.Report_Out_Conn;
                        break;
                    case "report_order_conn":
                        connStr = DBcontring.Report_Order_Conn;
                        break;
                    case "agent_in_conn":
                        connStr = DBcontring.Agent_In_Conn;
                        break;
                    case "agent_out_conn":
                        connStr = DBcontring.Agent_Out_Conn;
                        break;
                    case "agent_order_conn":
                        connStr = DBcontring.Agent_Order_Conn;
                        break;
                    case "report_share":
                        connStr = DBcontring.Report_Share;
                        break;
                }
            }
            else if (DBtype == 2)
            {
                switch (conn)
                {
                    case "report_in_conn":
                        connStr = DBcontring.Report_In2_Conn;
                        break;
                    case "report_out_conn":
                        connStr = DBcontring.Report_Out2_Conn;
                        break;
                    case "report_order_conn":
                        connStr = DBcontring.Report_Order2_Conn;
                        break;
                    case "agent_in_conn":
                        connStr = DBcontring.Agent_In2_Conn;
                        break;
                    case "agent_out_conn":
                        connStr = DBcontring.Agent_Out2_Conn;
                        break;
                    case "agent_order_conn":
                        connStr = DBcontring.Agent_Order2_Conn;
                        break;
                    case "report_share":
                        connStr = DBcontring.Report_Share;
                        break;
                }
            }

            return connStr;
        }
        /// <summary>
        /// 判斷user_id切換identityid
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public static string SwitchIdentityid(int user_id)
        {
            int DBtype = 0;
            System.Collections.Generic.List<SqlParameter> listParms = new System.Collections.Generic.List<SqlParameter>();
            listParms.Add(new SqlParameter("@user_id", user_id));
            SqlParameter[] sqlcmdpar = listParms.ToArray();
            string sql = "select identityid from dt_users where id = @user_id";
            return GetQueryFromShare(sql, sqlcmdpar).Rows[0]["identityid"].ToString().TrimEnd();
        }


        public static DataTable GetQueryFromReportSwitchbei2(string ProName , int DBtype, string con, dspModel dspmodel)
        {
            Stopwatch st = new Stopwatch();
            st.Reset();
            st.Start();
            long part1Time = 0;
            long part2Time = 0;
            //切換分庫
            //sql = SqlstrReplaceForSwitchDB(sql, DBtype);
            //if (!DBSwitch(reportcon))
            //if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
            //if (reportcon == "Data Source=52.229.166.233;uid=reportreader;pwd=Report168169.;database=dafacloud;Connect Timeout=15000;" && DateTime.Now.DayOfWeek == DayOfWeek.Sunday && DateTime.Now.Hour >= 1 && DateTime.Now.Hour < 6)
            //switch(DBtype)
            //{
            //    //reportcon = connectionString_bei;
            //    //sql = SqlstrReplace(sql);
            //}

            string reportcon = SwitchConnStr(DBtype, con);

            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(reportcon);
            DataSet ds1 = new DataSet();
            StringBuilder errorMessages = new StringBuilder();

            
            for (int retryCount = 0; retryCount < 2; retryCount++)
            {

                try
                {
                    conn.Open();
                    st.Stop();
                    part1Time = st.ElapsedMilliseconds;
                    st.Reset();
                    st.Start();
                    //PrepareCommand(cmd, conn, null, sql, cmdParms);
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = ProName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@userInData", SqlDbType.Structured);
                    cmd.Parameters.Add("@identityid", SqlDbType.VarChar);
                    cmd.Parameters.Add("@begindate", SqlDbType.DateTime);
                    cmd.Parameters.Add("@enddate", SqlDbType.DateTime);
                    cmd.Parameters[0].Value = dspmodel.paramsDt;
                    cmd.Parameters[1].Value = dspmodel.identityid;
                    cmd.Parameters[2].Value = dspmodel.strStartDate;
                    cmd.Parameters[3].Value = dspmodel.strEndDate;
                    cmd.ExecuteNonQuery();
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        if (part1Time + part2Time > 2500)
                            DBcon.DbHelperSQL.CreateErrorLogTxt($"耗時:{st.ElapsedMilliseconds.ToString()}", ProName, reportcon, "TimeDelay");
                        cmd.Parameters.Clear();
                    }
                    return ds1.Tables[0];
                }
                catch (SqlException ex)
                {
                    int sqlErrorNumber = ex.Number;
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        string parstr = "identityid:" + dspmodel.identityid + "_" + "LoginId:" + dspmodel.LoginId + "_" + "paramsDtCount:" + dspmodel.paramsDt.Rows.Count + "_" + "strEndDate:" + dspmodel.strEndDate + "_" + "strStartDate:" + dspmodel.strStartDate;

                        errorMessages.Append("Index #" + i + "\n" +
                            "Message: " + ex.Errors[i].Message + "\n" + "\n" +
                            "Error Number: " + ex.Errors[i].Number + "\n" + "\n" +
                            "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                            "Source: " + ex.Errors[i].Source + "\n" +
                            "Procedure: " + ex.Errors[i].Procedure + "\n"+
                            "Param: "+ parstr);
                    }
                    DBcon.DbHelperSQL.CreateErrorLogTxt(errorMessages.ToString(), ProName, reportcon, "ErrorConn");
                    if (sqlErrorNumber != 41839)//指定交易依賴時重試
                    {
                        return ds1.Tables[0];
                    }
                    throw ex;
                }
                catch (Exception ex)
                {
                    DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), ProName, reportcon, "ErrorConn");
                    throw ex;
                }
                finally
                {

                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }
            }
            st.Stop();
            if(st.ElapsedMilliseconds>2500)
                DBcon.DbHelperSQL.CreateErrorLogTxt($"耗時:{st.ElapsedMilliseconds.ToString()}", ProName, reportcon,"TimeDelay");
            return ds1.Tables[0];

        }

        /// <summary>
        /// 對報表庫備庫切換查詢
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="reportcon"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromReportSwitchbei(string sql, int DBtype, string con, SqlParameter[] cmdParms)
        {

            Stopwatch st = new Stopwatch();
            st.Reset();
            st.Start();
            long part1Time = 0;
            long part2Time = 0;
            //切換分庫
            sql = SqlstrReplaceForSwitchDB(sql, DBtype);
            //if (!DBSwitch(reportcon))
            //if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
            //if (reportcon == "Data Source=52.229.166.233;uid=reportreader;pwd=Report168169.;database=dafacloud;Connect Timeout=15000;" && DateTime.Now.DayOfWeek == DayOfWeek.Sunday && DateTime.Now.Hour >= 1 && DateTime.Now.Hour < 6)
            //switch(DBtype)
            //{
            //    //reportcon = connectionString_bei;
            //    //sql = SqlstrReplace(sql);
            //}

            string reportcon = SwitchConnStr(DBtype, con);

            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(reportcon);
            DataSet ds1 = new DataSet();
            StringBuilder errorMessages = new StringBuilder();

         

            for (int retryCount = 0; retryCount < 2; retryCount++)
            {

                try
                {
                    conn.Open();
                    st.Stop();
                    part1Time = st.ElapsedMilliseconds;
                    st.Reset();
                    st.Start();
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, conn, null, sql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);

                        st.Stop();
                        part2Time = st.ElapsedMilliseconds;
                        if (part1Time + part2Time > 2500)
                            DBcon.DbHelperSQL.CreateErrorLogTxt($"耗時:{part1Time + part2Time}", sql, reportcon, "TimeDelay");
                        cmd.Parameters.Clear();
                    }
                    return ds1.Tables[0];
                }
                catch (SqlException ex)
                {
                    int sqlErrorNumber = ex.Number;
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages.Append("Index #" + i + "\n" +
                            "Message: " + ex.Errors[i].Message + "\n" + "\n" +
                            "Error Number: " + ex.Errors[i].Number + "\n" + "\n" +
                            "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                            "Source: " + ex.Errors[i].Source + "\n" +
                            "Procedure: " + ex.Errors[i].Procedure + "\n");
                    }
                    DBcon.DbHelperSQL.CreateErrorLogTxt(errorMessages.ToString(), sql, reportcon, "ErrorConn");
                    if (sqlErrorNumber != 41839)//指定交易依賴時重試
                    {
                        return ds1.Tables[0];
                    }
                    throw ex;
                }
                catch (Exception ex)
                {
                    DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), sql, reportcon, "ErrorConn");
                    throw ex;
                }
                finally
                {

                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }
            }
           
            return ds1.Tables[0];

        }

        /// <summary>
        /// 對報表庫查詢
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="reportcon"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromReportSwitchbei(string sql, string reportcon, SqlParameter[] cmdParms)
        {
            //切換備庫
            //if (!DBSwitch(reportcon))
            //if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
            //if (reportcon == "Data Source=52.229.166.233;uid=reportreader;pwd=Report168169.;database=dafacloud;Connect Timeout=15000;" && DateTime.Now.DayOfWeek == DayOfWeek.Sunday && DateTime.Now.Hour >= 1 && DateTime.Now.Hour < 6)
            //{
            //    reportcon = connectionString_bei;
            //    sql = SqlstrReplace(sql);
            //}
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(reportcon);
            //stsqlparms = cmdParms;
            DataSet ds1 = new DataSet();
            StringBuilder errorMessages = new StringBuilder();
            for (int retryCount = 0; retryCount < 2; retryCount++)
            {
                SqlCommand cmd = new SqlCommand();
                try
                {
                    conn.Open();

                    PrepareCommand(cmd, conn, null, sql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        cmd.Parameters.Clear();
                    }


                    return ds1.Tables[0];
                }
                catch (SqlException ex)
                {
                    int sqlErrorNumber = ex.Number;
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages.Append("Index #" + i + "\n" +
                            "Message: " + ex.Errors[i].Message + "\n" + "\n" +
                            "Error Number: " + ex.Errors[i].Number + "\n" + "\n" +
                            "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                            "Source: " + ex.Errors[i].Source + "\n" +
                            "Procedure: " + ex.Errors[i].Procedure + "\n");
                    }
                    DBcon.DbHelperSQL.CreateErrorLogTxt(errorMessages.ToString(), sql, reportcon, "ErrorConn");
                    if (sqlErrorNumber != 41839)//指定交易依賴時重試
                    {
                        return ds1.Tables[0];
                    }
                    throw ex;
                }
                catch (Exception ex)
                {
                    DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), sql, reportcon, "ErrorConn");

                    throw ex;
                }
                finally
                {
                    cmd.Parameters.Clear();
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }

                }
            }

            return ds1.Tables[0];
        }

        /// <summary>
        /// 對Share查詢
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="reportcon"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromShare(string sql, SqlParameter[] cmdParms)
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString_share);
            Stopwatch st = new Stopwatch();
            st.Reset();
            st.Start();
            long part1Time = 0;
            long part2Time = 0;
            DataSet ds1 = new DataSet();
            StringBuilder errorMessages = new StringBuilder();
            for (int retryCount = 0; retryCount < 2; retryCount++)
            {
                try
                {
                    st.Stop();
                    part1Time = st.ElapsedMilliseconds;
                    st.Reset();
                    st.Start();
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, conn, null, sql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        st.Stop();
                        part2Time = st.ElapsedMilliseconds;
                        if (part1Time + part2Time > 2500)
                            DBcon.DbHelperSQL.CreateErrorLogTxt($"耗時:{st.ElapsedMilliseconds.ToString()}", sql, connectionString_share, "TimeDelay");
                        cmd.Parameters.Clear();
                    }
                    return ds1.Tables[0];
                }
                catch (SqlException ex)
                {
                    int sqlErrorNumber = ex.Number;
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages.Append("Index #" + i + "\n" +
                            "Message: " + ex.Errors[i].Message + "\n" + "\n" +
                            "Error Number: " + ex.Errors[i].Number + "\n" + "\n" +
                            "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                            "Source: " + ex.Errors[i].Source + "\n" +
                            "Procedure: " + ex.Errors[i].Procedure + "\n");
                    }
                    DBcon.DbHelperSQL.CreateErrorLogTxt(errorMessages.ToString(), sql, connectionString_share, "ErrorConn");
                    if (sqlErrorNumber != 41839)//指定交易依賴時重試
                    {
                        return ds1.Tables[0];
                    }
                    throw ex;
                }
                catch (Exception ex)
                {
                    DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), sql, connectionString_share, "ErrorConn");
                    throw ex;
                }
                finally
                {

                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }
           
            }
            return ds1.Tables[0];

        }


        /// <summary>
        /// 對Share查詢
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="reportcon"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromShare(string sql, SqlParameter[] cmdParms, int DBtype)
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString_share);

            sql = SqlstrReplaceForSwitchDB(sql, DBtype);

            DataSet ds1 = new DataSet();
            StringBuilder errorMessages = new StringBuilder();
            for (int retryCount = 0; retryCount < 2; retryCount++)
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, conn, null, sql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        cmd.Parameters.Clear();
                    }
                    return ds1.Tables[0];
                }
                catch (SqlException ex)
                {
                    int sqlErrorNumber = ex.Number;
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages.Append("Index #" + i + "\n" +
                            "Message: " + ex.Errors[i].Message + "\n" + "\n" +
                            "Error Number: " + ex.Errors[i].Number + "\n" + "\n" +
                            "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                            "Source: " + ex.Errors[i].Source + "\n" +
                            "Procedure: " + ex.Errors[i].Procedure + "\n");
                    }
                    DBcon.DbHelperSQL.CreateErrorLogTxt(errorMessages.ToString(), sql, connectionString_share, "ErrorConn");
                    if (sqlErrorNumber != 41839)//指定交易依賴時重試
                    {
                        return ds1.Tables[0];
                    }
                    throw ex;
                }
                catch (Exception ex)
                {
                    DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), sql, connectionString_share, "ErrorConn");
                    throw ex;
                }
                finally
                {

                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }

            }
            return ds1.Tables[0];

        }

        /// <summary>
        /// 對報表單一庫
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="reportcon"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        private static DataTable GetQueryFromReport(string sql, string reportcon, SqlParameter[] cmdParms)
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(reportcon);
            DataSet ds1 = new DataSet();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 50;
                PrepareCommand(cmd, conn, null, sql, cmdParms);
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    sda.Fill(ds1);
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), sql, reportcon, "ErrorConn");
                throw ex;
            }
            finally
            {

                if (conn.State == ConnectionState.Open)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }
            return ds1.Tables[0];
        }

        /// <summary>
        /// 對報表庫查詢無timeout
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="reportcon"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromReportLong(string sql, string reportcon, SqlParameter[] cmdParms)
        {

            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(reportcon);
            DataSet ds1 = new DataSet();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 0;
                PrepareCommand(cmd, conn, null, sql, cmdParms);
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    sda.Fill(ds1);
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                DBcon.DbHelperSQL.CreateErrorLogTxt(ex.ToString(), sql, reportcon, "ErrorConn");
                throw ex;
            }
            finally
            {

                if (conn.State == ConnectionState.Open)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }
            return ds1.Tables[0];
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException E)
                    {
                        connection.Close();
                        throw new Exception(E.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static void ExecuteSqlTran(ArrayList SQLStringList)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                SqlTransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < SQLStringList.Count; n++)
                    {
                        string strsql = SQLStringList[n].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    tx.Commit();

                }
                catch (System.Data.SqlClient.SqlException E)
                {
                    tx.Rollback();
                    throw new Exception(E.Message);
                }
            }
        }
        /// <summary>
        /// 执行带一个存储过程参数的的SQL语句。
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <param name="content">参数内容,比如一个字段是格式复杂的文章，有特殊符号，可以通过这个方式添加</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, string content)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(SQLString, connection);
                System.Data.SqlClient.SqlParameter myParameter = new System.Data.SqlClient.SqlParameter("@content", SqlDbType.NText);
                myParameter.Value = content;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (System.Data.SqlClient.SqlException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// 向数据库里插入图像格式的字段(和上面情况类似的另一种实例)
        /// </summary>
        /// <param name="strSQL">SQL语句</param>
        /// <param name="fs">图像字节,数据库的字段类型为image的情况</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSqlInsertImg(string strSQL, byte[] fs)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(strSQL, connection);
                System.Data.SqlClient.SqlParameter myParameter = new System.Data.SqlClient.SqlParameter("@fs", SqlDbType.Image);
                myParameter.Value = fs;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (System.Data.SqlClient.SqlException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        connection.Close();
                        throw new Exception(e.Message);
                    }
                }
            }
        }
        /// <summary>
        /// 执行查询语句，返回SqlDataReader
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string strSQL)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(strSQL, connection);
            try
            {
                connection.Open();
                SqlDataReader myReader = cmd.ExecuteReader();
                return myReader;
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                throw new Exception(e.Message);
            }

        }
        /// <summary>
        /// 對叢庫查詢
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromCongku(string sql, int DBtype, params SqlParameter[] cmdParms)
        {
            string conn = connectionString;
            if (DBtype == 1)
                conn = connectionString;
            else if (DBtype == 2)
                conn = connectionString_switch;

            SqlConnection aliconn = new SqlConnection(conn);
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                aliconn.Open();
                PrepareCommand(cmd, aliconn, null, sql, cmdParms);
                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
                aliconn.Close();
                cmd.Parameters.Clear();
                return dt;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (aliconn.State == ConnectionState.Open)
                {
                    aliconn.Dispose();
                    aliconn.Close();
                }
            }
        }


        #endregion

        #region 执行带参数的SQL语句

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException E)
                    {
                        throw new Exception(E.Message);
                    }
                }
            }
        }


        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">SQL语句的哈希表（key为sql语句，value是该语句的SqlParameter[]）</param>
        public static void ExecuteSqlTran(Hashtable SQLStringList)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlTransaction trans = conn.BeginTransaction())
                {
                    SqlCommand cmd = new SqlCommand();
                    try
                    {
                        //循环
                        foreach (DictionaryEntry myDE in SQLStringList)
                        {
                            string cmdText = myDE.Key.ToString();
                            SqlParameter[] cmdParms = (SqlParameter[])myDE.Value;
                            PrepareCommand(cmd, conn, trans, cmdText, cmdParms);
                            int val = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            trans.Commit();
                        }
                    }
                    catch
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }


        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        throw new Exception(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回SqlDataReader
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string SQLString, params SqlParameter[] cmdParms)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            try
            {
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                SqlDataReader myReader = cmd.ExecuteReader();
                cmd.Parameters.Clear();
                return myReader;
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                throw new Exception(e.Message);
            }

        }

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    return ds;
                }
            }
        }


        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

        #endregion

        #region 存储过程操作

        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader RunProcedure(string storedProcName, IDataParameter[] parameters)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataReader returnReader;
            connection.Open();
            SqlCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.CommandType = CommandType.StoredProcedure;
            returnReader = command.ExecuteReader();
            return returnReader;
        }


        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="tableName">DataSet结果中的表名</param>
        /// <returns>DataSet</returns>
        public static DataSet RunProcedure(string storedProcName, IDataParameter[] parameters, string tableName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                SqlDataAdapter sqlDA = new SqlDataAdapter();
                sqlDA.SelectCommand = BuildQueryCommand(connection, storedProcName, parameters);
                sqlDA.Fill(dataSet, tableName);
                connection.Close();
                return dataSet;
            }
        }


        /// <summary>
        /// 构建 SqlCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlCommand</returns>
        private static SqlCommand BuildQueryCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            return command;
        }

        /// <summary>
        /// 执行存储过程，返回影响的行数		
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="rowsAffected">影响的行数</param>
        /// <returns></returns>
        public static int RunProcedure(string storedProcName, IDataParameter[] parameters, out int rowsAffected)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                int result;
                connection.Open();
                SqlCommand command = BuildIntCommand(connection, storedProcName, parameters);
                rowsAffected = command.ExecuteNonQuery();
                result = (int)command.Parameters["ReturnValue"].Value;
                //Connection.Close();
                return result;
            }
        }

        /// <summary>
        /// 创建 SqlCommand 对象实例(用来返回一个整数值)	
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlCommand 对象实例</returns>
        private static SqlCommand BuildIntCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.Parameters.Add(new SqlParameter("ReturnValue",
                SqlDbType.Int, 4, ParameterDirection.ReturnValue,
                false, 0, 0, string.Empty, DataRowVersion.Default, null));
            return command;
        }
        #endregion

        /// <summary>
        /// 创建错误日志
        /// </summary>
        /// <param name="path">日志地址 在当前工作目录+ErrorLog下</param>
        /// <param name="strPort">接口</param>
        /// <param name="strCostTime">響應耗時</param>
        /// <param name="strPara">參數內容</param>
        public static void CreateErrorLogTxt(string ex, string sql, string conn,string title)
        {
            string strPath; //错误文件的路径
            DateTime dt = DateTime.Now;
            try
            {
                //工程目录下 创建日志文件夹 
                //strPath = Thread.GetDomain().BaseDirectory + "\\RESPONSETIME\\ErrorConn";
                strPath = $"D:\\webService\\RESPONSETIME\\{title}";
                if (Directory.Exists(strPath) == false) //工程目录下 Log目录 '目录是否存在,为true则没有此目录
                {
                    Directory.CreateDirectory(strPath); //建立目录　Directory为目录对象
                }
                strPath = strPath + "\\" + dt.ToString("yyyyMM");

                if (Directory.Exists(strPath) == false) //目录是否存在  '工程目录下 Log\月 目录   yyyymm
                {
                    Directory.CreateDirectory(strPath); //建立目录//日志文件，以 日 命名 
                }
                strPath = strPath + "\\" + dt.ToString("dd") + ".log";
                StreamWriter fileWriter = new StreamWriter(strPath, true); //创建日志文件
                fileWriter.WriteLine("錯誤訊息: " + ex);
                fileWriter.WriteLine("觸發時間: " + dt.ToString("HH:mm:ss.fff"));
                fileWriter.WriteLine(" 連線字串: " + conn);
                fileWriter.WriteLine(" 參數內容: " + sql);
                fileWriter.WriteLine("");
                fileWriter.Close(); //关闭StreamWriter对象
            }
            catch (Exception e)
            {
                sql += "--" + e.Message;
            }
        }
    }
}
