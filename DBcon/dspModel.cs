﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DBcon
{
    public class dspModel
    {
        public DataTable paramsDt { get; set; }

        public string identityid { get; set; }

        public DateTime strStartDate { get; set; }

        public DateTime strEndDate { get; set; }

        public int LoginId { get; set; }
    }
}
