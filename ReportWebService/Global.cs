﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using ServiceStack.Redis;
using System.IO;
using System.Threading;
using System.Text;
using System.Linq.Expressions;
using EasyFrame.Core;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;



namespace EasyFrame.Web
{
    public class Global : System.Web.HttpApplication
    {
        private static PooledRedisClientManager _pooledRedisClientManager = null;
        public static PooledRedisClientManager PooledRedisClientManager
        {
            get
            {
                if (_pooledRedisClientManager == null)
                {
                    _pooledRedisClientManager = RedisManager.CreateManager(ListHost, ListHost, 2, 2);
                }
                return _pooledRedisClientManager;
            }
        }
        private static List<string> ListHost
        {
            get
            {
                //string item = "dafarpass168@before.dafaesb.com:6379";
                string item = "YsdIweCnHtl/SjNdpzp47DtNvPdSHb3vcpeE2UffuqU=@beforeredis.redis.cache.windows.net:6379";
                return new List<string>
                {
                    item
                };
            }
        }
        public static class RedisManager
        {
            //private static string ServiceStackLicenseKey = "4228-e1JlZjo0MjI4LE5hbWU6ZGFmYSxUeXBlOkJ1c2luZXNzLEhhc2g6YkJKUXFlL1BJdmt5dUU4dTJXbVhmc3ZpTXQwU1I1V2U0bnBmSHVHVjN5UXBpMFZoSkJyQUg3OU5HTG5rU2t0NGJHUFd6RVhHaFI4QVpZMG9GVm9lV29ia1ZIRkdncGs4WlJhekZyajU0K0wrditUSnRFQkZHQ0JBWWJzMGFBSlViWkZnOXJjMmZGLzE4eUxRcWN1eFZRZ2Y4QnBVUXNEVjA5NC9RYVJOT0hZPSxFeHBpcnk6MjAxNy0wOS0zMH0 =";

            public static PooledRedisClientManager CreateManager(List<string> ReadServerList, List<string> WriteServerList, int iMaxWritePoolSize, int iMaxReadPoolSize)
            {
                ServiceStack.Licensing.RegisterLicense(rediscon.ServiceStackLicenseKey);
                return new PooledRedisClientManager(ReadServerList.ToArray(), WriteServerList.ToArray(), new RedisClientManagerConfig
                {
                    MaxWritePoolSize = iMaxWritePoolSize,
                    MaxReadPoolSize = iMaxReadPoolSize,
                    AutoStart = true
                });
            }

            private static string[] SplitString(string strSource, string split)
            {
                return strSource.Split(split.ToArray<char>());
            }
        }
        public static RedisClient redisClient
        {
            get
            {
                RedisClient redisClient = (RedisClient)PooledRedisClientManager.GetClient();
                redisClient.Db = 0L;
                return redisClient;
            }
        }
        public static RedisClient redisClientdb4
        {
            get
            {
                RedisClient redisClientdb4 = (RedisClient)PooledRedisClientManager.GetClient();
                redisClientdb4.Db = 4L;
                return redisClientdb4;
            }
        }
        System.Timers.Timer cleat_timer = new System.Timers.Timer();
        System.Timers.Timer total_online_timer = new System.Timers.Timer();
        System.Timers.Timer timer_inout = new System.Timers.Timer();
        System.Timers.Timer timer_newpay = new System.Timers.Timer();
        System.Timers.Timer clear_timer = new System.Timers.Timer();
        System.Timers.Timer profit_timer = new System.Timers.Timer();
        System.Timers.Timer tenant_timer = new System.Timers.Timer();

        protected void Application_Start(object sender, EventArgs e)
        {

            AddTenant();
            //MappingStatic();
            //獲取分庫資訊
            tenant_timer.AutoReset = false;
            tenant_timer.Interval = 1000.0;
            tenant_timer.Elapsed += new System.Timers.ElapsedEventHandler(tenant_Elapsed);
            tenant_timer.Start();

            //昨日盈利榜
            profit_timer.AutoReset = false;
            profit_timer.Interval = 1000.0;
            profit_timer.Elapsed += new System.Timers.ElapsedEventHandler(ProFit_Elapsed);
            //profit_timer.Start();

            //在線人數
            total_online_timer.AutoReset = false;
            total_online_timer.Interval = 5000.0;
            total_online_timer.Elapsed += new System.Timers.ElapsedEventHandler(total_online);
            total_online_timer.Start();

            //修改in狀態
            timer_inout.AutoReset = false;
            timer_inout.Interval = 10000;
            timer_inout.Elapsed += new System.Timers.ElapsedEventHandler(InOutUpdate_Elapsed);
            timer_inout.Start();

            //修改首充報表狀態
            timer_newpay.AutoReset = false;
            timer_newpay.Interval = 600000;
            timer_newpay.Elapsed += new System.Timers.ElapsedEventHandler(NewpayUpdate_Elapsed);
            timer_newpay.Start();

            //清理redis db0
            cleat_timer.AutoReset = false;
            cleat_timer.Interval = 1500.0;
            cleat_timer.Elapsed += new System.Timers.ElapsedEventHandler(clear_redis);
            cleat_timer.Start();

            //清理redis db4
            clear_timer.AutoReset = false;
            clear_timer.Interval = 1500.0;
            clear_timer.Elapsed += new System.Timers.ElapsedEventHandler(clear_redis4);
            clear_timer.Start();



        }

        private void tenant_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(System.DateTime.Now.ToString("HH:mm") == "12:00")
            {
                try
                {
                    AddTenant();

                }
                catch (Exception)
                {

                }
            }
            
        }
        //獲取分庫資訊
        public static void AddTenant()
        {
            ReportHelper reporthelper = new ReportHelper();
            dt_tenantDB TenantDB = new dt_tenantDB();
            List<dt_tenantDB> listTenantDB = new List<dt_tenantDB>();

            //string sql_str = "select * from dt_tenantDB order by identityid asc";
            string sql_str = "select id,identityid,DBtype from dt_tenant where DBtype in (1,2) order by identityid asc";
            DataTable dt_tenant = DBcon.DbHelperSQL.GetQueryFromShare(sql_str, null);

            reporthelper.ConvertToMode<dt_tenantDB>(dt_tenant, TenantDB, listTenantDB);
            int count = listTenantDB.Count;
            foreach (dt_tenantDB DB in listTenantDB)
            {
                string key = DB.identityid;
                int type = DB.DBtype != 0 ? DB.DBtype : 1;
                //int type = 1;
                if (!Flash_Dic.dic_tenant.ContainsKey(key))
                    Flash_Dic.dic_tenant.Add(key, type);
                else
                    Flash_Dic.dic_tenant[key] = type;
            }
            //Flash_Dic.dic_tenant.Add("test03142", 2);

        }

        private void MappingStatic()
        {
            int x = 0;
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    x = i;
                    DateTime star = System.DateTime.Now.AddDays(-7 + i);
                    DateTime end = System.DateTime.Now.AddDays(-7 + i);
                    string sql_str = "select * FROM dt_lottery_record where add_date>='" + star + "' and add_date<='" + end + "'";
                    if (i == 0)
                    {
                        StaticDt.dtOrderRecord = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, 1, "report_order_conn", null);
                    }
                    else
                    {
                        DataTable dt = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, 1, "report_order_conn", null);
                        StaticDt.dtOrderRecord.Merge(dt.Copy());
                    }

                }
                for (int i = 0; i < 7; i++)
                {
                    x = i;
                    DateTime star = System.DateTime.Now.AddDays(-7 + i);
                    DateTime end = System.DateTime.Now.AddDays(-7 + i);
                    string sql_str = "select * FROM dt_lottery_record where add_date>='" + star + "' and add_date<='" + end + "'";
                    if (i == 0)
                    {
                        StaticDt.dtInRecord = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, 1, "report_order_conn", null);
                    }
                    else
                    {
                        DataTable dt = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(sql_str, 1, "report_order_conn", null);
                        StaticDt.dtInRecord.Merge(dt.Copy());
                    }

                }
            }
            catch(Exception e)
            {
               int sss =  x;
            }
            


        }

        private void rpt_cold_session(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //宣告七個報表冷資料模型

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 在線人數緩存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void total_online(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                var redisOnlineClientConfig = GlobalVariable.onlineRedisClient;
                var onlineList = redisOnlineClientConfig.GetAll<EasyFrame.NewCommon.Model.dt_sys_online>();
                Dictionary<string, int> dic_identityid = new Dictionary<string, int>();
                foreach (EasyFrame.NewCommon.Model.dt_sys_online online in onlineList)
                {
                    if (!dic_identityid.ContainsKey(online.identityid))
                    {
                        if ((DateTime.Now - online.oper_time).TotalMinutes <= 5)
                        {
                            dic_identityid.Add(online.identityid, 1);
                        }
                    }
                    else
                    {
                        if ((DateTime.Now - online.oper_time).TotalMinutes <= 5)
                        {
                            int o_count = dic_identityid[online.identityid];
                            dic_identityid[online.identityid] = o_count + 1;
                        }
                    }
                }
                Flash_Dic.verdic = dic_identityid;
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                total_online_timer.Start();
            }
        }
        private void ProFit_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            LogMsg logmsg = new LogMsg();
            Stopwatch st = new Stopwatch();
            string para = "Date=" + System.DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            try
            {
                DateTime s = DateTime.Now;
                if (Flash_Dic.gdt_ProfitLossTop != null && Flash_Dic.gdt_ProfitLossTop.Rows.Count > 0)
                {
                    string date = System.DateTime.Now.ToString("yyyy-MM-dd");
                    if ((System.DateTime.Now >= Convert.ToDateTime(date + " 00:01") && System.DateTime.Now <= Convert.ToDateTime(date + " 00:10")) || System.DateTime.Now.ToString("HH:mm") == "04:00")
                    {
                        st.Reset();
                        st.Start();
                        updateProfit();
                        st.Stop();
                        lock (logmsg)
                        {
                            logmsg.CreateErrorLogTxt("After_Global", "ProFit", st.ElapsedMilliseconds.ToString(), para);
                        }
                    }
                }
                else
                {
                    st.Reset();
                    st.Start();
                    updateProfit();
                    st.Stop();
                    lock (logmsg)
                    {
                        logmsg.CreateErrorLogTxt("After_Global", "ProFit", st.ElapsedMilliseconds.ToString(), para);
                    }
                }

            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {
                lock (logmsg)
                {
                    st.Stop();
                    para += " \r\n錯誤訊息:" + ex2;
                    logmsg.CreateErrorLogTxt("After_Error", "Global", st.ElapsedMilliseconds.ToString(), para);
                }
            }
            finally
            {
                profit_timer.Start();
            }
        }
        private void updateProfit()
        {
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_order2_conn = DBcon.DBcontring.Report_Order2_Conn;
            List<SqlParameter> AliSqlParamter = new List<SqlParameter>();
            SqlParameter alipa_begindate = new SqlParameter("@strStartDate", SqlDbType.DateTime);
            alipa_begindate.Value = System.DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            AliSqlParamter.Add(alipa_begindate);

            string instr = "select top 10 v_order.identityid as IdentityId,v_order.user_id as UserId,v_order.lottery_in as BetMoney,v_win.lottery_out AS win , isnull(RebateMoney,0)RebateMoney, isnull(DiscountMoney,0)DiscountMoney, isnull(out_money,0)out_money,"
                         + " (v_win.lottery_out - v_order.lottery_in + isnull(RebateMoney, 0) + isnull(DiscountMoney, 0) - isnull(out_money, 0)) as WinMoney "
                         + " from "
                         + " (select identityid, user_id, isnull(sum(normal_money), 0) as 'lottery_in' from[dbo].[dt_lottery_record] where add_date = @strStartDate   and identityid <> 'test'  group by identityid, user_id) "
                         + " as v_order "
                         + "   left join "
                         + " (select identityid, user_id , isnull(sum(money),0) as 'lottery_out' from srv_lnk_total_in.dafacloud.dbo.dt_lottery_winning_userrecord with(nolock) where add_date = @strStartDate  and identityid<>'test'  group by identityid,user_id )  "
                         + " as v_win "
                         + "   on v_order.identityid = v_win.identityid and v_order.user_id = v_win.user_id "
                         + "    left join "
                         + "   (select identityid, user_id , isnull(sum(money),0) as RebateMoney from srv_lnk_total_in.dafacloud.dbo.dt_user_get_point_userrecord   where add_date = @strStartDate    and identityid<>'test'   group by identityid,user_id ) "
                         + " as v_point "
                         + "   on v_order.identityid = v_point.identityid and v_order.user_id = v_point.user_id "
                         + "   left join "
                         + "   (select identityid, user_id , isnull(sum(money),0) as DiscountMoney from srv_lnk_total_in.dafacloud.dbo.dt_user_in_record  where add_date = @strStartDate   and type2 in (7, 12, 13, 14)  and identityid<>'test'   group by identityid,user_id) "
                         + " as v_in "
                         + "   on v_order.identityid = v_in.identityid and v_order.user_id = v_in.user_id "
                         + "   left join "
                         + "   (select identityid, user_id, isnull(sum(money),0) as out_money from srv_lnk_total_out.dafacloud.dbo.dt_user_out_record  where add_date = @strStartDate    and identityid<>'test' and((type2 = 10 and state = 1) or(type2 = 3 and state = 3))   group by identityid,user_id ) "
                         + " as v_out "
                         + "    on v_order.identityid = v_out.identityid and v_order.user_id = v_out.user_id "
                         + "    where(v_win.lottery_out - v_order.lottery_in + isnull(RebateMoney, 0) + isnull(DiscountMoney, 0) - isnull(out_money, 0)) > 0  ORDER BY(v_win.lottery_out -v_order.lottery_in + isnull(RebateMoney, 0) + isnull(DiscountMoney, 0) - isnull(out_money, 0)) DESC";
            DataTable DT1 = DBcon.DbHelperSQL.GetQueryFromReportLong(instr, report_order_conn, AliSqlParamter.ToArray());
            //分庫
            string instr2 = "select top 10 v_order.identityid as IdentityId,v_order.user_id as UserId,v_order.lottery_in as BetMoney,v_win.lottery_out AS win , isnull(RebateMoney,0)RebateMoney, isnull(DiscountMoney,0)DiscountMoney, isnull(out_money,0)out_money,"
                        + " (v_win.lottery_out - v_order.lottery_in + isnull(RebateMoney, 0) + isnull(DiscountMoney, 0) - isnull(out_money, 0)) as WinMoney "
                        + " from "
                        + " (select identityid, user_id, isnull(sum(normal_money), 0) as 'lottery_in' from[dbo].[dt_lottery_record] where add_date = @strStartDate   and identityid <> 'test'  group by identityid, user_id) "
                        + " as v_order "
                        + "   left join "
                        + " (select identityid, user_id , isnull(sum(money),0) as 'lottery_out' from srv_lnk_total_in.dafacloud.dbo.dt_lottery_winning_userrecord with(nolock) where add_date = @strStartDate  and identityid<>'test'  group by identityid,user_id )  "
                        + " as v_win "
                        + "   on v_order.identityid = v_win.identityid and v_order.user_id = v_win.user_id "
                        + "    left join "
                        + "   (select identityid, user_id , isnull(sum(money),0) as RebateMoney from srv_lnk_total_in.dafacloud.dbo.dt_user_get_point_userrecord   where add_date = @strStartDate    and identityid<>'test'   group by identityid,user_id ) "
                        + " as v_point "
                        + "   on v_order.identityid = v_point.identityid and v_order.user_id = v_point.user_id "
                        + "   left join "
                        + "   (select identityid, user_id , isnull(sum(money),0) as DiscountMoney from srv_lnk_total_in.dafacloud.dbo.dt_user_in_record  where add_date = @strStartDate   and type2 in (7, 12, 13, 14)  and identityid<>'test'   group by identityid,user_id) "
                        + " as v_in "
                        + "   on v_order.identityid = v_in.identityid and v_order.user_id = v_in.user_id "
                        + "   left join "
                        + "   (select identityid, user_id, isnull(sum(money),0) as out_money from srv_lnk_total_out.dafacloud.dbo.dt_user_out_record  where add_date = @strStartDate    and identityid<>'test' and((type2 = 10 and state = 1) or(type2 = 3 and state = 3))   group by identityid,user_id ) "
                        + " as v_out "
                        + "    on v_order.identityid = v_out.identityid and v_order.user_id = v_out.user_id "
                        + "    where(v_win.lottery_out - v_order.lottery_in + isnull(RebateMoney, 0) + isnull(DiscountMoney, 0) - isnull(out_money, 0)) > 0  ORDER BY(v_win.lottery_out -v_order.lottery_in + isnull(RebateMoney, 0) + isnull(DiscountMoney, 0) - isnull(out_money, 0)) DESC";
            DataTable DT2 = DBcon.DbHelperSQL.GetQueryFromReportLong(instr2, report_order2_conn, AliSqlParamter.ToArray());

            DT1.Merge(DT2);
            DT1.DefaultView.Sort = "WinMoney DESC";
            DataTable dytop10 = DT1.Rows.Cast<DataRow>().Take(10).CopyToDataTable();
            Flash_Dic.gdt_ProfitLossTop = dytop10;
        }

        private void InOutUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("HH:mm") == "12:00" || System.DateTime.Now.ToString("HH:mm") == "00:01" || System.DateTime.Now.ToString("HH:mm") == "00:02")
                {
                    string update_in_str = "";
                    List<SqlParameter> ListPar = new List<SqlParameter>();
                    SqlParameter DateParmeter = new SqlParameter("@add_time", System.Data.SqlDbType.DateTime2);
                    DateParmeter.Value = System.DateTime.Now.AddHours(-12).ToString("yyyy-MM-dd HH:mm:ss");
                    ListPar.Add(DateParmeter);

                    List<SqlParameter> ListPar24 = new List<SqlParameter>();
                    SqlParameter DateParmeter24 = new SqlParameter("@add_time", System.Data.SqlDbType.DateTime2);
                    DateParmeter24.Value =  System.DateTime.Now.AddHours(-24).ToString("yyyy-MM-dd HH:mm:ss");
                    ListPar24.Add(DateParmeter24);

                    string msg = "";
                    msg += "修改dt_user_fastpay_record:";
                    //修改dt_user_fastpay_record
                    update_in_str = "update dt_user_fastpay_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; 修改dt_user_bankpay_record:";
                    //修改dt_user_bankpay_record
                    update_in_str = "update dt_user_bankpay_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; 修改dt_user_alipay_record:";
                    //修改dt_user_alipay_record
                    update_in_str = "update dt_user_alipay_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; 修改dt_user_weixin_record:";
                    //修改dt_user_weixin_record
                    update_in_str = "update dt_user_weixin_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; 修改dt_user_qqpay_record:";
                    //修改dt_user_qqpay_record
                    update_in_str = "update dt_user_qqpay_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; dt_user_unionpay_record:";
                    // dt_user_unionpay_record
                    update_in_str = "update dt_user_unionpay_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; dt_user_four_record:";
                    // dt_user_four_record
                    update_in_str = "update dt_user_four_record set [State]=2 where [State] in (0,3) and AddTime<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; 修改dt_user_in_account:";
                    //修改dt_user_in_account
                    update_in_str = "update dt_user_in_account set [State]=2 where [Type]=1 and [State] in (0,3) and add_time2<@add_time;";
                    msg += SqlHelper.QueryFromSwitch(update_in_str, ListPar.ToArray());
                    msg += "筆; 退回:";
                    string draw_str = "select identityid,user_id,record_code from dt_user_draw_money_record where [State]=0 and add_time<@add_time;";
                    //string out_str = "select identityid,user_id,record_code from dt_user_out_account  where [Type]=3 and [State]=0 and add_time2<dateadd(HOUR,-24,dbo.fn_GetlocalDate())";

                    foreach (var dic in Flash_Dic.dic_tenant.Values.Distinct())
                    {
                        int DBtype = dic;
                        System.Data.DataTable dt_user_draw_money_record = SqlHelper.GetQueryFromLocalData(draw_str, DBtype, ListPar24.ToArray());
                        int draw_count = dt_user_draw_money_record.Rows.Count;
                        for (int i = 0; i < draw_count; i++)
                        {
                            string record_code = dt_user_draw_money_record.Rows[i]["record_code"].ToString().TrimEnd();
                            string user_id = dt_user_draw_money_record.Rows[i]["user_id"].ToString().TrimEnd();
                            string identityid = dt_user_draw_money_record.Rows[i]["identityid"].ToString().TrimEnd();
                            int result = SqlHelper.RunInsert(DBtype, record_code, user_id, identityid, "dsp_after_Account_OutCancel");
                            msg += "[" + record_code + ";" + user_id + ";" + identityid + "]";
                        }
                    }


                    LogPrint(msg, "UPDATElog.txt");
                }
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_inout.Start();
            }

        }
        private void NewpayUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string newpay_str = "";
                string in_str = "";
                string update_str = "";
                List<SqlParameter> ListPar = new List<SqlParameter>();
                SqlParameter DateParmeter = new SqlParameter("@add_time", System.Data.SqlDbType.DateTime2);
                DateParmeter.Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                ListPar.Add(DateParmeter);

                string userid = "";
                string identityid = "";
                string id = "";
                string msg = "";
                msg += "修改newpay:";

                foreach (var dic in Flash_Dic.dic_tenant.Values.Distinct())
                {
                    int DBtype = dic;
                    //修改dt_user_newpay
                    newpay_str = "select id,identityid,userid from dt_user_newpay  where  AddTime>@add_time and money = 0 OPTION (MAXDOP 1) ;";
                    System.Data.DataTable dt_user_newpay = SqlHelper.GetQueryFromLocalData(newpay_str, DBtype, ListPar.ToArray());

                    int count = dt_user_newpay.Rows.Count;
                    if (count > 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            userid = dt_user_newpay.Rows[i]["userid"].ToString().TrimEnd();
                            identityid = dt_user_newpay.Rows[i]["identityid"].ToString().TrimEnd();
                            id = dt_user_newpay.Rows[i]["id"].ToString().TrimEnd();

                            List<SqlParameter> ListParIn = new List<SqlParameter>();
                            SqlParameter IdentityidParmeter = new SqlParameter("@identityid", System.Data.SqlDbType.VarChar, 10);
                            IdentityidParmeter.Value = identityid;
                            SqlParameter UseridParmeter = new SqlParameter("@user_id", System.Data.SqlDbType.Int);
                            UseridParmeter.Value = userid;
                            ListParIn.Add(IdentityidParmeter);
                            ListParIn.Add(UseridParmeter);

                            in_str = "select top 1 id,sourcename,commt,cztime,money,add_time from dt_user_in_account with(nolock) where identityid=@identityid and user_id = @user_id and type=1 and state=1 order by cztime";
                            System.Data.DataTable dt_user_in_account = SqlHelper.GetQueryFromLocalData(in_str, DBtype, ListParIn.ToArray());
                            if (dt_user_in_account.Rows.Count > 0)
                            {
                                string money = dt_user_in_account.Rows[0]["money"].ToString();
                                string commt = dt_user_in_account.Rows[0]["commt"].ToString();
                                string sourcename = dt_user_in_account.Rows[0]["sourcename"].ToString();
                                string inaccountid = dt_user_in_account.Rows[0]["id"].ToString();
                                string addtime = dt_user_in_account.Rows[0]["add_time"].ToString();

                                update_str = "update dt_user_newpay set money = " + money + ",commt = '" + commt + "',sourcename = '" + sourcename + "',inaccountid = " + inaccountid + ",addtime='" + addtime + "' where id = " + id + " and identityid='" + identityid + "' and userid = " + userid + " and money=0";
                                int result = SqlHelper.QueryFromLocal(update_str, DBtype, null);
                                msg += "[" + id + ";" + identityid + ";" + userid + "]";

                            }

                        }

                        LogPrint(msg, "UPDATEnewpaylog.txt");

                    }
                }

            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_newpay.Start();
            }

        }
        private void LogPrint(string msg, string filename)
        {
            string logRoad = "D:\\LOGTXT\\" + System.DateTime.Now.ToString("yyyy-MM-dd") + filename;//错误日志记录路径
            //string logRoad = "C:\\LOGTXT\\" + System.DateTime.Now.ToString("yyyy-MM-dd") + filename;//错误日志记录路径
            if (!File.Exists(logRoad))
            {
                FileStream fs1 = new FileStream(logRoad, FileMode.Create, FileAccess.Write);//创建写入文件 
                StreamWriter sw = new StreamWriter(fs1);
                sw.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + msg);
                sw.WriteLine();
                sw.WriteLine();
                sw.Close();
                fs1.Close();
            }
            else
            {
                FileStream fs = new FileStream(logRoad, FileMode.Append, FileAccess.Write);
                StreamWriter sr = new StreamWriter(fs);
                sr.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + msg);
                sr.WriteLine();
                sr.WriteLine();
                sr.Close();
                fs.Close();
            }
        }



        private void cleardb4queue(System.Collections.Concurrent.ConcurrentQueue<string> queue)
        {
            RedisSessiondb redisSessiondb = new RedisSessiondb(true, "", 0, "");
            RedisSession redisSession = new RedisSession(true, "", 0);

            string key = string.Empty;
            while (queue.TryDequeue(out key))
            {
                List<string> ListDb8Key = redisSessiondb.Hash_Key(key);
                int count_key = ListDb8Key.Count;
                foreach (string RedisDb8Key in ListDb8Key)
                {
                    count_key = count_key - 1;
                    if (redisSession.IsExistKey(RedisDb8Key) == false)
                    {
                        redisSessiondb.Remove(key, RedisDb8Key);
                        SRedislog("删除紀錄", "userid:" + key + "sessionkey:" + RedisDb8Key, "db4");

                    }
                }
            }
        }
        private void clear_redis4(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                if (System.DateTime.Now.ToString("HH:mm") == "02:02")
                {
                    RedisSessiondb redisSessiondb = new RedisSessiondb(true, "", 0, "");
                    RedisSession redisSession = new RedisSession(true, "", 0);
                    List<string> ListRedisDb4Key = redisSessiondb.Hash_AllKey().ToList();
                    System.Collections.Concurrent.ConcurrentQueue<string> queue = new System.Collections.Concurrent.ConcurrentQueue<string>();
                    foreach (var key in ListRedisDb4Key)
                        queue.Enqueue(key);

                    List<System.Threading.Tasks.Task> taskList = new List<System.Threading.Tasks.Task>();
                    for (int i = 0; i < 20; i++)
                    {
                        Task task = Task.Run(() =>
                        {
                            cleardb4queue(queue);

                        });
                        taskList.Add(task);
                    }
                    Task.WaitAll(taskList.ToArray());
                }
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                clear_timer.Start();
            }

        }


        //private int num = 400;
        private void clear_redis(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                int second = DateTime.Now.Second;
                int minute = DateTime.Now.Minute;



                if (minute == 10 || minute == 20 || minute == 30 || minute == 40 || minute == 52 || minute == 0)
                {
                    if (second == 8 || second == 9)
                    {
                        RedisClient redisClient = Global.redisClient;
                        List<dt_sys_online> list = (from m in redisClient.GetAll<dt_sys_online>()
                                                    where m.oper_time.AddMinutes(5.0) < DateTime.Now
                                                    select m).ToList<dt_sys_online>();

                        try
                        {
                            RemoveList<dt_sys_online>(redisClient, list);
                            PooledRedisClientManager.DisposeClient(redisClient);
                            SRedislog("删除", list.Count() + "筆", "db1");
                            foreach (dt_sys_online online in list)
                            {
                                SRedislog("删除紀錄", "id:" + online.id + "identityid:" + online.identityid + "login_addr:" + online.login_addr + "login_ip:" + online.login_ip + "login_time:" + online.login_time + "oper_time:" + online.oper_time + "status:" + online.status + "user_id:" + online.user_id + "user_name:" + online.user_name + "x_sec:" + online.x_sec, "db1");
                            }
                        }
                        catch (Exception ex)
                        {
                            SRedislog("删除", ex.ToString(), "db1");
                        }
                        // this.label1_2.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff") + " 删了：" + list.Count.ToString();
                    }
                }
            }
            catch (ThreadAbortException ex) { }
            catch (Exception)
            {

            }
            finally
            {
                cleat_timer.Start();
            }


        }

        public static void SRedislog(string TName, string values, string file)
        {
            try
            {
                string str = DateTime.Now.ToString("MMdd");
                string str2 = DateTime.Now.ToString("HH:mm:ss:fff");
                string path = "C:\\LOGTXT\\Redis\\" + file + str + ".txt";
                FileStream fileStream = new FileStream(path, FileMode.Append);
                StreamWriter streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine(str2 + " " + TName);
                streamWriter.WriteLine(values);
                streamWriter.WriteLine();
                streamWriter.Flush();
                streamWriter.Close();
                fileStream.Close();
            }
            catch (Exception e)
            {
            }
        }
        public static void RemoveList<T>(RedisClient redisClient, List<T> listData) where T : class
        {
            if (listData != null && listData.Count >= 1)
            {
                List<string> list = new List<string>();
                byte[][] array = new byte[listData.Count][];
                string str = string.Empty;
                for (int i = 0; i < listData.Count; i++)
                {
                    T t = listData[i];
                    str = t.GetType().Name;
                    t = listData[i];
                    string text = t.GetType().GetProperty("id").GetValue(listData[i], null).ToString();
                    object obj = listData[i];
                    list.Add("urn:" + str + ":" + text);
                    array[i] = Encoding.UTF8.GetBytes(text);
                }
                redisClient.RemoveAll(list);
                redisClient.SRem("ids:" + str, array);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
   
}