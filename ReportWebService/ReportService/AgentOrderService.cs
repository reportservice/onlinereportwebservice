﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DBcon;

public class AgentOrderService
{
    public DataTable AfterAgentOrder(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        return DbHelperSQL.GetQueryFromReportSwitchbei("exec [srv_lnk_agent_order].[dafacloud].[dbo].[dsp_select_after_agentReport_dt_lottery_proxy_report] @identityid,@strStartDate,@strEndDate ", DBtype, "report_share", out_sqlcmdpar);
    }
}
