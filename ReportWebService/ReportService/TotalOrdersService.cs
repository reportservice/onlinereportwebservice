﻿using DBcon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class TotalOrdersService
{

    public string TotalOrderMoney()
    {
        return "select * from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
    }

    public DataTable TotalOrderNum(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        return DbHelperSQL.GetQueryFromReportSwitchbei("exec [srv_lnk_total_order].[dafacloud].[dbo].[dsp_select_after_dt_lottery_record] @identityid,@strStartDate,@strEndDate ", DBtype, "report_share", out_sqlcmdpar);
    }
}
