﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

public class TotalOutService
{

    public string DatabaseConn;

    /// <summary>
    /// 撤單金額
    /// </summary>
    /// <returns></returns>
    public DataTable CancelMoney(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        string out_str = "select isnull(sum(cdmony),0) as cdmony from [srv_lnk_total_out].[dafacloud].[dbo].dt_lottery_cancellationsflow_sum where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
        DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(out_str, DBtype, "report_share", out_sqlcmdpar);

        return outtable;
    }

    /// <summary>
    /// 撤單人數
    /// </summary>
    /// <returns></returns>
    public DataTable CancelNum(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        string out_str = "select count(distinct user_id) as cdnumber from [srv_lnk_total_out].[dafacloud].[dbo].dt_lottery_cancellationsflow_record where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
        DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(out_str, DBtype, "report_share", out_sqlcmdpar);
        return outtable;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="DBtype"></param>
    /// <param name="out_sqlcmdpar"></param>
    /// <returns></returns>
    public DataTable BeforeOutMoney(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        // string outstr = "select identityid,user_id,sum(money) money,type,type2,state  from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_out_record with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate group by identityid,user_id,type,type2,state";
        string outstr = "exec [srv_lnk_total_out].[dafacloud].[dbo].[dsp_select_before_dt_user_out_record] @usrid,@begindate,@enddate ";
        DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", out_sqlcmdpar);

        return outtable;
    }


    /// <summary>
    /// BeforeOutMoney
    /// </summary>
    /// <param name="DBtype"></param>
    /// <param name="out_sqlcmdpar"></param>
    /// <param name="dataSource">DataSource:主庫、庫一、庫二</param>
    /// <returns></returns>
    //public DataTable BeforeOutMoney(int DBtype, SqlParameter[] out_sqlcmdpar, DataSource dataSource = DataSource.主庫)
    //{
    //    // string outstr = "select identityid,user_id,sum(money) money,type,type2,state  from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_out_record with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate group by identityid,user_id,type,type2,state";
    //    string outstr = "exec [srv_lnk_total_out].[dafacloud].[dbo].[dsp_select_before_dt_user_out_record] @usrid,@begindate,@enddate ";
    //    DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", out_sqlcmdpar);

    //    //switch (dataSource)
    //    //{
    //    //    case DataSource.主庫:
    //    //        DatabaseConn=..
    //    //        break;
    //    //    case DataSource.庫一:
    //    //        DatabaseConn=..
    //    //        break;
    //    //    case DataSource.庫二:
    //    //        break;
    //    //    default:
    //    //        break;
    //    //}

    //    return outtable;
    //}
    //public enum DataSource
    //{
    //    主庫=0,
    //    庫一=1,
    //    庫二 = 2,
    //}
}
