﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

public class AgentOutService
{
    public DataTable AfterAgentWinMoney(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        string outstr = "exec [srv_lnk_agent_out].[dafacloud].[dbo].[dsp_select_after_agentReport_dt_lottery_winning_proxy_report]  @identityid,@strStartDate,@strEndDate ";
        //instr = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as TotalWinningAccount from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report] with(nolock)  where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id  ";
        DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", out_sqlcmdpar);

        return outtable;
    }
    public DataTable AfterAgentOutMoney(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        string outstr = "exec [srv_lnk_agent_out].[dafacloud].[dbo].[dsp_select_after_agentReport_dt_user_out_proxy_report]  @identityid,@strStartDate,@strEndDate ";
        //instr = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as TotalWinningAccount from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report] with(nolock)  where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id  ";
        DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", out_sqlcmdpar);

        return outtable;
    }

}
