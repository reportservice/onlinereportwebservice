﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

public class TotalInService
{
    /// <summary>
    /// 中獎人數
    /// </summary>
    /// <param name="searchType"></param>
    /// <returns></returns>
    public string WinningNumber(int searchType)
    {
        string in_str = "";
        switch (searchType)
        {
            case 1:
                //in_str = "select count(distinct(user_id)) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                in_str = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_lottery_winning_userrecord_num] @identityid,@strStartDate,@strEndDate ";
                break;
            case 2:
                //in_str = "select count(distinct(user_id)) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where identityid = @identityid and add_date>=@strStartDate and add_date<=@strEndDate";
                in_str = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_lottery_winning_userrecord_num_cold] @identityid,@strStartDate,@strEndDate ";
                break;
            case 3:
                //in_str = "select count(1) as zjnumber from(select distinct(user_id) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord] with(nolock) where add_date >= @strStartDate and add_date <= @strEndDate and identityid = @identityid union select distinct(user_id) as zjnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_userrecord_cold] with(nolock) where add_date >= @strStartDate and add_date <= @strEndDate and identityid = @identityid) as all_count ";
                in_str = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_lottery_winning_userrecord_num_mix] @identityid,@strStartDate,@strEndDate ";
                break;
        }
        return in_str;
    }

    /// <summary>
    /// 中獎金額
    /// </summary>
    /// <returns></returns>
    public DataTable WinningSum(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        //instr = "select * from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
        string instr = "select isnull(sum(zjmoney),0) as zjmoney,isnull(sum(zjcount),0) as zjcount from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_sum]  with(nolock) where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }
    /// <summary>
    /// 非充值人數
    /// </summary>
    /// <returns></returns>
    public DataTable InTypeNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        //in_str = "select count(distinct(case when type = 1 and state = 1 then user_id end))  as cznumber,count(distinct(case when type2 = 21 and state = 1 then user_id end)) as banknumber,count(distinct(case when type2 in (11, 20) and[state] = 1 then user_id end)) as rgcknumber,count(distinct(case when type2 in(7, 12) and[state] = 1 then user_id end)) as hdljnumber,count(distinct(case when type2 = 7 and[state] = 1 then user_id end)) as xthdnumber,count(distinct(case when type2 = 12 and[state] = 1 then user_id end)) as qthdnumber,count(distinct(case when type2 = 17 then user_id end)) as cdnumber,count(distinct(case when type2 = 14  and[state] = 1 then user_id end)) as dlgznumber,count(distinct(case when type2 = 13  and[state] = 1 then user_id end)) as dlfhnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock) where identityid =@identityid and add_date>=@strStartDate and add_date<=@strEndDate";
        string in_str = "exec[srv_lnk_total_in].[dafacloud].[dbo].dsp_select_in_type_number @identityid,@strStartDate,@strEndDate";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }
    /// <summary>
    /// 快捷充值人數
    /// </summary>
    /// <returns></returns>
    public DataTable FastPayNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        string in_str = "select count(distinct(user_id)) as fastnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2 in (select fastpay from [dt_diction_quickpay] where fastpay>0) and [state]=1";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

    public DataTable AliPayNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        string in_str = "select count(distinct(user_id)) as aliypaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select Alipay from [dt_diction_quickpay] where Alipay>0) or  type2 =22) and [state]=1";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

    public DataTable WechatNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        string in_str = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_wechatnumber] @identityid,@strStartDate,@strEndDate ";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

    public DataTable QQNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        //in_str = "select count(distinct(user_id)) as qqnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select QQ from [dt_diction_quickpay] where QQ>0) or type2 =201) and [state]=1";
        string in_str = "select count(distinct(user_id)) as qqnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in(select QQ from [dt_diction_quickpay] where QQ>0) or type2 =201) and [state]=1";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

    public DataTable FourthNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        string in_str = "";
        // "select count(distinct(user_id)) as fourthnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2=268 and [state]=1";
        in_str = "select count(distinct(user_id)) as fourthnumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and type2=268 and [state]=1";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

    public DataTable UnionPayNumber(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        string in_str = "";
        //in_str = "select count(distinct(user_id)) as unionpaynumber from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_record] with(nolock)  where identityid=@identityid and add_date>=@strStartDate and add_date<=@strEndDate  and (type2 in (select Unionpay from [dt_diction_quickpay] where Unionpay>0) OR TYPE2=291 )and [state]=1";
        in_str = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_unionpaynumber] @identityid,@strStartDate,@strEndDate ";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

    public DataTable AfterInMoneySum(int DBtype, SqlParameter[] in_sqlcmdpar)
    {
        string in_str = "";
        //in_str = "select add_date, identityid, sum(czmony)as czmony,max(cznumber) as cznumber,sum(operattime) as operattime,sum(fastpaymoney) as fastpaymoney,max(fastnumber) as fastnumber,sum(bankmony) as bankmony,max(banknumber) as banknumber,sum(aliypaymony) as aliypaymony,max(aliypaynumber) as aliypaynumber,sum(wechatmony) as wechatmony,max(wechatnumber) as wechatnumber,sum(rgckmony) as rgckmony,max(rgcknumber) as rgcknumber,max(rebatenumber) as rebatenumber,sum(hdljmony) as hdljmony,max(hdljnumber) as hdljnumber,sum(xthdmony) as xthdmony,max(xthdnumber) as xthdnumber,sum(qthdmony) as qthdmony,max(qthdnumber) as qthdnumber,max(zjnumber) as zjnumber,sum(dlgzmony) as dlgzmony,max(dlgznumber) as dlgznumber,sum(dlfhmony) as dlfhmony,max(dlfhnumber) as dlfhnumber,sum(czcount) as czcount,max(qqnumber) as qqnumber,sum(qqmoney) as qqmoney,sum(fourthmoney) as fourthmoney,max(fourthnumber) as fourthnumber,sum(unionpaymoney) as unionpaymoney,max(unionpaynumber) as unionpaynumber from[srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_sum]  with(nolock)  where identityid = @identityid and add_date>= @strStartDate and add_date<= @strEndDate group by identityid,add_date";
        in_str = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_user_in_record_sum] @identityid,@strStartDate,@strEndDate";
        DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(in_str, DBtype, "report_share", in_sqlcmdpar);

        return intable;
    }

}
