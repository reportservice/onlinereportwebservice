﻿using DBcon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


    public class AgentInService
    {

    public string BeforeAgentInMoney()
    {
        string instr = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as ActivityMoney,isnull(sum(czmoney),0) as RechargeMoney   from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_in_proxy_report] with(nolock)   where identityid = @identityid and user_id = @userid  and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        //instr = "select [dt_lottery_proxy_report].identityid,user_id as UserId,isnull(sum(tzmoney),0) as BetMoney  from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_lottery_proxy_report]  where dt_lottery_proxy_report.identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  " + userIds + "  group by [dt_lottery_proxy_report].identityid,user_id ";
        //instr = "select [dt_lottery_proxy_report].identityid,user_id as UserId,isnull(sum(tzmoney),0) as BetMoney  from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_lottery_proxy_report]  where dt_lottery_proxy_report.identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  " + userIds + "  group by [dt_lottery_proxy_report].identityid,user_id ";
        return instr;
    }
    public DataTable AfterAgentInMoney(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        return DbHelperSQL.GetQueryFromReportSwitchbei("exec [srv_lnk_agent_in].[dafacloud].[dbo].[dsp_select_after_agentReport_dt_user_in_proxy_report] @identityid,@strStartDate,@strEndDate ", DBtype, "report_share", out_sqlcmdpar);
    }

    public DataTable AfterAgentRebateMoney(int DBtype, SqlParameter[] out_sqlcmdpar)
    {
        return DbHelperSQL.GetQueryFromReportSwitchbei("exec [srv_lnk_agent_in].[dafacloud].[dbo].[dsp_select_after_agentReport_dt_user_get_point_proxy_report] @identityid,@strStartDate,@strEndDate ", DBtype, "report_share", out_sqlcmdpar);
    }
}
