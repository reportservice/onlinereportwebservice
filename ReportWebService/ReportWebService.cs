﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using EasyFrame.BLL;
using EasyFrame.NewCommon.Model;
using EasyFrame.Redis;
using ServiceStack.Redis;
using System.Web.Services.Protocols;
using System.Data;
using EasyFrame.NewCommon;
using LitJson;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Runtime.Caching;


/// <summary>
///ReportWebService 的摘要说明
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
// [System.Web.Script.Services.ScriptService]
public class ReportWebService : System.Web.Services.WebService
{
    /// <summary>
    /// Redis操作帮助类
    /// </summary>
    private RedisHelper redisHelper = new RedisHelper();
    public ReportWebService()
    {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }
    public EncryptionSoapHeader encryptionSoapHeader = new EncryptionSoapHeader();

    /// <summary>
    /// 验证权限
    /// </summary>
    /// <returns></returns>
    private string ValidateAuthority()
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        string strMsg = string.Empty;
        if (!encryptionSoapHeader.IsValid(out strMsg))
        {

            resultInfo.Code = -1;
            resultInfo.StrCode = strMsg;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        return strMsg;
    }

    /// <summary>
    /// 前台專案查詢今日盈利
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="StartTime"></param>
    /// <param name="EndTime"></param>
    /// <param name="identityId"></param>
    /// <param name="flog"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public ResultInfoT<ProfitLossData> GetProfitLossData(int UserId, string StartTime, string EndTime, string identityId, int flog)
    {
        ResultInfoT<ProfitLossData> backdata = new ResultInfoT<ProfitLossData>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "UserId=" + UserId + " StartTime=" + StartTime + " EndTime=" + EndTime + " identityId=" + identityId + " flog=" + flog;
        try
        {
            st.Reset();
            st.Start();
            string strBuff = BeforeProfitReport(UserId, flog, Convert.ToDateTime(StartTime), Convert.ToDateTime(EndTime));
            ResultInfoT<dt_report_inoutaccount_new> resultInfo = JsonMapper.ToObject<ResultInfoT<dt_report_inoutaccount_new>>(strBuff);
            if (resultInfo.Code == 1)
            {
                ProfitLossData PLData = new ProfitLossData();
                decimal In_MoneyPrepaid = 0m, In_ArtificialDeposit = 0m, allCount = 0m;
                decimal sWinMoney = 0m;
                decimal sBetMoney = 0m;
                decimal sActivityMoney = 0m;
                decimal sRebateMoney = 0m;
                PLData.Balance = resultInfo.BackData.In_ArtificialDeposit.ToString("#0.00");
                PLData.Betting = resultInfo.BackData.Out_BettingAccount.ToString("#0.00");
                sBetMoney = Convert.ToDecimal(PLData.Betting);
                PLData.BonusMoney = resultInfo.BackData.Out_WinningAccount.ToString("#0.00");
                sWinMoney = Convert.ToDecimal(PLData.BonusMoney);
                PLData.Activity = resultInfo.BackData.Out_ActivityDiscountAccount.ToString("#0.00");
                sActivityMoney = Convert.ToDecimal(PLData.Activity);
                PLData.Rebate = resultInfo.BackData.Out_RebateAccount.ToString("#0.00");
                sRebateMoney = Convert.ToDecimal(PLData.Rebate);
                PLData.Withdraw = resultInfo.BackData.Out_Account.ToString("#0.00");

                In_MoneyPrepaid = resultInfo.BackData.In_MoneyPrepaid;
                In_ArtificialDeposit = resultInfo.BackData.In_ArtificialDeposit;
                PLData.AllProfitLoss = (sWinMoney - sBetMoney + sActivityMoney + sRebateMoney).ToString("#0.00");

                allCount = In_MoneyPrepaid;
                PLData.Recharge = allCount.ToString("#0.00");

                List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
                sqlcmdlist.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = identityId });
                sqlcmdlist.Add(new SqlParameter("@user_id", SqlDbType.Int) { Value = UserId });
                SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
                int DBType = SqlHelper.SwitchDBtype(identityId);
                DataTable dt_user_capital = DBcon.DbHelperSQL.GetQueryFromCongku("select top 1 use_money from dt_user_capital with(index([index_user_id])) where identityid = @identityid and user_id=@user_id and flog=0 ", DBType, sqlcmdpar);
                if (dt_user_capital != null && dt_user_capital.Rows.Count > 0)
                {
                    PLData.Balance = dt_user_capital.Rows[0]["use_money"].ToString();
                }
                // backdata.Code = CodeConstantsHelper.RetGetDataSuc;
                // backdata.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                backdata.BackData = PLData;

            }
            else
            {
                //记录错误信息
                //  EasyFrame.Common.Log.CreateErrorLogTxt("ReportWebService", "ReportWebService", "GetProfitLossData", resultInfo.StrCode);
                // backdata.Code = CodeConstantsHelper.RetParamErr;
                backdata.StrCode = "数据报表更新中,请稍后查询...";
            }
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_GetProfitLossData", "GetProfitLossData", st.ElapsedMilliseconds.ToString(), para);
            }
        }
        catch (Exception ex)
        {
            //记录错误信息
            // EasyFrame.Common.Log.CreateErrorLogTxt("ReportWebService", "ReportWebService", "GetProfitLossData", ex.Message);
            // backdata.Code = CodeConstantsHelper.RetParamCatch;
            backdata.StrCode = "数据报表更新中,请稍后查询...";
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetProfitLossData", st.ElapsedMilliseconds.ToString(), para);
            }
        }
        return backdata;
    }

    /// <summary>
    /// 今日盈利
    /// </summary>
    /// <param name="iUserId"></param>
    /// <param name="flog"></param>
    /// <param name="dtStartTime"></param>
    /// <param name="dtEndTime"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string BeforeProfitReport(int iUserId, int flog, DateTime dtStartTime, DateTime dtEndTime)
    {
        Stopwatch st = new Stopwatch();
        st.Reset();
        st.Start();
        LogMsg logmsg = new LogMsg();
        string para = "iUserId=" + iUserId + " flog=" + flog + " dtStartTime=" + dtStartTime + " dtEndTime=" + dtEndTime;
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        string strResult = string.Empty;
        try
        {
            //验证是否有权访问  
            // string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            //会员
            if (flog == 0)
            {
                //获取前台盈利报表数据
                //strResult = GetProfitReport(iUserId, dtStartTime, dtEndTime);
                strResult = GetProfitReport_New(iUserId, dtStartTime, dtEndTime);
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_BeforeProfitReport", "BeforeProfitReport", st.ElapsedMilliseconds.ToString(), para);
                }
                return strResult;
            }
            //测试账号
            strResult = GetProfitReportTest_New(iUserId, dtStartTime, dtEndTime);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_BeforeProfitReport", "BeforeProfitReport", st.ElapsedMilliseconds.ToString(), para);
            }
            return strResult;
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "BeforeProfitReport", st.ElapsedMilliseconds.ToString(), para);
            }
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            return LitJson.JsonMapper.ToJson(resultInfo);
        }

    }

    public string report_share = ReportHelper.Report_Share;
    public string report_in_conn = ReportHelper.Report_In_Conn;
    public string report_out_conn = ReportHelper.Report_Out_Conn;
    public string report_order_conn = ReportHelper.Report_Order_Conn;
    /// <summary>
    /// 測試區今日盈利
    /// </summary>
    /// <param name="iUserId"></param>
    /// <param name="dtStartTime"></param>
    /// <param name="dtEndTime"></param>
    /// <returns></returns>
    private string GetProfitReportTest_New(int iUserId, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_inoutaccount_new> resultInfo = new ResultInfoT<dt_report_inoutaccount_new>();
        DataTableHelper dataTableHelper = new DataTableHelper();
        //从报表库拿数据
        //  string identityid = strIdentityId; //条件从查询条件获取
        string usrid = iUserId.ToString();
        string begindate = dtStartTime.ToString("yyyy-MM-dd HH:mm:ss");//条件从查询条件获取
        string enddate = dtEndTime.ToString("yyyy-MM-dd HH:mm:ss");//条件从查询条件获取
        dt_month_all_record dar = new dt_month_all_record();

        string instr = "";
        DataTable intable = null;
        string outstr = "";
        DataTable outtable = null;
        string orderstr = "";
        DataTable ordertable = null;
        dt_report_inoutaccount reportSummary = new dt_report_inoutaccount();
        ReportHelper reporthelper = new ReportHelper();
        //dt_user_in_verify duiv = new dt_user_in_verify();
        string identityid = "";
        List<SqlParameter> AliSqlParamter = new List<SqlParameter>();
        SqlParameter alipa_usrid = new SqlParameter("@usrid", SqlDbType.Int);
        alipa_usrid.Value = usrid;
        SqlParameter alipa_begindate = new SqlParameter("@begindate", SqlDbType.DateTime);
        alipa_begindate.Value = begindate;
        SqlParameter alipa_enddate = new SqlParameter("@enddate", SqlDbType.DateTime);
        alipa_enddate.Value = enddate;
        AliSqlParamter.Add(alipa_usrid);
        AliSqlParamter.Add(alipa_begindate);
        AliSqlParamter.Add(alipa_enddate);

        List<SqlParameter> UserSqlParamter = new List<SqlParameter>();
        SqlParameter pa_usrid = new SqlParameter("@usrid", SqlDbType.Int);
        pa_usrid.Value = usrid;
        UserSqlParamter.Add(pa_usrid);

        int DBtype = 0;

        string userstr = "select identityid,id from dt_users where id=@usrid";
        DataTable usertable = DBcon.DbHelperSQL.GetQueryFromShare(userstr, UserSqlParamter.ToArray());

        string Identityid = usertable.Rows[0]["identityid"].ToString();
        DBtype = SqlHelper.SwitchDBtype(Identityid);

        //查询in表汇总
        instr = "select identityid,user_id,sum(money) money,type,type2,state from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_in_record_test with(nolock) where user_id=@usrid and add_date>=@begindate and add_date <=@enddate group by identityid,user_id,type,type2,state";
        intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
        reporthelper.ConvertToInMode(intable, dar);
        identityid = usertable.Rows[0]["identityid"].ToString();

        instr = "select isnull(sum(money),0) as  zjmoney from [srv_lnk_total_out].[dafacloud].[dbo].dt_lottery_winning_userrecord_test with(nolock) where user_id=@usrid and add_date>=@begindate and add_date <=@enddate ";
        intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
        reporthelper.ConvertToMode(intable, dar);

        //查询get_point表的数据
        instr = "select isnull(sum(money),0) as rebatemoney from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_get_point_userrecord_test with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate ";
        intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
        reporthelper.ConvertToMode(intable, dar);

        //string str = "";


        //查询out表的数据
        outstr = "select identityid,user_id,sum(money) money,type,type2,state  from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_out_record_test with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate group by identityid,user_id,type,type2,state";
        outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", AliSqlParamter.ToArray());
        // reporthelper.ConvertToMode(outtable, duos);
        reporthelper.ConvertToOutMode(outtable, dar);


        //查询order表的数据
        orderstr = "select isnull(sum(normal_money),0) as tzmony from [srv_lnk_total_out].[dafacloud].[dbo].dt_lottery_record_test with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate  ";
        ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, DBtype, "report_share", AliSqlParamter.ToArray());
        //reporthelper.ConvertToMode(ordertable, dls);
        reporthelper.ConvertToMode(ordertable, dar);

        dt_report_inoutaccount_new inOutAccount = new dt_report_inoutaccount_new();
        inOutAccount.identityid = identityid;
        inOutAccount.user_id = Convert.ToInt32(usrid);
        reporthelper.ConvertInoutAccountModelToModel(inOutAccount, dar);
        inOutAccount.Out_Account =  dar.rgtcmoney;
        inOutAccount.In_MoneyPrepaid = inOutAccount.In_AlipayPrepaid + inOutAccount.In_BankPrepaid + inOutAccount.In_FastPrepaid + inOutAccount.In_FourthPrepaid + inOutAccount.In_QQ + inOutAccount.In_UnionpayPrepaid + inOutAccount.In_WeChatPrepaid + inOutAccount.In_ArtificialDeposit;
        inOutAccount.Out_ActivityDiscountAccount = inOutAccount.Out_ActivityDiscountAccount + inOutAccount.Out_OtherDiscountAccount;
        //盈利总额=中奖总额－投注总额+活动总额+返点总额
        inOutAccount.ProfitLoss = dar.zjmoney - dar.tzmony + dar.hdljmony + dar.rebatemoney;
        inOutAccount.WinRate = 0;
        //if (dar.tzmony>0)
        //{
        //    inOutAccount.WinRate = (dar.zjmoney - dar.tzmony + dar.hdljmony + dar.rebatemoney) / dar.zjmoney;
        //}
        inOutAccount.AddTime = DateTime.Now;
        resultInfo.Code = 1;
        resultInfo.BackData = inOutAccount;

        //    return usrid;

        return LitJson.JsonMapper.ToJson(resultInfo);
    }

    /// <summary>
    /// 正式帳號盈利
    /// </summary>
    /// <param name="iUserId"></param>
    /// <param name="dtStartTime"></param>
    /// <param name="dtEndTime"></param>
    /// <returns></returns>
    private string GetProfitReport_New(int iUserId, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_inoutaccount_new> resultInfo = new ResultInfoT<dt_report_inoutaccount_new>();
        DataTableHelper dataTableHelper = new DataTableHelper();
        //从报表库拿数据
        //  string identityid = strIdentityId; //条件从查询条件获取
        string usrid = iUserId.ToString();
        string begindate = dtStartTime.ToString("yyyy-MM-dd HH:mm:ss");//条件从查询条件获取
        string enddate = dtEndTime.ToString("yyyy-MM-dd HH:mm:ss");//条件从查询条件获取
        dt_month_all_record dar = new dt_month_all_record();
        int searchType = 0;
        if (Convert.ToDateTime(dtStartTime) >= DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(dtEndTime) >= DateTime.Now.AddDays(-1).Date)
        {
            searchType = 1;
        }
        else if (Convert.ToDateTime(dtStartTime) < DateTime.Now.AddDays(-1).Date && Convert.ToDateTime(dtEndTime) < DateTime.Now.AddDays(-1).Date)
        {
            searchType = 2;
        }
        else
        {
            searchType = 3;
        }

        string instr = "";
        DataTable intable = null;
        string outstr = "";
        DataTable outtable = null;
        string orderstr = "";
        DataTable ordertable = null;
        dt_report_inoutaccount reportSummary = new dt_report_inoutaccount();
        ReportHelper reporthelper = new ReportHelper();
        //dt_user_in_verify duiv = new dt_user_in_verify();
        string identityid = "";
        int DBtype = 0;

        List<SqlParameter> AliSqlParamter = new List<SqlParameter>();
        SqlParameter alipa_usrid = new SqlParameter("@usrid", SqlDbType.Int);
        alipa_usrid.Value = usrid;
        SqlParameter alipa_begindate = new SqlParameter("@begindate", SqlDbType.DateTime);
        alipa_begindate.Value = begindate;
        SqlParameter alipa_enddate = new SqlParameter("@enddate", SqlDbType.DateTime);
        alipa_enddate.Value = enddate;
        AliSqlParamter.Add(alipa_usrid);
        AliSqlParamter.Add(alipa_begindate);
        AliSqlParamter.Add(alipa_enddate);

        TotalOutService totalOutService = new TotalOutService();

        List<SqlParameter> UserSqlParamter = new List<SqlParameter>();
        SqlParameter pa_usrid = new SqlParameter("@usrid", SqlDbType.Int);
        pa_usrid.Value = usrid;
        UserSqlParamter.Add(pa_usrid);
        string userstr = "select identityid,id from dt_users where id=@usrid";
        DataTable usertable = DBcon.DbHelperSQL.GetQueryFromShare(userstr, UserSqlParamter.ToArray());

        if (usertable.Rows.Count > 0)
            identityid = usertable.Rows[0]["identityid"].ToString();

        DBtype = SqlHelper.SwitchDBtype(identityid);

        //查询in表汇总
        //instr = "select identityid,user_id,sum(money) money,type,type2,state from  [srv_lnk_total_in].[dafacloud].[dbo].dt_user_in_record with(nolock) where user_id=@usrid and add_date>=@begindate and add_date <=@enddate group by identityid,user_id,type,type2,state";
        instr = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_user_in_record] @usrid,@begindate,@enddate ";
        intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
        reporthelper.ConvertToInMode(intable, dar);

        if (searchType == 1)
        {
            instr = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_lottery_winning_userrecord] @usrid,@begindate,@enddate";
        }
        else if (searchType == 2)
        {
            instr = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_lottery_winning_userrecord_cold] @usrid,@begindate,@enddate";
        }
        else
        {
            instr = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_lottery_winning_userrecord_mix] @usrid,@begindate,@enddate";
        }
        //instr = "select isnull(sum(money),0) as  zjmoney from dt_lottery_winning_userrecord with(nolock) where user_id=@usrid and add_date>=@begindate and add_date <=@enddate group by identityid,user_id";
        intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
        reporthelper.ConvertToMode(intable, dar);

        //查询get_point表的数据
        instr = "exec [srv_lnk_total_in].[dafacloud].[dbo].[dsp_select_dt_user_get_point_userrecord] @usrid,@begindate,@enddate";
        intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
        reporthelper.ConvertToMode(intable, dar);

        string str = "";


        //查询out表的数据
        //outstr = "select identityid,user_id,sum(money) money,type,type2,state  from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_out_record with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate group by identityid,user_id,type,type2,state";
        outtable = totalOutService.BeforeOutMoney(DBtype, AliSqlParamter.ToArray()); //DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", AliSqlParamter.ToArray());
        // reporthelper.ConvertToMode(outtable, duos);
        reporthelper.ConvertToOutMode(outtable, dar);


        //查询order表的数据
        //orderstr = "select isnull(sum(normal_money),0) as tzmony from [srv_lnk_total_order].[dafacloud].[dbo].dt_lottery_record with(nolock) where user_id=@usrid and add_date>=@begindate and add_date<=@enddate  group by identityid,user_id";
        orderstr = "exec [srv_lnk_total_order].[dafacloud].[dbo].[dsp_select_dt_lottery_record] @usrid,@begindate,@enddate";
        ordertable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(orderstr, DBtype, "report_share", AliSqlParamter.ToArray());
        //reporthelper.ConvertToMode(ordertable, dls);
        reporthelper.ConvertToMode(ordertable, dar);
        dar.hdljmony = dar.qthdmony + dar.xthdmony;
        dt_report_inoutaccount_new inOutAccount = new dt_report_inoutaccount_new();
        inOutAccount.identityid = identityid;
        inOutAccount.user_id = Convert.ToInt32(usrid);
        reporthelper.ConvertInoutAccountModelToModel(inOutAccount, dar);
        inOutAccount.In_MoneyPrepaid = inOutAccount.In_AlipayPrepaid + inOutAccount.In_BankPrepaid + inOutAccount.In_FastPrepaid + inOutAccount.In_FourthPrepaid + inOutAccount.In_QQ + inOutAccount.In_UnionpayPrepaid + inOutAccount.In_WeChatPrepaid + inOutAccount.In_ArtificialDeposit;
        inOutAccount.Out_Account =  dar.rgtcmoney;
        inOutAccount.Out_ActivityDiscountAccount = inOutAccount.Out_ActivityDiscountAccount + inOutAccount.Out_OtherDiscountAccount;
        //盈利总额=中奖总额－投注总额+活动总额+返点总额
        inOutAccount.ProfitLoss = dar.zjmoney - dar.tzmony + dar.hdljmony + dar.rebatemoney;
        inOutAccount.WinRate = 0;
        //if (dar.tzmony>0)
        //{
        //    inOutAccount.WinRate = (dar.zjmoney - dar.tzmony + dar.hdljmony + dar.rebatemoney) / dar.zjmoney;
        //}
        inOutAccount.AddTime = DateTime.Now;
        resultInfo.Code = 1;
        resultInfo.BackData = inOutAccount;

        //    return usrid;

        return LitJson.JsonMapper.ToJson(resultInfo);
    }


    private string GetProfitReport(int iUserId, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        //RedisClientConfig redisClientConfig = new RedisClientConfig();
        //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
        var redisClient = GlobalVariable.redisClient;
        var redisClientKey = GlobalVariable.redisClientKey;
        //var reportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.user_id == iUserId && m.AddTime >= dtStartTime && m.AddTime <= dtEndTime).ToList();
        //Log.Info("获取出入账数据开始时间：" + DateTime.Now.ToString());
        List<dt_report_inoutaccount> reportAgentList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, iUserId.ToString(), "").ToList();
        //Log.Info("获取出入账数据结束时间：" + DateTime.Now.ToString());
        //redisClient.Quit();
        //redisClient.Dispose();
        //redisClientKey.Quit();
        //redisClientKey.Dispose();
        GlobalVariable.PooledRedisClientManager.DisposeClient(redisClient);
        GlobalVariable.PooledRedisClientManager.DisposeClient(redisClientKey);
        dt_report_inoutaccount inOutAccount = new dt_report_inoutaccount();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
            //中奖总额
            inOutAccount.Out_WinningAccount += reportAgentList[i].Out_WinningAccount;
            //活动总额
            inOutAccount.Out_ActivityDiscountAccount += reportAgentList[i].Out_ActivityDiscountAccount;
            //返点总额
            inOutAccount.Out_RebateAccount += reportAgentList[i].Out_RebateAccount;
            //充值总额
            inOutAccount.In_FastPrepaid += reportAgentList[i].In_FastPrepaid + reportAgentList[i].In_BankPrepaid
                                         + reportAgentList[i].In_AlipayPrepaid + reportAgentList[i].In_WeChatPrepaid + reportAgentList[i].In_ArtificialDeposit;
            //提现总额
            inOutAccount.Out_Account += reportAgentList[i].Out_Account;
            //盈利总额=中奖总额－投注总额+活动总额+返点总额
            inOutAccount.ProfitLoss = inOutAccount.Out_WinningAccount;
            inOutAccount.ProfitLoss -= inOutAccount.Out_BettingAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_ActivityDiscountAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_RebateAccount;
        }
        // Log.Info("统计今日盈亏结束时间：" + DateTime.Now.ToString());
        inOutAccount.AddTime = DateTime.Now;
        resultInfo.Code = 1;
        resultInfo.BackData = inOutAccount;
        return LitJson.JsonMapper.ToJson(resultInfo);
    }

    private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
    {
        if (conn.State != ConnectionState.Open)
            conn.Open();
        cmd.Connection = conn;
        cmd.CommandText = cmdText;
        if (trans != null)
            cmd.Transaction = trans;
        cmd.CommandType = CommandType.Text;//cmdType;
        if (cmdParms != null)
        {
            foreach (SqlParameter parm in cmdParms)
                cmd.Parameters.Add(parm);
        }
    }


    private string GetTestProfitReport(int iUserId, DateTime dtStartTime, DateTime dtEndTime)
    {
        ResultInfoT<dt_report_test_inoutaccount> resultInfo = new ResultInfoT<dt_report_test_inoutaccount>();
        //RedisClientConfig redisClientConfig = new RedisClientConfig();
        //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
        var redisClient = GlobalVariable.redisClient;
        var redisClientKey = GlobalVariable.redisClientKey;
        //var reportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.user_id == iUserId && m.AddTime >= dtStartTime && m.AddTime <= dtEndTime).ToList();
        List<int> listUserId = new List<int>();
        listUserId.Add(iUserId);
        List<dt_report_test_inoutaccount> reportAgentList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, dtStartTime, dtEndTime, iUserId.ToString(), "").ToList();
        //redisClient.Quit();
        //redisClient.Dispose();
        //redisClientKey.Quit();
        //redisClientKey.Dispose();
        GlobalVariable.PooledRedisClientManager.DisposeClient(redisClient);
        GlobalVariable.PooledRedisClientManager.DisposeClient(redisClientKey);
        dt_report_test_inoutaccount inOutAccount = new dt_report_test_inoutaccount();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
            //中奖总额
            inOutAccount.Out_WinningAccount += reportAgentList[i].Out_WinningAccount;
            //活动总额
            inOutAccount.Out_ActivityDiscountAccount += reportAgentList[i].Out_ActivityDiscountAccount;
            //返点总额
            inOutAccount.Out_RebateAccount += reportAgentList[i].Out_RebateAccount;
            //充值总额
            inOutAccount.In_FastPrepaid += reportAgentList[i].In_FastPrepaid + reportAgentList[i].In_BankPrepaid
                                         + reportAgentList[i].In_AlipayPrepaid + reportAgentList[i].In_WeChatPrepaid + reportAgentList[i].In_ArtificialDeposit;
            //提现总额
            inOutAccount.Out_Account += reportAgentList[i].Out_Account;
            //盈利总额=中奖总额－投注总额+活动总额+返点总额
            inOutAccount.ProfitLoss = inOutAccount.Out_WinningAccount;
            inOutAccount.ProfitLoss -= inOutAccount.Out_BettingAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_ActivityDiscountAccount;
            inOutAccount.ProfitLoss += inOutAccount.Out_RebateAccount;
        }
        inOutAccount.AddTime = DateTime.Now;
        resultInfo.Code = 1;
        resultInfo.BackData = inOutAccount;
        return LitJson.JsonMapper.ToJson(resultInfo);
    }

    /// <summary>
    /// 獲取歷史投注金額
    /// </summary>
    /// <param name="iUserId"></param>
    /// <param name="flog"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetBetMoney(int iUserId, int flog)
    {
        ResultInfoT<dt_report_inoutaccount> resultInfo = new ResultInfoT<dt_report_inoutaccount>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "iUserId=" + iUserId + " flog=" + flog;
        try
        {
            //验证是否有权访问  
            // string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            st.Reset();
            st.Start();
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            //var redisClient = GlobalVariable.redisClient;
            //var redisClientKey = GlobalVariable.redisClientKey;
            dt_report_inoutaccount inOutAccount = new dt_report_inoutaccount();
            //会员账号
            // if (flog == 0)
            //{
            GetBetData(iUserId, inOutAccount);
            //}

            //redisClient.Quit();
            //redisClient.Dispose();
            //redisClientKey.Quit();
            //redisClientKey.Dispose();
            //GlobalVariable.PooledRedisClientManager.DisposeClient(redisClient);
            //GlobalVariable.PooledRedisClientManager.DisposeClient(redisClientKey);
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_GetBetMoney", "GetBetMoney", st.ElapsedMilliseconds.ToString(), para);
            }
            inOutAccount.AddTime = DateTime.Now;
            resultInfo.Code = 1;
            resultInfo.BackData = inOutAccount;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetBetMoney", st.ElapsedMilliseconds.ToString(), para);
            }
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 歷史投注金額方法
    /// </summary>
    /// <param name="iUserId"></param>
    /// <param name="inOutAccount"></param>
    private void GetBetData(int iUserId, dt_report_inoutaccount inOutAccount)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<SqlParameter> UserSqlParamter = new List<SqlParameter>();
        SqlParameter pa_usrid = new SqlParameter("@userid", SqlDbType.Int);
        pa_usrid.Value = iUserId;
        UserSqlParamter.Add(pa_usrid);

        string identityid = DBcon.DbHelperSQL.SwitchIdentityid(iUserId);
        int DBtype = SqlHelper.SwitchDBtype(identityid);

        string bet_str = "select sum(normal_money) as Out_BettingAccount from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where  user_id=@userid  group by user_id ";
        DataTable bettable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(bet_str, DBtype, "report_share", UserSqlParamter.ToArray());
        ReportHelper rh = new ReportHelper();
        dt_report_inoutaccount reportAgentList = new dt_report_inoutaccount();
        for (int r = 0; r < bettable.Rows.Count; r++)
        {
            rh.ConvertDataRowToModel(bettable.Rows[r], reportAgentList);
        }
        inOutAccount.Out_BettingAccount = reportAgentList.Out_BettingAccount;

        //List<dt_report_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        //for (var i = 0; i < reportAgentList.Count; i++)
        //{
        //    //投注总额
        //    inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
        //}
    }

    private void GetTestBetData(int iUserId, RedisClient redisClient, RedisClient redisClientKey, dt_report_inoutaccount inOutAccount)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<dt_report_test_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_test_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //投注总额
            inOutAccount.Out_BettingAccount += reportAgentList[i].Out_BettingAccount;
        }
    }

    /// <summary>
    /// 获取中奖金额
    /// </summary>
    /// <param name="context"></param>

    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetAwardMoney(int iUserId, int flog)
    {
        ResultInfoT<AwardMoneyData> resultInfo = new ResultInfoT<AwardMoneyData>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "iUserId=" + iUserId + " flog=" + flog;
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            st.Reset();
            st.Start();
            AwardMoneyData awardMoneyData = new AwardMoneyData();
            //会员
            //if (flog == 0)
            //{
            GetAwardData(iUserId, awardMoneyData);
            //  }
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_GetAwardMoney", "GetAwardMoney", st.ElapsedMilliseconds.ToString(), para);
            }

            resultInfo.Code = 1;
            resultInfo.BackData = awardMoneyData;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            EasyFrame.NewCommon.Log.Error(ex);
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetAwardMoney", st.ElapsedMilliseconds.ToString(), para);
            }
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    /// <summary>
    /// 累積中獎金額
    /// </summary>
    /// <param name="iUserId"></param>
    /// <param name="awardMoneyData"></param>
    private void GetAwardData(int iUserId, AwardMoneyData awardMoneyData)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        //List<dt_report_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        List<SqlParameter> UserSqlParamter = new List<SqlParameter>();
        SqlParameter pa_usrid = new SqlParameter("@userid", SqlDbType.Int);
        pa_usrid.Value = iUserId;
        UserSqlParamter.Add(pa_usrid);
        string identityid = DBcon.DbHelperSQL.SwitchIdentityid(iUserId);
        int DBtype = SqlHelper.SwitchDBtype(identityid);
        string awar_str = "select user_id ,ISNULL(SUM(Out_WinningAccount),0) as Out_WinningAccount   from [srv_lnk_total_in].[dafacloud].[dbo].[dt_lottery_winning_historyRecord] with(nolock) where  user_id=@userid group by user_id ";
        DataTable awartable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(awar_str, DBtype, "report_share", UserSqlParamter.ToArray());
        ReportHelper rh = new ReportHelper();
        dt_report_inoutaccount reportAgentList = new dt_report_inoutaccount();
        for (int r = 0; r < awartable.Rows.Count; r++)
        {
            rh.ConvertDataRowToModel(awartable.Rows[r], reportAgentList);
        }
        //中奖总额
        awardMoneyData.AwardMoney = reportAgentList.Out_WinningAccount;
    }


    private void GetTestAwardData(int iUserId, RedisClient redisClient, RedisClient redisClientKey, AwardMoneyData awardMoneyData)
    {
        //var reportAgentList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.user_id == iUserId).ToList();
        List<dt_report_test_inoutaccount> reportAgentList = redisHelper.SearchByUserId<dt_report_test_inoutaccount>(redisClient, redisClientKey, iUserId.ToString()).ToList();
        for (var i = 0; i < reportAgentList.Count; i++)
        {
            //中奖总额
            awardMoneyData.AwardMoney += reportAgentList[i].Out_WinningAccount;
        }
    }


    /// <summary>
    /// 代理报表查询－－－－－－前台的[下级报表 GetAgentReport]的功能
    /// </summary>
    /// <param name="context"></param>

    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetAgentReport(string IdentityId, int UserId, string UserName, int IsAgent, DateTime StartTime, DateTime EndTime, int ClassNum, int LoginId, int flog, int Index, int DataNum)
    {
        ResultInfoT<List<AgentReport>> ResultInfo = new ResultInfoT<List<AgentReport>>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "IdentityId=" + IdentityId + " UserId=" + UserId + " UserName=" + UserName + " IsAgent=" + IsAgent + " StartTime=" + StartTime + " EndTime=" + EndTime + " ClassNum=" + ClassNum + " LoginId=" + LoginId + " flog=" + flog + " Index=" + Index + " DataNum=" + DataNum;
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}    
           
            st.Reset();
            st.Start();
            //RedisClientConfig redisClientConfig = new RedisClientConfig();
            //redisClientConfig.ServerIP = DBcon.DBcontring.redisServerIP;
            // var redisClient = GlobalVariable.redisClient;
            // var redisClientKey = GlobalVariable.redisClientKey;
            string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;
            string report_order_conn = DBcon.DBcontring.Report_Order_Conn;
            string report_in_conn = DBcon.DBcontring.Report_In_Conn;


            List<AgentReport> agentReportList = new List<AgentReport>();
            int Count = 1;
            if (LoginId == UserId)
            {
                ClassNum = 1;
            }
            //if (IsAgent > 0)
            //{

            //報表
            List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
            sqlcmdlist.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
            sqlcmdlist.Add(new SqlParameter("@user_id", UserId));
            sqlcmdlist.Add(new SqlParameter("@LoginId", LoginId));
            SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

            int DBtype = SqlHelper.SwitchDBtype(IdentityId);
            //查user_id,user_id的下級,login_id
            string strSql = "select id,user_name,is_agent from dt_users with(index([agent_id])) where identityid=@identityid and agent_id=@user_id union select id,user_name,is_agent from dt_users with(index([PK_dt_users])) where identityid=@identityid and id=@user_id  union select id,user_name,is_agent from dt_users with(index([PK_dt_users])) where identityid=@identityid and id=@LoginId";
            DataTable userDataTable = DBcon.DbHelperSQL.GetQueryFromShare(strSql, sqlcmdpar);
            Dictionary<string, string> AgentClassList = new Dictionary<string, string>();
            List<int> MemberClassList = new List<int>();
            Dictionary<int, int> TempClass = new Dictionary<int, int>();
            DataTable dtMemberClass = userDataTable.Copy();
            dtMemberClass.Columns.Remove("user_name");
            dtMemberClass.Columns.Remove("is_agent");
            // AgentClassList.Add(UserId.ToString(), UserName);
            for (int i = 0; i < userDataTable.Rows.Count; i++)
            {
                // string strIsAgent = userDataTable.Rows[i]["is_agent"].ToString();
                string strUserName = userDataTable.Rows[i]["is_agent"].ToString() + ";" + userDataTable.Rows[i]["user_name"].ToString();
                string strId = userDataTable.Rows[i]["id"].ToString();
                if (strId == "37555")
                {
                    string ss = "";
                }

                int iId = int.Parse(strId);
                //取得要查詢的id和name和是否代理
                AgentClassList.Add(iId.ToString(), strUserName);
                //取得id
                MemberClassList.Add(iId);

            }
            //正式账号
            if (flog == 0)
            {
                //获取正式代理下级数据
                string strResult = GetAgentData(dtMemberClass, MemberClassList, IdentityId, UserId, StartTime, EndTime, UserName, ClassNum, LoginId, userDataTable, agentReportList, ResultInfo, AgentClassList);
                if (!string.IsNullOrWhiteSpace(strResult))
                {
                    //  return strResult;
                }
            }
            else //测试账号
            {
                // 获取测试代理下级数据
                string strResult = GetTestAgentData(MemberClassList, IdentityId, UserId, StartTime, EndTime, UserName, ClassNum, LoginId, userDataTable, agentReportList, ResultInfo, AgentClassList);
                if (!string.IsNullOrWhiteSpace(strResult))
                {
                    //   return strResult;
                }
            }
            agentReportList = agentReportList.OrderByDescending(m => m.BetMoney).ToList();
            Count = agentReportList.Count;
            agentReportList = agentReportList.Skip(Index * DataNum).Take(DataNum).ToList();

            ResultInfo.Code = 1;
            ResultInfo.DataCount = Count;
            ResultInfo.BackData = agentReportList;

            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_GetAgentReport", "GetAgentReport", st.ElapsedMilliseconds.ToString(), para);
            }
            return LitJson.JsonMapper.ToJson(ResultInfo);

        }
        catch (Exception ex)
        {
            ResultInfo.Code = -1;
            ResultInfo.DataCount = 0;
            ResultInfo.StrCode = ex.Message;
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetAgentReport", st.ElapsedMilliseconds.ToString(), para);
            }
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
    }


    private Dictionary<string, int> GetAgentUserData_New(DataTable userClassDataTable)
    {
        string strUserIds = string.Empty;
        //保存会员的所有上级代理
        Dictionary<string, int> dicAgentUserData = new Dictionary<string, int>();
        //临时保存上级代理
        Dictionary<string, string> dicTempAgent = new Dictionary<string, string>();

        for (var i = 0; i < userClassDataTable.Rows.Count; i++)
        {
            // string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            string strUserId = userClassDataTable.Rows[i]["father_id"].ToString();
            int count = Convert.ToInt32(userClassDataTable.Rows[i]["count"]);
            dicAgentUserData.Add(strUserId, count);
            //string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            //string strFatherId = userClassDataTable.Rows[i]["father_id"].ToString();
            //if (!dicAgentUserData.ContainsKey(strUserId))
            //{
            //    dicAgentUserData.Add(strUserId, strFatherId + ",");
            //    continue;
            //}
            //dicAgentUserData[strUserId] += strFatherId + ",";
        }
        return dicAgentUserData;
    }
    private Dictionary<string, string> GetAgentUserData(DataTable userClassDataTable)
    {
        string strUserIds = string.Empty;
        //保存会员的所有上级代理
        Dictionary<string, string> dicAgentUserData = new Dictionary<string, string>();
        //临时保存上级代理
        Dictionary<string, string> dicTempAgent = new Dictionary<string, string>();
        for (var i = 0; i < userClassDataTable.Rows.Count; i++)
        {
            string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            string strFatherId = userClassDataTable.Rows[i]["father_id"].ToString();
            if (!dicAgentUserData.ContainsKey(strUserId))
            {
                dicAgentUserData.Add(strUserId, strFatherId + ",");
                continue;
            }
            dicAgentUserData[strUserId] += strFatherId + ",";
        }
        return dicAgentUserData;
    }
    private Dictionary<string, string> GetAgentUserClass(DataTable userClassDataTable)
    {
        string strUserIds = string.Empty;
        //保存会员的所有上级代理
        Dictionary<string, string> dicAgentUserData = new Dictionary<string, string>();
        //临时保存上级代理
        Dictionary<string, string> dicTempAgent = new Dictionary<string, string>();
        for (var i = 0; i < userClassDataTable.Rows.Count; i++)
        {
            string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            string strUserClass = userClassDataTable.Rows[i]["user_class"].ToString();
            if (!dicAgentUserData.ContainsKey(strUserId))
            {
                dicAgentUserData.Add(strUserId, strUserClass);
                continue;
            }

        }
        return dicAgentUserData;
    }
    public void ComputeNum(Dictionary<string, int> dicNum, string strUserId, Dictionary<string, string> dicAgentData)
    {
        if (!dicNum.ContainsKey(strUserId))
        {
            dicNum.Add(strUserId, 1);
        }
        else
        {
            dicNum[strUserId]++;
        }
        if (dicAgentData.ContainsKey(strUserId))
        {
            string[] strFatherIds = dicAgentData[strUserId].Split(',');
            for (int i = 0; i < strFatherIds.Length; i++)
            {
                string strFatherId = strFatherIds[i];
                if (!dicNum.ContainsKey(strFatherId))
                {
                    dicNum.Add(strFatherId, 1);
                }
                else
                {
                    dicNum[strFatherId]++;
                }
            }
        }

    }
    

    /// <summary>
    /// 获取正式代理数据
    /// </summary>
    /// <param name="MemberClassList">会员所有层级用户ID</param>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="StartTime">开始时间</param>
    /// <param name="EndTime">结束时间</param>
    /// <param name="UserName">会员名称</param>
    /// <param name="ClassNum">代理层级</param>
    /// <param name="LoginId">登录账号ID</param>
    /// <param name="UsersList">用户数据</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private string GetAgentData(DataTable dtMemberClass, List<int> MemberClassList, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum,
     int LoginId, DataTable UsersList, List<AgentReport> agentReportList, ResultInfoT<List<AgentReport>> ResultInfo, Dictionary<string, string> AgentClassList)
    {


        ReportHelper reporthelper = new ReportHelper();
        //connectstring
        string agent_in_conn = DBcon.DBcontring.Agent_In_Conn;
        string agent_out_conn = DBcon.DBcontring.Agent_Out_Conn;
        string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;
        string report_order_conn = DBcon.DBcontring.Report_Order_Conn;

        //報表
        List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
        sqlcmdlist.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        sqlcmdlist.Add(new SqlParameter("@strStartDate", StartTime));
        sqlcmdlist.Add(new SqlParameter("@strEndDate", EndTime));
        sqlcmdlist.Add(new SqlParameter("@LoginId", LoginId));
        SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

        //報表
        List<SqlParameter> sqlcmdlist2 = new List<SqlParameter>();
        sqlcmdlist2.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        sqlcmdlist2.Add(new SqlParameter("@strStartDate", StartTime));
        sqlcmdlist2.Add(new SqlParameter("@strEndDate", EndTime));
        sqlcmdlist2.Add(new SqlParameter("@LoginId", LoginId));
        sqlcmdlist2.Add(new SqlParameter("@userType", SqlDbType.Structured) { Value = dtMemberClass });
        SqlParameter[] sqlcmdpar2 = sqlcmdlist2.ToArray();

        //線上

        SqlParameter pa_identityid1 = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
        pa_identityid1.Value = IdentityId;
        SqlParameter pa_StartDate1 = new SqlParameter("@strStartDate", SqlDbType.DateTime);
        pa_StartDate1.Value = StartTime.ToString("yyyy-MM-dd");
        SqlParameter pa_EndDate1 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        pa_EndDate1.Value = Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd");


        //站
        List<SqlParameter> sqlcmdlist_identityid = new List<SqlParameter>();
        sqlcmdlist_identityid.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        SqlParameter[] sqlcmdpar_identityid = sqlcmdlist_identityid.ToArray();

        //站長
        List<SqlParameter> sqlcmdlist_alicare = new List<SqlParameter>();
        sqlcmdlist_alicare.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        sqlcmdlist_alicare.Add(new SqlParameter("@userid", UserId));
        SqlParameter[] sqlcmdpar_care = sqlcmdlist_alicare.ToArray();

        Dictionary<string, string> dicAgent = new Dictionary<string, string>();
        Dictionary<string, ReportAgent> dicReportAgent = new Dictionary<string, ReportAgent>();

        //判斷分庫
        int DBtype = SqlHelper.SwitchDBtype(IdentityId);

        //string user_in = "";
        Dictionary<string, int> dicBetNum = new Dictionary<string, int>();
       

        string userIds = string.Empty;
        //取得站長關注的玩家字典
        for (int i = 0; i < MemberClassList.Count; i++)
        {

            string strUserId = MemberClassList[i].ToString();
            userIds += strUserId + ",";


            if (dicAgent.ContainsKey(strUserId))
                continue;

            dicAgent.Add(strUserId, MemberClassList[i].ToString());
            ReportAgent agentReport = new ReportAgent();


            if (!dicReportAgent.ContainsKey(strUserId))
            {
                dicReportAgent.Add(strUserId, agentReport);

            }
        }
        //只查詢此站的下一級tdfdmoney
        //if (userIds.Length > 0)
        //{
        //    userIds = userIds.Substring(0, userIds.Length - 1);
        //    userIds = "and user_id in (" + userIds + ")";
        //}
        //else
        //{
        //    userIds = "and user_id in (0)";
        //}


        DBcon.dspModel dspModel = new DBcon.dspModel();
        dspModel.paramsDt = dtMemberClass;
        dspModel.identityid = IdentityId;
        dspModel.strStartDate = StartTime;
        dspModel.strEndDate = EndTime;
        //投注
        //string agent_order_str = "select [dt_lottery_proxy_report].identityid,user_id as UserId,isnull(sum(tzmoney),0) as BetMoney  from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_lottery_proxy_report]  where dt_lottery_proxy_report.identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  " + userIds + "  group by [dt_lottery_proxy_report].identityid,user_id  ";
        DataTable dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_lottery_proxy_report", DBtype, "agent_order_conn", dspModel);
        //DataTable dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_order_str, DBtype, "report_share", sqlcmdpar2);
        //充值
        //string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as DiscountMoney   from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_in_proxy_report] with(nolock)   where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate  " + userIds + "  group by identityid,user_id  having sum(hdljmoney)>0 ";
        DataTable dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_user_in_proxy_report", DBtype, "agent_in_conn", dspModel);
        //DataTable dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_in_str, DBtype, "report_share", sqlcmdpar);
        //返點
        //string agent_point_str = "select identityid,user_id as UserId,isnull(sum(tdfdmoney),0) as RebateMoney    from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_get_point_proxy_report] with(nolock)   where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate  " + userIds + "  group by identityid,user_id ";
        //DataTable dt_agentpoint = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_point_str, DBtype, "report_share", sqlcmdpar);
        DataTable dt_agentpoint = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_user_get_point_proxy_report", DBtype, "agent_in_conn", dspModel);
        //中獎
        //string agent_win_str = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as WinMoney   from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report] with(nolock)  where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate " + userIds + "   group by identityid,user_id ";
        //DataTable dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_win_str, DBtype, "report_share", sqlcmdpar);
        DataTable dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_lottery_winning_proxy_report", DBtype, "agent_out_conn", dspModel);


        string userIdIn = "";
        //投注datatable實體物件
        for (int i = 0; i < dt_agentorder.Rows.Count; i++)
        {
            userIdIn += dt_agentorder.Rows[i]["UserId"].ToString().TrimEnd() + ",";
            AgentReport agentreport = new AgentReport();
            reporthelper.ConvertDataRowToModel_sql(dt_agentorder.Rows[i], agentreport);
            agentreport.IsAgent = false;
           
            agentReportList.Add(agentreport);

        }
        if(userIdIn.Length>0)
        {
            userIdIn = userIdIn.Substring(0, userIdIn.Length-1);
        }
        else
        {
            userIdIn = "0";
        }

        System.Threading.Tasks.Task agentorderTask = new System.Threading.Tasks.Task(
           () =>
           {

                //投注人数
                List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
               sqlcmdlist_online.Add(pa_identityid1);
               sqlcmdlist_online.Add(pa_StartDate1);
               sqlcmdlist_online.Add(pa_EndDate1);
               SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();
               Dictionary<string, int> dicBetNum2 = new Dictionary<string, int>();

                //string query_str = "select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from dbo.dt_users_class where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id  union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from dbo.dt_users_class where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate      group by v2.user_id   ) as view2  group by father_id OPTION (RECOMPILE)";
                //DataTable usersDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, DBtype, "report_share", sqlcmdpar_online);
                //string query_str = "select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [dbo].[dt_lottery_record] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from dbo.dt_users_class where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id  union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [dbo].[dt_lottery_record]   where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from dbo.dt_users_class  where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate      group by v2.user_id   ) as view2  group by father_id OPTION (RECOMPILE,MAXDOP 2)";
                string query_str = $@"SELECT father_id, 
                                           Count(DISTINCT ( v1.user_id )) AS count 
                                    FROM   (SELECT distinct  user_id 
                                            FROM   dt_lottery_record 
                                            WHERE  add_date >= @strStartDate 
                                                   AND add_date < @strEndDate 
                                                   AND identityid = @identityid) AS v1 
                                           LEFT JOIN (SELECT father_id, 
                                                             user_id 
                                                      FROM   dt_users_class 
                                                      WHERE  identityid = @identityid 
                                                             AND father_id IN ( {userIdIn} )  
                                                      UNION 
                                                      SELECT user_id AS father_id, 
                                                             user_id 
                                                      FROM   dt_users_class 
                                                      WHERE  user_id IN ( {userIdIn} )) 
			                                    AS v2 
                                                  ON v1.user_id = v2.user_id 
                                    GROUP  BY father_id ";
               DataTable usersDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, DBtype, "report_order_conn", sqlcmdpar_online);
               dicBetNum = GetAgentUserData_New(usersDataTable);

               
           }
           );
        agentorderTask.Start();
        //datatable實體物件
        for (int i = 0; i < agentReportList.Count; i++)
        {
            int listAgentId_str = agentReportList[i].UserId;

            if (dt_agentpoint.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
            {
                reporthelper.ConvertDataRowToModel_sql(dt_agentpoint.Select("UserId = '" + listAgentId_str + "'")[0], agentReportList[i]);
            }



        }


        //datatable實體物件
        for (int i = 0; i < agentReportList.Count; i++)
        {
            int listAgentId_str = agentReportList[i].UserId;

            if (dt_agentwin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
            {
                reporthelper.ConvertDataRowToModel_sql(dt_agentwin.Select("UserId = '" + listAgentId_str + "'")[0], agentReportList[i]);
            }
            if (dt_agentin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
            {
                reporthelper.ConvertDataRowToModel_sql(dt_agentin.Select("UserId = '" + listAgentId_str + "'")[0], agentReportList[i]);
            }

        }

        
        string user_show_in = "";
        ////補齊全部
        for (int i = 0; i < agentReportList.Count; i++)
        {
            user_show_in += agentReportList[i].UserId + ",";
        }
        if (user_show_in.Length > 0)
        {
            user_show_in = user_show_in.Substring(0, user_show_in.Length - 1);
            user_show_in = " and dt_users.id in (" + user_show_in + ") ";
        }
        else
        {
            user_show_in = " and dt_users.id in (0) ";
        }
        string logingroup_str = "select distinct user_id,user_class from dt_users_class with(index([user_id])) where identityid=@identityid and user_id = @LoginId";// + LoginId.ToString();
        DataTable dddd = DBcon.DbHelperSQL.GetQueryFromShare(logingroup_str, sqlcmdpar);
        int class_n = 0;
        if (dddd.Rows.Count > 0)
        {
            class_n = Convert.ToInt32(dddd.Rows[0]["user_class"]);
        }
        int admin_agent_class = Convert.ToInt32(class_n);
        // int admin_agent_class = Convert.ToInt32(dicAgentClass[LoginId.ToString()]);

        List<SqlParameter> listdetail = new List<SqlParameter>();
        listdetail.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });

        string user_detail = "select dt_users.identityid,dt_users.id as user_id,dt_users.user_name,v1.user_class from dt_users with(index([PK_dt_users])) left join  (  select distinct user_id,user_class from dt_users_class with(index([user_id])) where identityid=@identityid) as v1 on dt_users.id = v1.user_id where identityid=@identityid and flog=0 " + user_show_in;
        DataTable report_usershow = DBcon.DbHelperSQL.GetQueryFromShare(user_detail, listdetail.ToArray());

        agentorderTask.Wait();

        int remove_index = 0;
        int remove_user_index = 0;
        bool remove_signal = false;
        bool remove_user = false;
        int x = 0;
        try
        {
            for (int i = 0; i < agentReportList.Count; i++)
            {
                x = i;
                string strAgentId = agentReportList[i].UserId.ToString();

                //投注人数
                if (dicBetNum.ContainsKey(strAgentId))
                {
                    string isagent = AgentClassList[strAgentId].Split(';')[0];
                    string username = AgentClassList[strAgentId].Split(';')[1];
                    //ClassNum = Convert.ToInt32(dicAgentClass[strAgentId]) - admin_agent_class;
                    ClassNum = Convert.ToInt32(report_usershow.Select("user_id = '" + strAgentId + "'")[0]["user_class"]) - admin_agent_class;
                    agentReportList[i].BetNum = dicBetNum[strAgentId];
                    // agentReportList[i].UserType = ClassNum + "级代理";
                    agentReportList[i].UserName = username;

                    if (isagent == "True")
                    {
                        agentReportList[i].UserType = ClassNum + "级代理";
                        agentReportList[i].IsAgent = true;
                    }
                    else if (isagent == "False")
                    {
                        agentReportList[i].UserType = ClassNum + "级玩家";
                        agentReportList[i].IsAgent = false;
                    }
                    if (LoginId == agentReportList[i].UserId)
                    {
                        agentReportList[i].UserType = "总代";
                        remove_index = i;
                        remove_signal = true;
                    }
                    if (UserId == agentReportList[i].UserId)
                    {

                        remove_user_index = i;
                        remove_user = true;
                    }


                }
                else
                {
                    string isagent = AgentClassList[strAgentId].Split(';')[0];
                    string username = AgentClassList[strAgentId].Split(';')[1];
                    //ClassNum = Convert.ToInt32(dicAgentClass[strAgentId]) - admin_agent_class;
                    ClassNum = Convert.ToInt32(report_usershow.Select("user_id = '" + strAgentId + "'")[0]["user_class"]) - admin_agent_class;
                    //agentReportList[i].BetNum = dicBetNum[strAgentId];
                    // agentReportList[i].UserType = ClassNum + "级代理";
                    agentReportList[i].UserName = username;
                    //// if (dicBetNum[strAgentId] > 1)
                    //{

                    //}
                    if (isagent == "True")
                    {
                        agentReportList[i].UserType = ClassNum + "级代理";
                        agentReportList[i].IsAgent = true;
                    }
                    else if (isagent == "False")
                    {
                        agentReportList[i].UserType = ClassNum + "级玩家";
                        agentReportList[i].IsAgent = false;
                    }
                    //if (LoginId == agentReportList[i].UserId)
                    //{
                    //    agentReportList[i].UserType = "总代";
                    //    remove_index = i;
                    //    remove_signal = true;
                    //}
                    //if (UserId == agentReportList[i].UserId)
                    //{

                    //    remove_user_index = i;
                    //    remove_user = true;
                    //}
                }

                //盈利=中奖-投注+团队返点+团队活动优惠
                agentReportList[i].ProfitMoney = agentReportList[i].WinMoney - agentReportList[i].BetMoney + agentReportList[i].DiscountMoney + agentReportList[i].RebateMoney;
            }
        }
        catch (Exception e)
        {

        }
       
        if (remove_signal == true)
        {
            agentReportList.RemoveAt(remove_index);
        }
        if (remove_user == true && UserId != LoginId)
        {
            if (remove_signal == true)
            {
                remove_user_index = remove_user_index - 1;
            }
            agentReportList.RemoveAt(remove_user_index);
        }









        //判断是否有数据。
        if (agentReportList.Count < 1)
        {
            ResultInfo.Code = 1;
            ResultInfo.BackData = agentReportList;
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
        //List<dt_report_inoutaccount> reportMemberList = new List<dt_report_inoutaccount>();
        //if (MemberClassList.Count > 0)
        //{
        //    //reportMemberList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.agent_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        //    ////  reportMemberList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, "", "").Where(m => MemberClassList.Contains(m.user_id)).ToList();
        //}
        // dt_report_agent reportAgent = new dt_report_agent();



        return string.Empty;
    }

    /// <summary>
    /// 測試區下級代理查詢
    /// </summary>
    /// <param name="MemberClassList"></param>
    /// <param name="IdentityId"></param>
    /// <param name="UserId"></param>
    /// <param name="StartTime"></param>
    /// <param name="EndTime"></param>
    /// <param name="UserName"></param>
    /// <param name="ClassNum"></param>
    /// <param name="LoginId"></param>
    /// <param name="UsersList"></param>
    /// <param name="agentReportList"></param>
    /// <param name="ResultInfo"></param>
    /// <param name="AgentClassList"></param>
    /// <returns></returns>
    private string GetTestAgentData(List<int> MemberClassList, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum,
    int LoginId, DataTable UsersList, List<AgentReport> agentReportList, ResultInfoT<List<AgentReport>> ResultInfo, Dictionary<string, string> AgentClassList)
    {


        ReportHelper reporthelper = new ReportHelper();
        //connectstring
        string agent_in_conn = DBcon.DBcontring.Agent_In_Conn;
        string agent_out_conn = DBcon.DBcontring.Agent_Out_Conn;
        string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;
        string report_order_conn = DBcon.DBcontring.Report_Order_Conn;

        //報表
        List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
        sqlcmdlist.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        sqlcmdlist.Add(new SqlParameter("@strStartDate", StartTime));
        sqlcmdlist.Add(new SqlParameter("@strEndDate", EndTime));
        SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

        //線上
        List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
        SqlParameter pa_identityid1 = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
        pa_identityid1.Value = IdentityId;
        SqlParameter pa_StartDate1 = new SqlParameter("@strStartDate", SqlDbType.DateTime);
        pa_StartDate1.Value = StartTime.ToString("yyyy-MM-dd");
        SqlParameter pa_EndDate1 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        pa_EndDate1.Value = Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd");
        sqlcmdlist_online.Add(pa_identityid1);
        sqlcmdlist_online.Add(pa_StartDate1);
        sqlcmdlist_online.Add(pa_EndDate1);
        SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

        //站
        List<SqlParameter> sqlcmdlist_identityid = new List<SqlParameter>();
        sqlcmdlist_identityid.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        SqlParameter[] sqlcmdpar_identityid = sqlcmdlist_identityid.ToArray();

        //站長
        List<SqlParameter> sqlcmdlist_alicare = new List<SqlParameter>();
        sqlcmdlist_alicare.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        sqlcmdlist_alicare.Add(new SqlParameter("@userid", UserId));
        SqlParameter[] sqlcmdpar_care = sqlcmdlist_alicare.ToArray();

        Dictionary<string, string> dicAgent = new Dictionary<string, string>();
        Dictionary<string, ReportAgent> dicReportAgent = new Dictionary<string, ReportAgent>();

        //判斷分庫
        int DBtype = SqlHelper.SwitchDBtype(IdentityId);

        string user_in = "";
        Dictionary<string, int> dicBetNum = new Dictionary<string, int>();
        System.Threading.Tasks.Task agentorderTask = new System.Threading.Tasks.Task(
            () =>
            {

                //投注人数

                string query_str = "select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_record_test] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id "
                              // + " union all  select user_id as father_id,count(distinct(user_id)) count from [dbo].[dt_lottery_record]  left outer join (  select father_id from [dt_users_class]  where identityid=@identityid ) as v2 on [dt_lottery_record].User_Id = v2.father_id where [dt_lottery_record].identityid =@identityid and [dt_lottery_record].add_date>=@strStartDate and [dt_lottery_record].add_date<@strEndDate   and v2.father_id is null  group by identityid,user_id "
                              + " union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_record_test] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from [dbo].[dt_users_class] where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid   and v1.add_date>=@strStartDate and v1.add_date<@strEndDate     group by v2.user_id  "
                              + " ) as view2  group by father_id OPTION (RECOMPILE,MAXDOP 2)";
                DataTable usersDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, DBtype, "report_share", sqlcmdpar_online);
                dicBetNum = GetAgentUserData_New(usersDataTable);

                for (int i = 0; i < dicBetNum.Count; i++)
                {
                    if (!string.IsNullOrEmpty(usersDataTable.Rows[i]["father_id"].ToString()))
                        user_in += usersDataTable.Rows[i]["father_id"] + ",";
                }
                if (user_in.Length > 0)
                {
                    user_in = user_in.Substring(0, user_in.Length - 1);
                    user_in = "and user_id in (" + user_in + ")";
                }
                else
                {
                    user_in = "and user_id in (0)";
                }

            }
            );
        agentorderTask.Start();

        string userIds = string.Empty;
        //取得站長關注的玩家字典
        for (int i = 0; i < MemberClassList.Count; i++)
        {

            string strUserId = MemberClassList[i].ToString();
            userIds += strUserId + ",";
            if (dicAgent.ContainsKey(strUserId))
                continue;

            dicAgent.Add(strUserId, MemberClassList[i].ToString());
            ReportAgent agentReport = new ReportAgent();


            if (!dicReportAgent.ContainsKey(strUserId))
            {
                dicReportAgent.Add(strUserId, agentReport);

            }
        }
        //只查詢此站的下一級tdfdmoney
        if (userIds.Length > 0)
        {
            userIds = userIds.Substring(0, userIds.Length - 1);
        }
        else
        {
            userIds = "0";
        }


        string agent_order_str = "select [dt_lottery_proxy_report_test].identityid,user_id as UserId,isnull(sum(tzmoney),0) as BetMoney  from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_proxy_report_test]  where dt_lottery_proxy_report_test.identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate and user_id in (" + userIds + ") group by [dt_lottery_proxy_report_test].identityid,user_id  ";
        DataTable dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_order_str, DBtype, "report_share", sqlcmdpar);
        //string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as DiscountMoney,isnull(sum(tdfdmoney),0) as RebateMoney    from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_get_point_proxy_report] with(nolock)   where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate and user_id in (" + userIds + ")  group by identityid,user_id  having sum(hdljmoney)>0 or sum(dlfdmoney) >0";
        string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as DiscountMoney,isnull(sum(tdfdmoney),0) as RebateMoney    from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_in_proxy_report_test] with(nolock)   where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate and user_id in (" + userIds + ")  group by identityid,user_id  having sum(hdljmoney)>0 or sum(dlfdmoney) >0";
        DataTable dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_in_str, DBtype, "report_share", sqlcmdpar);
        string agent_win_str = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as WinMoney   from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report_test] with(nolock)  where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate and user_id in (" + userIds + ")  group by identityid,user_id ";
        DataTable dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_win_str, DBtype, "report_share", sqlcmdpar);


        //投注datatable實體物件
        for (int i = 0; i < dt_agentorder.Rows.Count; i++)
        {
            AgentReport agentreport = new AgentReport();
            reporthelper.ConvertDataRowToModel_sql(dt_agentorder.Rows[i], agentreport);
            agentreport.IsAgent = false;
            agentReportList.Add(agentreport);

        }
        //返點活動datatable實體物件

        for (int i = 0; i < dt_agentin.Rows.Count; i++)
        {
            AgentReport agentreport = new AgentReport();
            reporthelper.ConvertDataRowToModel_sql(dt_agentin.Rows[i], agentreport);
            // agentreport.UserType = ClassNum + "级代理";
            agentreport.IsAgent = false;
            int userid = Convert.ToInt32(dt_agentin.Rows[i]["UserId"]);
            //如果原本沒有的userid,就在加入
            if (agentReportList.Where(m => m.UserId == userid).Count() == 0)
            {
                agentReportList.Add(agentreport);
            }

        }



        //datatable實體物件
        for (int i = 0; i < agentReportList.Count; i++)
        {
            int listAgentId_str = agentReportList[i].UserId;

            if (dt_agentwin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
            {
                reporthelper.ConvertDataRowToModel_sql(dt_agentwin.Select("UserId = '" + listAgentId_str + "'")[0], agentReportList[i]);
            }
            if (dt_agentin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
            {
                reporthelper.ConvertDataRowToModel_sql(dt_agentin.Select("UserId = '" + listAgentId_str + "'")[0], agentReportList[i]);
            }

        }



        //投注人数
        //Dictionary<string, int> dicBetNum = new Dictionary<string, int>();
        //Dictionary<string, int> dicBetNum2 = new Dictionary<string, int>();
        //string query_str = "select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_record_test] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id "
        //              // + " union all  select user_id as father_id,count(distinct(user_id)) count from [dbo].[dt_lottery_record]  left outer join (  select father_id from [dt_users_class]  where identityid=@identityid ) as v2 on [dt_lottery_record].User_Id = v2.father_id where [dt_lottery_record].identityid =@identityid and [dt_lottery_record].add_date>=@strStartDate and [dt_lottery_record].add_date<@strEndDate   and v2.father_id is null  group by identityid,user_id "
        //              + " union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_record_test] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from [dbo].[dt_users_class] where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid   and v1.add_date>=@strStartDate and v1.add_date<@strEndDate     group by v2.user_id  "
        //              + " ) as view2  group by father_id OPTION (RECOMPILE)";
        //DataTable usersDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, report_share, sqlcmdpar_online);
        ////DataTable usersDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, report_order_conn, sqlcmdpar_identityid);
        //// DataTable usersDataTable2 = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str2, report_order_conn, sqlcmdpar_identityid);
        //dicBetNum = GetAgentUserData_New(usersDataTable);
        //string user_in = "";
        //for (int i = 0; i < dicBetNum.Count; i++)
        //{
        //    user_in += usersDataTable.Rows[i]["father_id"] + ",";
        //}
        //if (user_in.Length > 0)
        //{
        //    user_in = user_in.Substring(0, user_in.Length - 1);
        //    user_in = "and user_id in (" + user_in + ")";
        //}
        //else
        //{
        //    user_in = "and user_id in (0)";
        //}

        //  Dictionary<string, string> dicAgentData2 = GetAgentUserData(usersDataTable2);

        //string report_class_str = "select distinct user_id,user_class from [dt_users_class] where identityid=@identityid  " + user_in + "";
        //DataTable report_classtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(report_class_str, report_share, sqlcmdpar);
        //Dictionary<string, string> dicAgentClass = GetAgentUserClass(report_classtable);
        //if (!dicAgentClass.ContainsKey(LoginId.ToString()))
        //{
        //    dicAgentClass.Add(LoginId.ToString(), LoginId.ToString());
        //}

        //// 投注人数
        //for (int i = 0; i < report_betnumtable.Rows.Count; i++)
        //{
        //    string strUserId = report_betnumtable.Rows[i]["user_id"].ToString();
        //    ComputeNum(dicBetNum, strUserId, dicAgentData);
        ////    ComputeNum(dicBetNum2, strUserId, dicAgentData2);
        //}
        //for (int i = 0; i < usersDataTable.Rows.Count; i++)
        //{
        //    //dt_report_num report_num = new dt_report_num();
        //    //reporthelper.ConvertDataRowToModel_sql(report_betnumtable.Rows[i], report_num);
        //    //reportNumList.Add(report_num);
        //    //string strUserId = report_betnumtable.Rows[i]["father_id"].ToString();
        //    string strUserId = usersDataTable.Rows[i]["father_id"].ToString();
        //    int strUsercount = Convert.ToInt32(usersDataTable.Rows[i]["count"]);
        //    // ComputeNum(dicBetNum, strUserId, dicAgentData);
        //    dicBetNum.Add(strUserId, strUsercount);
        //}
        string user_show_in = "";
        ////補齊全部
        for (int i = 0; i < agentReportList.Count; i++)
        {
            user_show_in += agentReportList[i].UserId + ",";
        }
        if (user_show_in.Length > 0)
        {
            user_show_in = user_show_in.Substring(0, user_show_in.Length - 1);
            user_show_in = " and dt_users.id in (" + user_show_in + ") ";
        }
        else
        {
            user_show_in = " and dt_users.id in (0) ";
        }
        List<SqlParameter> listlogin = new List<SqlParameter>();
        listlogin.Add(new SqlParameter("@LoginId", LoginId));
        listlogin.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        string logingroup_str = "select distinct user_id,user_class from dt_users_class where identityid=@identityid and user_id = @LoginId";// + LoginId.ToString();
        DataTable dddd = DBcon.DbHelperSQL.GetQueryFromShare(logingroup_str, listlogin.ToArray());
        int class_n = 0;
        if (dddd.Rows.Count > 0)
        {
            class_n = Convert.ToInt32(dddd.Rows[0]["user_class"]);
        }
        int admin_agent_class = Convert.ToInt32(class_n);

        List<SqlParameter> listidentityid = new List<SqlParameter>();
        listidentityid.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });

        string user_detail = "select dt_users.identityid,dt_users.id as user_id,dt_users.user_name,v1.user_class from dt_users left join  (  select distinct user_id,user_class from dt_users_class where identityid=@identityid) as v1 on dt_users.id = v1.user_id where identityid=@identityid and flog='1' " + user_show_in;
        DataTable report_usershow = DBcon.DbHelperSQL.GetQueryFromShare(user_detail, listidentityid.ToArray());

        //int admin_agent_class = Convert.ToInt32(dicAgentClass[LoginId.ToString()]);
        agentorderTask.Wait();
        int remove_index = 0;
        int remove_user_index = 0;
        bool remove_signal = false;
        bool remove_user = false;
        for (int i = 0; i < agentReportList.Count; i++)
        {
            string strAgentId = agentReportList[i].UserId.ToString();

            //投注人数
            if (dicBetNum.ContainsKey(strAgentId))
            {
                string isagent = AgentClassList[strAgentId].Split(';')[0];
                string username = AgentClassList[strAgentId].Split(';')[1];
                //ClassNum = Convert.ToInt32(dicAgentClass[strAgentId]) - admin_agent_class;
                ClassNum = Convert.ToInt32(report_usershow.Select("user_id = '" + strAgentId + "'")[0]["user_class"]) - admin_agent_class;
                agentReportList[i].BetNum = dicBetNum[strAgentId];
                // agentReportList[i].UserType = ClassNum + "级代理";
                agentReportList[i].UserName = username;

                if (isagent == "True")
                {
                    agentReportList[i].UserType = ClassNum + "级代理";
                    agentReportList[i].IsAgent = true;
                }
                else if (isagent == "False")
                {
                    agentReportList[i].UserType = ClassNum + "级玩家";
                    agentReportList[i].IsAgent = false;
                }
                if (LoginId == agentReportList[i].UserId)
                {
                    agentReportList[i].UserType = "总代";
                    remove_index = i;
                    remove_signal = true;
                }
                if (UserId == agentReportList[i].UserId && UserId != LoginId)
                {
                    remove_user_index = i;
                    remove_user = true;
                }

            }
            else
            {
                string isagent = AgentClassList[strAgentId].Split(';')[0];
                string username = AgentClassList[strAgentId].Split(';')[1];
                //ClassNum = Convert.ToInt32(dicAgentClass[strAgentId]) - admin_agent_class;
                ClassNum = Convert.ToInt32(report_usershow.Select("user_id = '" + strAgentId + "'")[0]["user_class"]) - admin_agent_class;
                //agentReportList[i].BetNum = dicBetNum[strAgentId];
                // agentReportList[i].UserType = ClassNum + "级代理";
                agentReportList[i].UserName = username;
                //// if (dicBetNum[strAgentId] > 1)
                //{
                //}
                if (isagent == "True")
                {
                    agentReportList[i].UserType = ClassNum + "级代理";
                    agentReportList[i].IsAgent = true;
                }
                else if (isagent == "False")
                {
                    agentReportList[i].UserType = ClassNum + "级玩家";
                    agentReportList[i].IsAgent = false;
                }
                //if (LoginId == agentReportList[i].UserId)
                //{
                //    agentReportList[i].UserType = "总代";
                //    remove_index = i;
                //    remove_signal = true;
                //}
                //if (UserId == agentReportList[i].UserId)
                //{

                //    remove_user_index = i;
                //    remove_user = true;
                //}
            }

            //盈利=中奖-投注+团队返点+团队活动优惠
            agentReportList[i].ProfitMoney = agentReportList[i].WinMoney - agentReportList[i].BetMoney + agentReportList[i].DiscountMoney + agentReportList[i].RebateMoney;
        }
        if (remove_signal == true)
        {
            agentReportList.RemoveAt(remove_index);
        }
        if (remove_user == true && UserId != LoginId)
        {
            if (remove_signal == true)
            {
                remove_user_index = remove_user_index - 1;
            }
            agentReportList.RemoveAt(remove_user_index);
        }





        //判断是否有数据。
        if (agentReportList.Count < 1)
        {
            ResultInfo.Code = 1;
            ResultInfo.BackData = agentReportList;
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
        List<dt_report_inoutaccount> reportMemberList = new List<dt_report_inoutaccount>();
        if (MemberClassList.Count > 0)
        {
            //reportMemberList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.agent_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            ////  reportMemberList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, "", "").Where(m => MemberClassList.Contains(m.user_id)).ToList();
        }
        dt_report_agent reportAgent = new dt_report_agent();


        return string.Empty;
    }

    /// <summary>
    /// 获取正式会员数据
    /// </summary>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="StartTime">起始日期</param>
    /// <param name="EndTime">结束日期</param>
    /// <param name="UserName">用户名称</param>
    /// <param name="ClassNum">用户层级</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private void GetUserData(RedisClient redisClient, RedisClient redisClientKey, int UserId, string IdentityId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum, List<AgentReport> agentReportList)
    {
        //List<dt_report_inoutaccount> reportMemberList = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_inoutaccount> reportMemberList = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, UserId.ToString(), "").ToList();
        AgentReport agentReport = new AgentReport();
        agentReport.UserId = UserId;
        agentReport.UserName = UserName;
        agentReport.UserType = ClassNum + "级玩家";
        agentReport.IsAgent = false;
        for (int k = 0; k < reportMemberList.Count; k++)
        {
            agentReport.BetMoney += reportMemberList[k].Out_BettingAccount;
            agentReport.WinMoney += reportMemberList[k].Out_WinningAccount;
            agentReport.RebateMoney += reportMemberList[k].Out_RebateAccount;
            agentReport.DiscountMoney += reportMemberList[k].Out_ActivityDiscountAccount;
            agentReport.DiscountMoney += reportMemberList[k].Out_OtherDiscountAccount;
        }
        //盈利＝中奖总额－投注总额+活动优惠+返点
        agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
        agentReport.BetNum = 1;
        agentReportList.Add(agentReport);
    }
    /// <summary>
    /// 获取测试代理数据
    /// </summary>
    /// <param name="MemberClassList">会员所有层级用户ID</param>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="StartTime">开始时间</param>
    /// <param name="EndTime">结束时间</param>
    /// <param name="UserName">会员名称</param>
    /// <param name="ClassNum">代理层级</param>
    /// <param name="LoginId">登录账号ID</param>
    /// <param name="UsersList">用户数据</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private string GetTestAgentData(List<int> MemberClassList, RedisClient redisClient, RedisClient redisClientKey, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum,
           int LoginId, DataTable UsersList, List<AgentReport> agentReportList, ResultInfoT<List<AgentReport>> ResultInfo, Dictionary<int, int> AgentClassList)
    {
        // List<dt_report_test_agent> reportTestAgentList = redisClient.GetAll<dt_report_test_agent>().Where(m => m.identityid == IdentityId && AgentClassList.ToArray().Contains(m.user_id) && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_test_agent> reportTestAgentList = redisHelper.Search<dt_report_test_agent>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, "", "").Where(m => AgentClassList.ContainsKey(m.user_id)).ToList();
        //判断是否有数据。
        if (reportTestAgentList.Count < 1)
        {
            ResultInfo.Code = 1;
            ResultInfo.BackData = agentReportList;
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
        List<dt_report_test_inoutaccount> reportMemberList = new List<dt_report_test_inoutaccount>();
        if (MemberClassList.Count > 0)
        {
            //reportMemberList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.identityid == IdentityId && m.agent_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
            reportMemberList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, "", "").Where(m => MemberClassList.Contains(m.user_id)).ToList();
        }
        dt_report_test_agent reportTestAgent = new dt_report_test_agent();
        Dictionary<int, int> dicAgent = new Dictionary<int, int>();
        #region 代理数据汇总
        //var reportNumList = redisClient.GetAll<dt_report_test_num>().Where(m => m.identityid == IdentityId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_test_num> reportNumList = redisHelper.Search<dt_report_test_num>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, "", "").ToList();
        decimal isLower = 0m;
        for (int i = 0; i < reportTestAgentList.Count; i++)
        {
            if (dicAgent.ContainsKey(reportTestAgentList[i].user_id))
            {
                continue;
            }
            //Log.Info(reportTestAgentList[i].user_id.ToString());
            dicAgent.Add(reportTestAgentList[i].user_id, 1);
            //Log.Info("OK");
            AgentReport agentReport = new AgentReport();
            agentReport.UserId = reportTestAgentList[i].user_id;
            agentReport.UserName = UserName;
            agentReport.UserType = ClassNum + "级代理";
            agentReport.IsAgent = false;
            List<dt_report_test_agent> Items = reportTestAgentList.Where(x => x.user_id == reportTestAgentList[i].user_id).ToList();
            for (int k = 0; k < Items.Count; k++)
            {
                agentReport.BetMoney += Items[k].Out_BettingAccount;
                agentReport.BetMoney += Items[k].Lower_Out_BettingAccount;
                isLower += Items[k].Lower_Out_BettingAccount;
                agentReport.WinMoney += Items[k].Out_WinningAccount;
                agentReport.WinMoney += Items[k].Lower_Out_WinningAccount;
                agentReport.RebateMoney += Items[k].Out_RebateAccount;
                agentReport.RebateMoney += Items[k].Lower_Out_RebateAccount;
                agentReport.DiscountMoney += Items[k].ActivityDiscountMoney;
                agentReport.DiscountMoney += Items[k].Lower_ActivityDiscountMoney;
                agentReport.DiscountMoney += Items[k].OtherDiscountMoney;
                agentReport.DiscountMoney += Items[k].Lower_OtherDiscountMoney;
            }
            //下级投注汇总大于0时,表示有下级在投注
            agentReport.BetNum = 0;
            if (isLower > 0 && reportTestAgentList[i].user_id != UserId)
            {
                agentReport.IsAgent = true;
                //agentReport.BetNum = 1;
            }
            if (reportTestAgentList[i].user_id != UserId)
            {
                if (UserId != LoginId)
                {
                    agentReport.UserType = ClassNum + 1 + "级代理";
                }
                DataRow[] userDataRows = UsersList.Select("id=" + reportTestAgentList[i].user_id);
                if (userDataRows != null && userDataRows.Length > 0)
                {
                    agentReport.UserName = userDataRows[0]["user_name"].ToString();
                }
            }
            else
            {
                if (LoginId == reportTestAgentList[i].user_id)
                {
                    agentReport.UserType = "总代";
                    continue;
                }
                agentReport.BetMoney = 0;
                agentReport.WinMoney = 0;
                agentReport.RebateMoney = 0;
                agentReport.DiscountMoney = 0;
                for (int k = 0; k < Items.Count; k++)
                {
                    agentReport.BetMoney += Items[k].Out_BettingAccount;
                    agentReport.WinMoney += Items[k].Out_WinningAccount;
                    agentReport.RebateMoney += Items[k].Out_RebateAccount;
                    agentReport.DiscountMoney += Items[k].ActivityDiscountMoney;
                    agentReport.DiscountMoney += Items[k].OtherDiscountMoney;
                }
                if (isLower != agentReport.BetMoney)
                {
                    agentReport.BetNum = 1;
                }
            }
            isLower = 0;
            //盈利＝中奖总额－投注总额+活动优惠+返点
            agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;

            if (agentReport.ProfitMoney > 0 || agentReport.WinMoney > 0 || agentReport.BetMoney > 0 || agentReport.DiscountMoney > 0 || agentReport.RebateMoney > 0)
            {
                if (agentReport.BetNum == 0)
                {
                    //查询投注人数
                    string strSql = "select user_id from dt_users_class where identityid='" + IdentityId + "' and father_id=" + agentReport.UserId;
                    DataTable userClassDataTable = DBcon.DbHelperSQL.GetQueryFromCongku(strSql,1,null);
                    Dictionary<int, int> ClassListss = new Dictionary<int, int>();
                    if (!ClassListss.ContainsKey(agentReport.UserId))
                    {
                        ClassListss.Add(agentReport.UserId, agentReport.UserId);
                    }
                    for (int k = 0; k < userClassDataTable.Rows.Count; k++)
                    {
                        int strUserId = Convert.ToInt32(userClassDataTable.Rows[k]["user_id"]);
                        if (ClassListss.ContainsKey(strUserId))
                        {
                            continue;
                        }
                        ClassListss.Add(strUserId, strUserId);
                    }
                    //投注人数
                    List<dt_report_test_num> tempreportAgentList = reportNumList.Where(m => m.identityid == IdentityId && ClassListss.ContainsKey(m.user_id) && m.Out_BettingAccount_Num > 0 && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
                    agentReport.BetNum = tempreportAgentList.GroupBy(m => m.user_id).Count();
                }
                if (agentReport.BetMoney == 0)
                {
                    agentReport.BetNum = 0;
                }
                agentReportList.Add(agentReport);
            }
        }
        #endregion
        #region  会员数据汇总
        for (int i = 0; i < reportMemberList.Count; i++)
        {
            if (dicAgent.ContainsKey(reportMemberList[i].user_id))
            {
                continue;
            }
            //Log.Info(reportTestAgentList[i].user_id.ToString());
            dicAgent.Add(reportMemberList[i].user_id, 1);
            //Log.Info("OK");
            AgentReport agentReport = new AgentReport();
            agentReport.UserId = reportMemberList[i].user_id;
            agentReport.UserName = UserName;
            if (reportMemberList[i].user_id != UserId)
            {
                DataRow[] userDataRows = UsersList.Select("id=" + reportMemberList[i].user_id);
                if (userDataRows != null && userDataRows.Length > 0)
                {
                    agentReport.UserName = userDataRows[0]["user_name"].ToString();
                }
            }
            agentReport.UserType = ClassNum + "级玩家";
            agentReport.IsAgent = false;
            List<dt_report_test_inoutaccount> Items = reportMemberList.Where(x => x.user_id == reportMemberList[i].user_id).ToList();
            for (int k = 0; k < Items.Count; k++)
            {
                agentReport.BetMoney += Items[k].Out_BettingAccount;
                agentReport.WinMoney += Items[k].Out_WinningAccount;
                agentReport.RebateMoney += Items[k].Out_RebateAccount;
                agentReport.DiscountMoney += Items[k].Out_ActivityDiscountAccount;
                agentReport.DiscountMoney += Items[k].Out_OtherDiscountAccount;
            }
            //盈利＝中奖总额－投注总额+活动优惠+返点
            agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
            agentReport.BetNum = 1;
            agentReportList.Add(agentReport);
        }
        #endregion
        return string.Empty;
    }
    /// <summary>
    /// 获取测试会员数据
    /// </summary>
    /// <param name="redisClient">Redis操作类</param>
    /// <param name="UserId">用户ID</param>
    /// <param name="IdentityId">站长ID</param>
    /// <param name="StartTime">起始日期</param>
    /// <param name="EndTime">结束日期</param>
    /// <param name="UserName">用户名称</param>
    /// <param name="ClassNum">用户层级</param>
    /// <param name="agentReportList">代理报表汇总数据</param>
    private void GetTestUserData(RedisClient redisClient, RedisClient redisClientKey, int UserId, string IdentityId, DateTime StartTime, DateTime EndTime, string UserName, int ClassNum, List<AgentReport> agentReportList)
    {
        //List<dt_report_test_inoutaccount> reportTestMemberList = redisClient.GetAll<dt_report_test_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        List<dt_report_test_inoutaccount> reportTestMemberList = redisHelper.Search<dt_report_test_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, IdentityId, UserId.ToString(), "").ToList();
        AgentReport agentReport = new AgentReport();
        agentReport.UserId = UserId;
        agentReport.UserName = UserName;
        agentReport.UserType = ClassNum + "级玩家";
        agentReport.IsAgent = false;
        for (int k = 0; k < reportTestMemberList.Count; k++)
        {
            agentReport.BetMoney += reportTestMemberList[k].Out_BettingAccount;
            agentReport.WinMoney += reportTestMemberList[k].Out_WinningAccount;
            agentReport.RebateMoney += reportTestMemberList[k].Out_RebateAccount;
            agentReport.DiscountMoney += reportTestMemberList[k].Out_ActivityDiscountAccount;
            agentReport.DiscountMoney += reportTestMemberList[k].Out_OtherDiscountAccount;
        }
        //盈利＝中奖总额－投注总额+活动优惠+返点
        agentReport.ProfitMoney = agentReport.WinMoney - agentReport.BetMoney + agentReport.DiscountMoney + agentReport.RebateMoney;
        agentReport.BetNum = 1;
        agentReportList.Add(agentReport);
    }
    /// <summary>
    /// 前台获取中奖总额前10名排行榜－－－－－－前台的[获取昨日中奖总额前十__昨日奖金榜   GetWinMoneyInTop]的功能
    /// </summary>
    /// <param name="context"></param>
    /// 
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetReportWinMoneyInTop(DateTime datetime)
    {
        string Date = datetime.ToShortDateString();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "datetime=" + datetime;
        try
        {
            st.Reset();
            st.Start();
            ReportHelper reporthelper = new ReportHelper();
            string strBuff = GetWinMoneyInTop(Date);
            ResultInfoT<List<WinMoneyInTop>> ResultInfo = JsonMapper.ToObject<ResultInfoT<List<WinMoneyInTop>>>(strBuff);
            if (ResultInfo.Code == 1)
            {
                List<WinMoneyInTop> AjaxTop = ResultInfo.BackData;
                int UserId = 0;
                string IdentityId = string.Empty;
                decimal BetMoney = 0m;
                decimal WinMoney = 0m;
                List<RankList> RankList = new List<RankList>();
                int Ranking = 0;
                for (int i = 0; i < AjaxTop.Count; i++)
                {
                    WinMoney = AjaxTop[i].WinMoney;
                    if (WinMoney > 0)
                    {
                        UserId = AjaxTop[i].UserId;
                        IdentityId = AjaxTop[i].IdentityId;
                        BetMoney = AjaxTop[i].BetMoney;
                        dt_users dt_users = new dt_users();
                        List<SqlParameter> UserSqlParamter = new List<SqlParameter>();
                        SqlParameter pa_usrid = new SqlParameter("@UserId", SqlDbType.Int);
                        pa_usrid.Value = UserId;
                        UserSqlParamter.Add(pa_usrid);
                        string instr = "select * from dt_users where id = @UserId";
                        DataTable intable = DBcon.DbHelperSQL.GetQueryFromShare(instr, UserSqlParamter.ToArray());
                        reporthelper.ConvertToMode(intable, dt_users);

                        if (dt_users != null)
                        {
                            Ranking++;
                            RankList PerOne = new RankList();
                            PerOne.UserId = UserId;
                            PerOne.UserName = dt_users.user_name == null ? "" : dt_users.user_name;
                            PerOne.UserPhoto = dt_users.avatar == null ? "" : dt_users.avatar;
                            PerOne.NickName = dt_users.nick_name == null ? dt_users.nick_name : dt_users.nick_name;
                            PerOne.Bonus = WinMoney.ToString("#0");
                            PerOne.Ranking = Ranking.ToString();
                            RankList.Add(PerOne);
                        }
                    }
                }
                string Keys = "RankingList" + DateTime.Now.ToString("dd");
                string Jsonc = string.Empty;
                Jsonc = JsonMapper.ToJson(RankList);
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_GetReportWinMoneyInTop", "GetReportWinMoneyInTop", st.ElapsedMilliseconds.ToString(), para);
                }
                return Jsonc;
            }
            else
            {
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_Error", "GetReportWinMoneyInTop", st.ElapsedMilliseconds.ToString(), para);
                }
                return null;
            }
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetReportWinMoneyInTop", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /* //昨日獎金榜
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetWinMoneyInTop_new(string strDay)
    {
        ResultInfoT<List<WinMoneyInTop>> resultInfo = new ResultInfoT<List<WinMoneyInTop>>();
        try
        {
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}

            dt_month_all_record dar = new dt_month_all_record();
            List<dt_report_inoutaccount> reportSummary_List = new List<dt_report_inoutaccount>();
            List<WinMoneyInTop> betMoneyInTopList = new List<WinMoneyInTop>();
            ReportHelper reporthelper = new ReportHelper();
            dt_user_in_verify duiv = new dt_user_in_verify();
            string identityid = "";
            //查询in表汇总

            List<SqlParameter> AliSqlParamter = new List<SqlParameter>();
            SqlParameter alipa_begindate = new SqlParameter("@begindate", SqlDbType.DateTime);
            alipa_begindate.Value = strDay;
            SqlParameter alipa_enddate = new SqlParameter("@enddate", SqlDbType.DateTime);
            alipa_enddate.Value = System.DateTime.Now.ToString("yyyy-MM-dd");
            AliSqlParamter.Add(alipa_begindate);
            AliSqlParamter.Add(alipa_enddate);

            string instr = "select top 10 user_id as UserId,identityid as IdentityId,isnull(sum(money),0) as  WinMoney from dt_lottery_winning_userrecord where  add_date>=@begindate and add_date <@enddate AND identityid <>'test' group by identityid,user_id order by isnull(sum(money),0) desc";
            DataTable intable = GetQueryFromReportSwitchbei(instr, report_in_conn, AliSqlParamter.ToArray());
            dt_report_inoutaccount reportSummary = new dt_report_inoutaccount();

            for (int i = 0; i < intable.Rows.Count; i++)
            {
                WinMoneyInTop wmit = new WinMoneyInTop();
                reporthelper.ConvertDataRowToModel(intable.Rows[i], wmit);
                betMoneyInTopList.Add(wmit);
            }
            //查询order表的数据
            string orderstr = "select user_id as UserId,identityid as IdentityId,isnull(sum(normal_money),0) as BetMoney from dt_lottery_record with(nolock) where  add_date>=@begindate and add_date<@enddate  AND identityid <>'test'  group by identityid,user_id";
            DataTable ordertable = GetQueryFromReportSwitchbei(orderstr, report_order_conn, AliSqlParamter.ToArray());
            for (int i = 0; i < betMoneyInTopList.Count; i++)
            {

                if (ordertable.Select("UserId='" + betMoneyInTopList[i].UserId + "'").Count() > 0)
                {
                    betMoneyInTopList[i].BetMoney = Convert.ToDecimal(ordertable.Select("UserId='" + betMoneyInTopList[i].UserId + "'")[0]["BetMoney"]);
                }
                else
                {
                    string orderstr_yesterday = string.Format("select user_id as UserId,identityid as IdentityId,isnull(sum(normal_money),0) as BetMoney from dt_lottery_record with(nolock) where  add_date>='{0}' and add_date<'{1}' AND identityid <>'test'   and user_id ='" + betMoneyInTopList[i].UserId + "' group by identityid,user_id", Convert.ToDateTime(strDay).AddDays(-1).ToString("yyyy-MM-dd"), strDay);
                    DataTable ordertable_yesterday = GetQueryFromReportSwitchbei(orderstr_yesterday, report_order_conn, null);
                    betMoneyInTopList[i].BetMoney = Convert.ToDecimal(ordertable_yesterday.Select("UserId='" + betMoneyInTopList[i].UserId + "'")[0]["BetMoney"]);
                }


            }


            resultInfo.Code = 1;
            resultInfo.DataCount = betMoneyInTopList.Count;
            resultInfo.BackData = betMoneyInTopList;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }
    */
    /// <summary>
    /// 昨日盈利榜
    /// </summary>
    /// <param name="strDay"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetWinMoneyInTop(string strDay)
    {
        ResultInfoT<List<WinMoneyInTop>> resultInfo = new ResultInfoT<List<WinMoneyInTop>>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "datetime=" + strDay;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            dt_month_all_record dar = new dt_month_all_record();
            List<dt_report_inoutaccount> reportSummary_List = new List<dt_report_inoutaccount>();
            List<WinMoneyInTop> betMoneyInTopList = new List<WinMoneyInTop>();
            ReportHelper reporthelper = new ReportHelper();
            //dt_user_in_verify duiv = new dt_user_in_verify();
            //查询in表汇总
            //等待獲取緩存
            while (Flash_Dic.gdt_ProfitLossTop.Rows.Count == 0)
            {
                Thread.Sleep(1000);
            }

            DataTable intable = Flash_Dic.gdt_ProfitLossTop;
            dt_report_inoutaccount reportSummary = new dt_report_inoutaccount();

            for (int i = 0; i < intable.Rows.Count; i++)
            {
                WinMoneyInTop wmit = new WinMoneyInTop();
                reporthelper.ConvertDataRowToModel(intable.Rows[i], wmit);
                betMoneyInTopList.Add(wmit);
            }

            resultInfo.Code = 1;
            resultInfo.DataCount = betMoneyInTopList.Count;
            resultInfo.BackData = betMoneyInTopList;
            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_GetWinMoneyInTop", "GetWinMoneyInTop", st.ElapsedMilliseconds.ToString(), para);
            }
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetWinMoneyInTop", st.ElapsedMilliseconds.ToString(), para);
            }
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }

    /// <summary>
    /// 前台後端操作時間
    /// </summary>
    /// <param name="datetime"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetReportGetWithdrawTime(DateTime datetime)
    {
        string Strdatetime = datetime.ToShortDateString();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "datetime=" + datetime;
        try
        {
            st.Reset();
            st.Start();
            string strBuff = GetWithdrawTime(Strdatetime);
            ResultInfoT<List<ServiceRateData>> ResultInfo = JsonMapper.ToObject<ResultInfoT<List<ServiceRateData>>>(strBuff);
            if (ResultInfo.Code == 1)
            {
                List<ServiceRateData> ServiceRateDataList = ResultInfo.BackData;
                string Keys = string.Empty;
                string Jsonc = string.Empty;
                string sss = DateTime.Now.ToString("dd");
                Dictionary<string, string> PerdayCache = new Dictionary<string, string>();
                for (int k = 0; k < ServiceRateDataList.Count; k++)
                {
                    Keys = ServiceRateDataList[k].IdentityId + sss;
                    ServiceTing ServiceTing = new ServiceTing();
                    ServiceTing.RechargeTime = ServiceRateDataList[k].RechargeTime.ToString();
                    ServiceTing.WithdrawTime = ServiceRateDataList[k].WithdrawTime.ToString();
                    Jsonc = JsonMapper.ToJson(ServiceTing);
                    if (PerdayCache.ContainsKey(Keys))
                        PerdayCache[Keys] = Jsonc;
                    else
                        PerdayCache.Add(Keys, Jsonc);
                    // new PerdayCache().SetVer(Keys, Jsonc, PerdayCache.DisServiceRateData);
                }
                string test = "feifan" + sss;
                Jsonc = PerdayCache[test].ToString();
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_GetReportGetWithdrawTime", "GetReportGetWithdrawTime", st.ElapsedMilliseconds.ToString(), para);
                }
                return Jsonc;
            }
            else
            {
                //记录错误信息
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_Error", "GetReportGetWithdrawTime", st.ElapsedMilliseconds.ToString(), para);
                }
                return null;
            }
        }
        catch (Exception ex)
        {
            //记录错误信息
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetReportGetWithdrawTime", st.ElapsedMilliseconds.ToString(), para);
            }
            return null;
        }
    }
    /// <summary>
    /// 前台获取出入款平均时间
    /// </summary>
    /// <param name="context"></param>
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetWithdrawTime(string strDay)
    {
        ResultInfoT<List<WithdrawTimeEntity>> resultInfo = new ResultInfoT<List<WithdrawTimeEntity>>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "datetime=" + strDay;
        try
        {
            st.Reset();
            st.Start();
            //验证是否有权访问  
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            //提现时间合计
            double fTotalWithdrawTime = 0;
            //提现次数
            double fWithdrawNum = 0;
            //充值时间合计
            double fTotalRechargeTime = 0;
            //充值次数
            double fRechargeTimeNum = 0;


            List<WithdrawTimeEntity> BTimeList = new List<WithdrawTimeEntity>();
            int wTime = 0;
            int rTime = 0;
            List<SqlParameter> AliSqlParamter = new List<SqlParameter>();
            SqlParameter alipa_begindate = new SqlParameter("@begindate", SqlDbType.DateTime);
            alipa_begindate.Value = strDay;
            SqlParameter alipa_enddate = new SqlParameter("@enddate", SqlDbType.DateTime);
            alipa_enddate.Value = Convert.ToDateTime(strDay).AddDays(1).ToString("yyyy-MM-dd");
            AliSqlParamter.Add(alipa_begindate);
            AliSqlParamter.Add(alipa_enddate);

            //分庫
            foreach (var dic in Flash_Dic.dic_tenant.Values.Distinct())
            {

                int DBtype = dic;

                DataTable tenantDataTable = DBcon.DbHelperSQL.GetQueryFromShare("select identityid from dt_tenant where DBtype=" + DBtype + " order by identityid", null);
                List<dt_report_inoutaccount> PerList = new List<dt_report_inoutaccount>();
                // string instr2 = "select IdentityId,sum(datediff(s, add_time, cztime)) as RechargeTime,count(identityid) as cznumber from [srv_lnk_total_in].[dafacloud].[dbo].dt_user_in_verify   where  type=1 and state=1 and cztime >=@begindate and cztime <@enddate group by identityid";
               // DataTable intable2 = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr2, DBtype, "report_share", AliSqlParamter.ToArray());
                string instr = " select identityid,operattime as RechargeTime,czcount from  [srv_lnk_total_in].[dafacloud].[dbo].dt_user_in_sum where add_date>=@begindate and add_date<@enddate";
                DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", AliSqlParamter.ToArray());
                //string outstr = "select IdentityId,sum(datediff(s, add_time, cztime)) as WithdrawTime,count(identityid) as txnumber from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_out_verify   where  type2 in(3, 9, 10) and state = 1 and cztime >= @begindate and cztime < @enddate group by identityid";
                string outstr = "select identityid,opearttime as WithdrawTime,txcount from [srv_lnk_total_out].[dafacloud].[dbo].dt_user_out_sum   where   add_date >= @begindate and add_date < @enddate ";
                DataTable outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, DBtype, "report_share", AliSqlParamter.ToArray());
                FormatHelper formatHelper = new FormatHelper();


                for (int i = 0; i < tenantDataTable.Rows.Count; i++)
                {
                    string identityid = tenantDataTable.Rows[i]["identityid"].ToString().TrimEnd();
                    WithdrawTimeEntity withdrawTime = new WithdrawTimeEntity();
                    if (intable.Select("IdentityId = '" + identityid + "'").Count() > 0)
                    {

                        fTotalRechargeTime = Convert.ToDouble(intable.Select("IdentityId = '" + identityid + "'")[0]["RechargeTime"]);
                        fRechargeTimeNum = Convert.ToDouble(intable.Select("IdentityId = '" + identityid + "'")[0]["czcount"]);
                        rTime = formatHelper.GetSecond(fTotalRechargeTime / fRechargeTimeNum);
                        withdrawTime.RechargeTime = rTime;
                    }
                    if (outtable.Select("IdentityId = '" + identityid + "'").Count() > 0)
                    {
                        fTotalWithdrawTime = Convert.ToDouble(Convert.ToDecimal(outtable.Select("IdentityId = '" + identityid + "'")[0]["WithdrawTime"]));
                        fWithdrawNum = Convert.ToDouble(outtable.Select("IdentityId = '" + identityid + "'")[0]["txcount"]);
                        wTime = formatHelper.GetSecond(fTotalWithdrawTime / fWithdrawNum);
                        withdrawTime.WithdrawTime = wTime;

                    }
                    withdrawTime.IdentityId = identityid;
                    BTimeList.Add(withdrawTime);
                }
            }


            st.Stop();
            lock (logmsg)
            {
                logmsg.CreateErrorLogTxt("Before_GetWithdrawTime", "GetWithdrawTime", st.ElapsedMilliseconds.ToString(), para);
            }
            resultInfo.Code = 1;
            resultInfo.BackData = BTimeList;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetWithdrawTime", st.ElapsedMilliseconds.ToString(), para);
            }
            resultInfo.Code = -1;
            resultInfo.StrCode = ex.Message;
            return LitJson.JsonMapper.ToJson(resultInfo);
        }
    }



    /// <summary>
    /// 前台代理报表数据xt－－－－－－前台的[代理报表  GetAgencyHender]的功能
    /// </summary>
    /// <param name="context"></param>

    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetAgencyHender(string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, int IsAgent, int flog)
    {
        ResultInfoT<AgentHender> ResultInfo = new ResultInfoT<AgentHender>();
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        ObjectCache cache = MemoryCache.Default;
        string filecontents = IdentityId + UserId + StartTime + EndTime + IsAgent + flog;
        string para = "IdentityId=" + IdentityId + " UserId=" + UserId + " StartTime=" + StartTime + " EndTime=" + EndTime + " IsAgent=" + IsAgent + " flog=" + flog;
        try
        {
            st.Reset();
            st.Start();
            ////验证是否有权访问
            //string strMsg = ValidateAuthority();
            //if (!string.IsNullOrWhiteSpace(strMsg))
            //{
            //    return strMsg;
            //}
            //会员
            string strResult = cache[filecontents] as string;
         
            if (strResult == null)
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(2);//只讓快取存活多久時間
                if (flog == 0)
                {
                    //获取会员数据
                    strResult = GetAgencyData_New(IsAgent, IdentityId, UserId, StartTime, EndTime, ResultInfo);
                    //方法                    
                }
                else
                {
                    //获取测试账号数据
                    strResult = GetAgencyDataTest_New(IsAgent, IdentityId, UserId, StartTime, EndTime, ResultInfo);
                }
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_GetAgencyHender", "GetAgencyHender", st.ElapsedMilliseconds.ToString(), para);
                }
                cache.Set(filecontents, strResult, policy);
                return strResult;
            }
            else
            {
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_GetAgencyHender_cache", "GetAgencyHender", st.ElapsedMilliseconds.ToString(), para);
                }
                return strResult;
            }
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetAgencyHender", st.ElapsedMilliseconds.ToString(), para);
            }
            ResultInfo.Code = -1;
            ResultInfo.DataCount = 0;
            ResultInfo.StrCode = "发生错误..";
            return LitJson.JsonMapper.ToJson(ResultInfo);
        }
    }


    private static object _ErrorLogLock = new object();
    private static string ErrorLogPath = Thread.GetDomain().BaseDirectory + "\\Log\\Error\\";
    private static void Error(string folder, string title, string text)
    {
        try
        {
            var path = ErrorLogPath + folder + "\\";
            var msg = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "\t" + title + "\t" + text + Environment.NewLine;

            lock (_ErrorLogLock)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                File.AppendAllText(path + DateTime.Now.ToString("yyyyMMdd") + ".log", msg);
            }
        }
        catch { }
    }
    public static void Error(string folder, string title, string text, string paras = " - ")
    {
        text += "\t" + paras;
        Error(folder, title, text);
    }
    /// <summary>
    /// 前台代理報表正是帳號查詢
    /// </summary>
    /// <param name="IsAgent"></param>
    /// <param name="IdentityId"></param>
    /// <param name="UserId"></param>
    /// <param name="StartTime"></param>
    /// <param name="EndTime"></param>
    /// <param name="ResultInfo"></param>
    /// <returns></returns>
    private string GetAgencyData_New(int IsAgent, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, ResultInfoT<AgentHender> ResultInfo)
    {
        string strStartTime = StartTime.ToString("yyyy-MM-dd");
        string strEndTime = EndTime.ToString("yyyy-MM-dd");
        AgentHender AgencyHenders = new AgentHender();
        AgentHender_New AgencyHenders_new = new AgentHender_New();
        decimal BetMoney = 0m;
        decimal Bonus = 0m;
        decimal RebateMoney = 0m;
        decimal RechargeMoney = 0m;
        decimal WithdrawMoney = 0m;
        decimal ActivityMoney = 0m;
        int BetNum = 0;
        int FirstChargeNum = 0;
        int RegisterNum = 0;
        int TeamNum = 0;
        decimal TeamBalance = 0m;
        decimal ProfitMoney = 0m;
        decimal AgentRebate = 0m;
        decimal AgentWages = 0m;
        decimal AgentDividends = 0m;

        //線上
        List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();


        //sqlcmdlist_online.Add(new SqlParameter("@strStartDate", StartTime.ToString("yyyy-MM-dd")));
        //sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd")));
        SqlParameter pa_identityid1 = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
        pa_identityid1.Value = IdentityId;
        SqlParameter pa_userid1 = new SqlParameter("@userid", SqlDbType.Int);
        pa_userid1.Value = UserId;
        SqlParameter pa_StartDate1 = new SqlParameter("@strStartDate", SqlDbType.DateTime);
        pa_StartDate1.Value = StartTime.ToString("yyyy-MM-dd");
        SqlParameter pa_EndDate1 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        pa_EndDate1.Value = Convert.ToDateTime(EndTime).ToString("yyyy-MM-dd");
        SqlParameter pa_EndDate2 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        pa_EndDate2.Value = Convert.ToDateTime(EndTime.AddDays(1)).ToString("yyyy-MM-dd");
        sqlcmdlist_online.Add(pa_identityid1);
        sqlcmdlist_online.Add(pa_userid1);
        sqlcmdlist_online.Add(pa_StartDate1);
        sqlcmdlist_online.Add(pa_EndDate1);
        SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

        ReportHelper reporthelper = new ReportHelper();
        string agent_in_conn = ReportHelper.Agent_In_Conn;
        string agent_out_conn = ReportHelper.Agent_Out_Conn;
        string agent_order_conn = ReportHelper.Agent_Order_Conn;
        string report_order_conn = ReportHelper.Report_Order_Conn;
        int DBtype = SqlHelper.SwitchDBtype(IdentityId);

        //System.Threading.Tasks.Task agentorderTask = new System.Threading.Tasks.Task(
        //   () =>
        //   {
        List<SqlParameter> sqlcmdlist_online3 = new List<SqlParameter>();
        sqlcmdlist_online3.Add(pa_identityid1);
        sqlcmdlist_online3.Add(pa_userid1);
        sqlcmdlist_online3.Add(pa_StartDate1);
        sqlcmdlist_online3.Add(pa_EndDate2);
        SqlParameter[] sqlcmdpar_online3 = sqlcmdlist_online3.ToArray();
      
        //string query_str = "  select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [dbo].[dt_lottery_record] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from dbo.dt_users_class where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id "
        //          // + " union all  select user_id as father_id,count(distinct(user_id)) count from [dbo].[dt_lottery_record]  left outer join (  select father_id from [dt_users_class]  where identityid='"+identityid+"' ) as v2 on [dt_lottery_record].User_Id = v2.father_id where [dt_lottery_record].identityid ='"+identityid+"' and [dt_lottery_record].add_date>=@strStartDate and [dt_lottery_record].add_date<@strEndDate   and v2.father_id is null  group by identityid,user_id "
        //          + " union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [dbo].[dt_lottery_record] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from dbo.dt_users_class where identityid = @identityid  and user_id =@userid ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate      group by v2.user_id  "
        //          + " ) as view2  group by father_id OPTION (RECOMPILE,MAXDOP 2)";
        string query_str = "select count(distinct (user_id)) as count from dt_lottery_record where add_date>=@strStartDate and add_date<@strEndDate and identityid=@identityid and user_id in (select user_id from dt_users_class where father_id=@userid union select @userid) OPTION (RECOMPILE,MAXDOP 2)";
       DataTable usersDataTable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, DBtype, "report_order_conn", sqlcmdpar_online3);


        // 投注人数
        if(usersDataTable.Rows.Count>0)
        {
            BetNum = Convert.ToInt32(usersDataTable.Rows[0]["count"]);
        }
        //if (usersDataTable.Select("father_id = '" + UserId + "'").Count() > 0)
        //{
        //    BetNum = Convert.ToInt32(usersDataTable.Select("father_id = '" + UserId + "'")[0]["count"]);
        //}
        //});

        DataTable dtMemberClass = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei($"select '{UserId}'", DBtype, "report_share", sqlcmdpar_online);

        //  if (IsAgent > 0)//大于0表示是代理
        // {
        #region 代理数据获取
        // List<dt_report_agent> reportAgentList = redisHelper.Search<dt_report_agent>(  StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        DBcon.dspModel dspModel = new DBcon.dspModel();
        dspModel.paramsDt = dtMemberClass;
        dspModel.identityid = IdentityId;
        dspModel.strStartDate = StartTime;
        dspModel.strEndDate = EndTime;
        //充值
        string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as ActivityMoney,isnull(sum(czmoney),0) as RechargeMoney   from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_in_proxy_report] with(nolock)   where identityid = @identityid and user_id = @userid  and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        //string agent_in_str = "select identityid,user_id as UserId,isnull(sum(tdfdmoney),0) as RebateMoney,isnull(sum(dlfdmoney),0) as AgentRebate,isnull(sum(hdljmoney),0) as ActivityMoney,isnull(sum(czmoney),0) as RechargeMoney,isnull(sum(dlgzmoney),0) as AgentWages,isnull(sum(dlfhmoney),0) as AgentDividends    from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_in_proxy_report] with(nolock)   where identityid = @identityid and user_id = @userid  and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        DataTable dt_agentin = new DataTable();

        dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_in_str, DBtype, "report_share", sqlcmdpar_online);

        reporthelper.ConvertToMode(dt_agentin, AgencyHenders_new);
        //返點
        //string agent_point_str = "select identityid,user_id as UserId,isnull(sum(tdfdmoney),0) as RebateMoney,isnull(sum(dlfdmoney),0) as AgentRebate  from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_get_point_proxy_report] with(nolock)   where identityid = @identityid and user_id = @userid  and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        DataTable dt_agentpoint = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_user_get_point_proxy_report", DBtype, "agent_in_conn", dspModel); //DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_point_str, DBtype, "report_share", sqlcmdpar_online);
        reporthelper.ConvertToMode(dt_agentpoint, AgencyHenders_new);
        //中獎
        //string agent_win_str = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as Bonus from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report] with(nolock)  where identityid = @identityid and user_id = @userid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        DataTable dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_lottery_winning_proxy_report", DBtype, "agent_out_conn", dspModel); 
        reporthelper.ConvertToMode(dt_agentwin, AgencyHenders_new);
        //投注
        //string agent_order_str = "select identityid,user_id as UserId,isnull(sum(tzmoney),0) as BetMoney from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_lottery_proxy_report] with(nolock)  where identityid=@identityid and user_id = @userid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
        //DataTable dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_order_str, DBtype, "report_share", sqlcmdpar_online);
        DataTable dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_lottery_proxy_report", DBtype, "agent_order_conn", dspModel);
        reporthelper.ConvertToMode(dt_agentorder, AgencyHenders_new);
        //提現
        string agent_out_str = "select identityid,user_id as UserId,isnull(sum(txmoney),0) as WithdrawMoney from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_out_proxy_report] with(nolock)  where identityid=@identityid and user_id = @userid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
        DataTable dt_agentout = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_out_str, DBtype, "report_share", sqlcmdpar_online);
        //DataTable dt_agentout = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei2("dsp_select_before_agentReport_dt_lottery_winning_proxy_report", DBtype, "agent_out_conn", dspModel);
        reporthelper.ConvertToMode(dt_agentout, AgencyHenders_new);

        //agentorderTask.Start();


        //for (var i = 0; i < reportAgentList.Count; i++)
        //{
        //    //投注总额
        //    BetMoney += reportAgentList[i].Out_BettingAccount;
        //    BetMoney += reportAgentList[i].Lower_Out_BettingAccount;
        //    //中奖总额
        //    Bonus += reportAgentList[i].Out_WinningAccount;
        //    Bonus += reportAgentList[i].Lower_Out_WinningAccount;
        //    //团队返点总额
        //    RebateMoney += reportAgentList[i].Out_RebateAccount;
        //    RebateMoney += reportAgentList[i].Lower_Out_RebateAccount;
        //    //充值总额
        //    RechargeMoney += reportAgentList[i].InMoney;
        //    RechargeMoney += reportAgentList[i].Lower_InMoney;
        //    //提现总额
        //    WithdrawMoney += reportAgentList[i].Out_Account;
        //    WithdrawMoney += reportAgentList[i].Lower_Out_Account;
        //    //活动总额
        //    ActivityMoney += reportAgentList[i].ActivityDiscountMoney;
        //    ActivityMoney += reportAgentList[i].Lower_ActivityDiscountMoney;
        //    ActivityMoney += reportAgentList[i].OtherDiscountMoney;
        //    ActivityMoney += reportAgentList[i].Lower_OtherDiscountMoney;
        //    //代理返点总额
        //    AgentRebate += reportAgentList[i].Out_RebateAccount;
        //}
        //Log.Info("UserId:" + UserId + ",统计代理数据结束时间：" + DateTime.Now.ToString());
        RechargeMoney = AgencyHenders_new.RechargeMoney;
        WithdrawMoney = AgencyHenders_new.WithdrawMoney;
        Bonus = AgencyHenders_new.Bonus;
        BetMoney = AgencyHenders_new.BetMoney;
        ActivityMoney = AgencyHenders_new.ActivityMoney;
        RebateMoney = AgencyHenders_new.RebateMoney;
        AgentRebate = AgencyHenders_new.AgentRebate;
        //盈利金额
        ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
        //查询首充人数
        //Log.Info("UserId:" + UserId + ",获取下级代理数据开始时间：" + DateTime.Now.ToString());
        List<SqlParameter> sqlcmdlist_share = new List<SqlParameter>();
        sqlcmdlist_share.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        sqlcmdlist_share.Add(new SqlParameter("@father_id", UserId));
        string strSql = "select user_id from dt_users_class where identityid=@identityid and father_id=@father_id";
        DataTable userClassDataTable = DBcon.DbHelperSQL.GetQueryFromShare(strSql, sqlcmdlist_share.ToArray());
        //Log.Info("UserId:" + UserId + ",获取下级代理数据结束时间：" + DateTime.Now.ToString());
        //Log.Info("UserId:" + UserId + ",缓存下级代理开始时间：" + DateTime.Now.ToString());
        Dictionary<int, int> ClassListss = new Dictionary<int, int>();
        ClassListss.Add(UserId, UserId);
        for (int i = 0; i < userClassDataTable.Rows.Count; i++)
        {
            string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            ClassListss.Add(int.Parse(strUserId), int.Parse(strUserId));
        }
        List<SqlParameter> sqlcmdlist_online2 = new List<SqlParameter>();

        //sqlcmdlist_online.Add(new SqlParameter("@strStartDate", StartTime.ToString("yyyy-MM-dd")));
        //sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd")));
        SqlParameter pa_identityid2 = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
        pa_identityid2.Value = IdentityId;
        SqlParameter pa_userid2 = new SqlParameter("@userid", SqlDbType.Int);
        pa_userid2.Value = UserId;
        SqlParameter pa_StartDate2 = new SqlParameter("@strStartDate", SqlDbType.DateTime);
        pa_StartDate2.Value = StartTime.ToString("yyyy-MM-dd");
        //SqlParameter pa_EndDate2 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        //pa_EndDate2.Value = Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd");
        sqlcmdlist_online2.Add(pa_identityid2);
        sqlcmdlist_online2.Add(pa_userid2);
        sqlcmdlist_online2.Add(pa_StartDate2);
        sqlcmdlist_online2.Add(pa_EndDate2);
        SqlParameter[] sqlcmdpar_online2 = sqlcmdlist_online2.ToArray();

        string congkustr = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_user_newpay] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid and father_id = @userid  ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate  and father_id is not null    group by v2.father_id  OPTION (MAXDOP 1)";
        if (DBtype == 1)
            congkustr = congkustr.Replace("dt_user_newpay", "dt_user_newpay");
        else
            congkustr = congkustr.Replace("dt_user_newpay", "dt_user_newpay" + DBtype);

        DataTable congkutable = DBcon.DbHelperSQL.GetQueryFromShare(congkustr, sqlcmdpar_online2, DBtype);
        if (congkutable.Select("father_id = '" + UserId + "'").Count() > 0)
        {
            FirstChargeNum = Convert.ToInt32(congkutable.Select("father_id = '" + UserId + "'")[0]["count"]);
        }

        //string query_str2 = "  select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [dbo].[dt_lottery_record] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from dbo.dt_users_class where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id "
        //          // + " union all  select user_id as father_id,count(distinct(user_id)) count from [dbo].[dt_lottery_record]  left outer join (  select father_id from [dt_users_class]  where identityid='"+identityid+"' ) as v2 on [dt_lottery_record].User_Id = v2.father_id where [dt_lottery_record].identityid ='"+identityid+"' and [dt_lottery_record].add_date>=@strStartDate and [dt_lottery_record].add_date<@strEndDate   and v2.father_id is null  group by identityid,user_id "
        //          + " union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [dbo].[dt_lottery_record] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from dbo.dt_users_class where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate      group by v2.user_id  "
        //          + " ) as view2  group by father_id OPTION (RECOMPILE)";
        //DataTable report_betnumtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str2, DBtype, "report_share", sqlcmdpar_online2);
        //string query_str = "select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from dbo.dt_users_class where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id  union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [srv_lnk_total_order].[dafacloud].[dbo].[dt_lottery_record] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from dbo.dt_users_class where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate      group by v2.user_id   ) as view2  group by father_id OPTION (RECOMPILE)";

        //DataTable report_betnumtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str, DBtype, "report_share", sqlcmdpar_online);
        //// 投注人数
        //if (report_betnumtable.Select("father_id = '" + UserId + "'").Count() > 0)
        //{
        //    BetNum = Convert.ToInt32(report_betnumtable.Select("father_id = '" + UserId + "'")[0]["count"]);
        //}
        // 代理工资
        AgentWages = AgencyHenders_new.AgentWages;
        //代理分红
        AgentDividends = AgencyHenders_new.AgentDividends;

        //注册人数

        //string reg_str = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_users_register] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid and father_id = @userid  ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate and father_id is not null     group by v2.father_id OPTION (MAXDOP 1) ";
        string reg_str = "select count(1)as'count' from [dt_users_register] with(index([ix_IdentityidAddtime])) where identityid = @identityid AND addtime >= @strStartDate AND addtime<@strEndDate AND USERID in (select USER_ID from [dt_users_class] where father_id = @userid and identityid = @identityid)OPTION(maxdop 1) ";
        DataTable regDataTable = DBcon.DbHelperSQL.GetQueryFromShare(reg_str, sqlcmdpar_online2);
        /*
        if (regDataTable.Select("father_id = '" + UserId + "'").Count() > 0)
        {
            RegisterNum = Convert.ToInt32(regDataTable.Select("father_id = '" + UserId + "'")[0]["count"]);
        }
        */        
        RegisterNum = Convert.ToInt32(regDataTable.Rows[0]["count"]);

        TeamNum = ClassListss.Count;

        List<SqlParameter> paras = new List<SqlParameter>();
        SqlParameter pa_identityid = new SqlParameter("@identityid", SqlDbType.VarChar, 10);
        pa_identityid.Value = IdentityId;
        SqlParameter pa_userid = new SqlParameter("@UserId", SqlDbType.Int);
        pa_userid.Value = UserId;
        SqlParameter pa_fatherid = new SqlParameter("@father_id", SqlDbType.Int);
        pa_fatherid.Value = UserId;
        paras.Add(pa_identityid);
        paras.Add(pa_userid);
        paras.Add(pa_fatherid);
        DataTable TMoney = DBcon.DbHelperSQL.GetQueryFromCongku(
          "select SUM(use_money) from dt_user_capital with(index(index_user_id))" +
          "where identityid=@identityid and flog=0 and user_id in(select user_id from dt_users_class  with(index(index_fatherid))" +
          "where father_id=@father_id ) OR user_id = @UserId ",DBtype, paras.ToArray());

        if (TMoney.Rows.Count > 0)
        {
            if (TMoney.Rows[0][0] != DBNull.Value)
            {
                TeamBalance = Convert.ToDecimal(TMoney.Rows[0][0]);
            }
        }
        //agentorderTask.Wait();
        #region 赋值
        AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
        AgencyHenders.Bonus = Bonus.ToString("#0.00");
        AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
        AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
        AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
        AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
        AgencyHenders.BetNum = BetNum.ToString();
        AgencyHenders.FirstChargeNum = FirstChargeNum.ToString();
        AgencyHenders.TeamNum = TeamNum.ToString();
        AgencyHenders.RegisterNum = RegisterNum.ToString();
        AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
        AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
        AgencyHenders.AgentRebate = AgentRebate.ToString("#0.00");
        AgencyHenders.AgentWages = AgentWages.ToString("#0.00");
        AgencyHenders.AgentDividends = AgentDividends.ToString("#0.00");
        #endregion
        //是否有分红。没有传null
        //Log.Info("UserId:" + UserId + ",是否有分红。没有传null开始时间：" + DateTime.Now.ToString());
        //fucking
        List<SqlParameter> listparms = new List<SqlParameter>();
        listparms.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        listparms.Add(new SqlParameter("@UserId", UserId));
        List<SqlParameter> listparms2 = new List<SqlParameter>();
        listparms2.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        listparms2.Add(new SqlParameter("@UserId", UserId));

        DataTable BounsDataTable = DBcon.DbHelperSQL.GetQueryFromCongku(string.Format("select top 1 id from dt_agent_bonus where identityid=@identityid and UserId=@UserId"), DBtype, listparms.ToArray());
        if (BounsDataTable.Rows.Count < 1)
        {
            AgencyHenders.AgentDividends = null;
        }
        //Log.Info("UserId:" + UserId + ",是否有分红。没有传null结束时间：" + DateTime.Now.ToString());
        //是否有工资。没有传null
        //Log.Info("UserId:" + UserId + ",是否有工资。没有传null开始时间：" + DateTime.Now.ToString());
        DataTable WageDataTable = DBcon.DbHelperSQL.GetQueryFromCongku(string.Format("select top 1 id from dt_agent_wageday where identityid=@identityid and UserId=@UserId"), DBtype, listparms2.ToArray());
        if (WageDataTable.Rows.Count < 1)
        {
            AgencyHenders.AgentWages = null;
        }
        //Log.Info("UserId:" + UserId + ",是否有工资。没有传null结束时间：" + DateTime.Now.ToString());
        #endregion
        //  }
        ResultInfo.Code = 1;
        ResultInfo.DataCount = 1;
        ResultInfo.BackData = AgencyHenders;
        string sss = LitJson.JsonMapper.ToJson(ResultInfo);
        //Log.Info("结束：" + sss);
        return sss;
    }
    void write()
    {

    }

    /// <summary>
    /// 測試帳號代理報表
    /// </summary>
    /// <param name="IsAgent"></param>
    /// <param name="IdentityId"></param>
    /// <param name="UserId"></param>
    /// <param name="StartTime"></param>
    /// <param name="EndTime"></param>
    /// <param name="ResultInfo"></param>
    /// <returns></returns>
    private string GetAgencyDataTest_New(int IsAgent, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, ResultInfoT<AgentHender> ResultInfo)
    {
        string strStartTime = StartTime.ToString("yyyy-MM-dd");
        string strEndTime = EndTime.ToString("yyyy-MM-dd");
        AgentHender AgencyHenders = new AgentHender();
        AgentHender_New AgencyHenders_new = new AgentHender_New();
        decimal BetMoney = 0m;
        decimal Bonus = 0m;
        decimal RebateMoney = 0m;
        decimal RechargeMoney = 0m;
        decimal WithdrawMoney = 0m;
        decimal ActivityMoney = 0m;
        int BetNum = 0;
        int FirstChargeNum = 0;
        int RegisterNum = 0;
        int TeamNum = 0;
        decimal TeamBalance = 0m;
        decimal ProfitMoney = 0m;
        decimal AgentRebate = 0m;
        decimal AgentWages = 0m;
        decimal AgentDividends = 0m;

        //線上
        List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
        //sqlcmdlist_online.Add(new SqlParameter("@identityid", IdentityId));
        //sqlcmdlist_online.Add(new SqlParameter("@userid", UserId));
        //sqlcmdlist_online.Add(new SqlParameter("@strStartDate", StartTime));
        //sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd")));
        //SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();
        SqlParameter pa_identityid1 = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
        pa_identityid1.Value = IdentityId;
        SqlParameter pa_userid1 = new SqlParameter("@userid", SqlDbType.Int);
        pa_userid1.Value = UserId;
        SqlParameter pa_StartDate1 = new SqlParameter("@strStartDate", SqlDbType.DateTime);
        pa_StartDate1.Value = StartTime.ToString("yyyy-MM-dd");
        SqlParameter pa_EndDate1 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        pa_EndDate1.Value = Convert.ToDateTime(EndTime).ToString("yyyy-MM-dd");
        sqlcmdlist_online.Add(pa_identityid1);
        sqlcmdlist_online.Add(pa_userid1);
        sqlcmdlist_online.Add(pa_StartDate1);
        sqlcmdlist_online.Add(pa_EndDate1);
        SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

        int DBtype = SqlHelper.SwitchDBtype(IdentityId);

        //  if (IsAgent > 0)//大于0表示是代理
        // {
        #region 代理数据获取
        // List<dt_report_agent> reportAgentList = redisHelper.Search<dt_report_agent>(  StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        ReportHelper reporthelper = new ReportHelper();
        string report_in_conn = ReportHelper.Report_In_Conn;
        string report_order_conn = ReportHelper.Report_Order_Conn;
        string agent_in_conn = ReportHelper.Agent_In_Conn;
        string agent_out_conn = ReportHelper.Agent_Out_Conn;
        string agent_order_conn = ReportHelper.Agent_Order_Conn;
        string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as ActivityMoney,isnull(sum(tdfdmoney),0) as RebateMoney,isnull(sum(czmoney),0) as RechargeMoney,isnull(sum(dlfdmoney),0) as AgentRebate   from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_in_proxy_report_test] with(nolock)   where identityid = @identityid and user_id = @userid  and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        DataTable dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_in_str, DBtype, "report_share", sqlcmdpar_online);
        reporthelper.ConvertToMode(dt_agentin, AgencyHenders_new);
        string agent_win_str = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as Bonus   from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report_test] with(nolock)  where identityid = @identityid and user_id = @userid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
        DataTable dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_win_str, DBtype, "report_share", sqlcmdpar_online);
        reporthelper.ConvertToMode(dt_agentwin, AgencyHenders_new);
        string agent_order_str = "select identityid,user_id as UserId,isnull(sum(tzmoney),0) as BetMoney from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_proxy_report_test] with(nolock)  where identityid=@identityid and user_id = @userid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
        DataTable dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_order_str, DBtype, "report_share", sqlcmdpar_online);
        reporthelper.ConvertToMode(dt_agentorder, AgencyHenders_new);
        string agent_out_str = "select identityid,user_id as UserId,isnull(sum(txmoney),0) as WithdrawMoney from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_out_proxy_report_test] with(nolock)  where identityid=@identityid and user_id = @userid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
        DataTable dt_agentout = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_out_str, DBtype, "report_share", sqlcmdpar_online);
        reporthelper.ConvertToMode(dt_agentout, AgencyHenders_new);


        //for (var i = 0; i < reportAgentList.Count; i++)
        //{
        //    //投注总额
        //    BetMoney += reportAgentList[i].Out_BettingAccount;
        //    BetMoney += reportAgentList[i].Lower_Out_BettingAccount;
        //    //中奖总额
        //    Bonus += reportAgentList[i].Out_WinningAccount;
        //    Bonus += reportAgentList[i].Lower_Out_WinningAccount;
        //    //团队返点总额
        //    RebateMoney += reportAgentList[i].Out_RebateAccount;
        //    RebateMoney += reportAgentList[i].Lower_Out_RebateAccount;
        //    //充值总额
        //    RechargeMoney += reportAgentList[i].InMoney;
        //    RechargeMoney += reportAgentList[i].Lower_InMoney;
        //    //提现总额
        //    WithdrawMoney += reportAgentList[i].Out_Account;
        //    WithdrawMoney += reportAgentList[i].Lower_Out_Account;
        //    //活动总额
        //    ActivityMoney += reportAgentList[i].ActivityDiscountMoney;
        //    ActivityMoney += reportAgentList[i].Lower_ActivityDiscountMoney;
        //    ActivityMoney += reportAgentList[i].OtherDiscountMoney;
        //    ActivityMoney += reportAgentList[i].Lower_OtherDiscountMoney;
        //    //代理返点总额
        //    AgentRebate += reportAgentList[i].Out_RebateAccount;
        //}
        //Log.Info("UserId:" + UserId + ",统计代理数据结束时间：" + DateTime.Now.ToString());
        RechargeMoney = AgencyHenders_new.RechargeMoney;
        WithdrawMoney = AgencyHenders_new.WithdrawMoney;
        Bonus = AgencyHenders_new.Bonus;
        BetMoney = AgencyHenders_new.BetMoney;
        ActivityMoney = AgencyHenders_new.ActivityMoney;
        RebateMoney = AgencyHenders_new.RebateMoney;
        AgentRebate = AgencyHenders_new.AgentRebate;
        //盈利金额
        ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
        //查询首充人数
        //Log.Info("UserId:" + UserId + ",获取下级代理数据开始时间：" + DateTime.Now.ToString());
        List<SqlParameter> listparms = new List<SqlParameter>();
        listparms.Add(new SqlParameter("@identityid", SqlDbType.VarChar, 50) { Value = IdentityId });
        listparms.Add(new SqlParameter("@father_id", UserId));
        string strSql = "select user_id from dt_users_class where identityid=@identityid and father_id=@father_id";
        DataTable userClassDataTable = DBcon.DbHelperSQL.GetQueryFromShare(strSql, listparms.ToArray());
        //Log.Info("UserId:" + UserId + ",获取下级代理数据结束时间：" + DateTime.Now.ToString());
        //Log.Info("UserId:" + UserId + ",缓存下级代理开始时间：" + DateTime.Now.ToString());
        Dictionary<int, int> ClassListss = new Dictionary<int, int>();
        ClassListss.Add(UserId, UserId);
        for (int i = 0; i < userClassDataTable.Rows.Count; i++)
        {
            string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
            ClassListss.Add(int.Parse(strUserId), int.Parse(strUserId));
        }
        List<SqlParameter> sqlcmdlist_online2 = new List<SqlParameter>();

        SqlParameter pa_identityid2 = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
        pa_identityid2.Value = IdentityId;
        SqlParameter pa_userid2 = new SqlParameter("@userid", SqlDbType.Int);
        pa_userid2.Value = UserId;
        SqlParameter pa_StartDate2 = new SqlParameter("@strStartDate", SqlDbType.DateTime);
        pa_StartDate2.Value = StartTime.ToString("yyyy-MM-dd");
        SqlParameter pa_EndDate2 = new SqlParameter("@strEndDate", SqlDbType.DateTime);
        pa_EndDate2.Value = Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd");
        sqlcmdlist_online2.Add(pa_identityid2);
        sqlcmdlist_online2.Add(pa_userid2);
        sqlcmdlist_online2.Add(pa_StartDate2);
        sqlcmdlist_online2.Add(pa_EndDate2);
        SqlParameter[] sqlcmdpar_online2 = sqlcmdlist_online2.ToArray();
        string congkustr = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_user_newpay] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid and father_id = @userid  ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate  and father_id is not null    group by v2.father_id  ";
        if (DBtype == 1)
            congkustr = congkustr.Replace("dt_user_newpay", "dt_user_newpay");
        else
            congkustr = congkustr.Replace("dt_user_newpay", "dt_user_newpay" + DBtype);
        DataTable congkutable = DBcon.DbHelperSQL.GetQueryFromShare(congkustr, sqlcmdpar_online2, DBtype);
        if (congkutable.Select("father_id = '" + UserId + "'").Count() > 0)
        {
            FirstChargeNum = Convert.ToInt32(congkutable.Select("father_id = '" + UserId + "'")[0]["count"]);
        }
        string query_str2 = "  select father_id ,sum(count) as count  from ( select  v2.father_id,count(distinct(v2.user_id))count  from ( select identityid,user_id,add_date from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_record_test] where identityid =@identityid and add_date>=@strStartDate and add_date<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from dbo.dt_users_class where identityid =@identityid   ) as v2  on v1.user_id = v2.user_id    where  v1.identityid =@identityid  and v1.add_date>=@strStartDate and v1.add_date<@strEndDate and father_id is not null      group by v2.father_id "
                  // + " union all  select user_id as father_id,count(distinct(user_id)) count from [dbo].[dt_lottery_record]  left outer join (  select father_id from [dt_users_class]  where identityid=@identityid ) as v2 on [dt_lottery_record].User_Id = v2.father_id where [dt_lottery_record].identityid =@identityid and [dt_lottery_record].add_date>=@strStartDate and [dt_lottery_record].add_date<@strEndDate   and v2.father_id is null  group by identityid,user_id "
                  + " union all  select v2.user_id,count(distinct(v2.user_id))count from(select identityid, user_id, add_date from [srv_lnk_total_out].[dafacloud].[dbo].[dt_lottery_record_test] where identityid = @identityid and add_date>= @strStartDate and add_date<@strEndDate  ) as v1 left outer join(select identityid, father_id, user_id, addtime from dbo.dt_users_class where identityid = @identityid   ) as v2  on v1.user_id = v2.user_id    where v1.identityid = @identityid   and v1.add_date>=@strStartDate and v1.add_date<@strEndDate     group by v2.user_id  "
                  + " ) as view2  group by father_id OPTION (RECOMPILE,MAXDOP 2)";
        DataTable report_betnumtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(query_str2, report_share, sqlcmdpar_online2);
        // 投注人数
        if (report_betnumtable.Select("father_id = '" + UserId + "'").Count() > 0)
        {
            BetNum = Convert.ToInt32(report_betnumtable.Select("father_id = '" + UserId + "'")[0]["count"]);
        }
        //for (int i = 0; i < report_betnumtable.Rows.Count; i++)
        //{
        //    //dt_report_num report_num = new dt_report_num();
        //    //reporthelper.ConvertDataRowToModel_sql(report_betnumtable.Rows[i], report_num);
        //    //reportNumList.Add(report_num);
        //    string strUserId = report_betnumtable.Rows[i]["father_id"].ToString();
        //    int strUsercount = Convert.ToInt32(report_betnumtable.Rows[i]["count"]);
        //    // ComputeNum(dicBetNum, strUserId, dicAgentData);
        //    dicBetNum.Add(strUserId, strUsercount);
        //}
        //Log.Info("UserId:" + UserId + ",获取投注人数结束时间：" + DateTime.Now.ToString());
        //Log.Info("UserId:" + UserId + ",获取代理工资分红数据开始时间：" + DateTime.Now.ToString());
        //List<dt_report_inoutaccount> reportInAccount = redisClient.GetAll<dt_report_inoutaccount>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
        //List<dt_report_inoutaccount> reportInAccount = redisHelper.Search<dt_report_inoutaccount>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();
        //for (int t = 0; t < reportInAccount.Count; t++)
        //{
        //    //代理工资
        //    AgentWages += reportInAccount[t].Wages;
        //    //代理分红
        //    AgentDividends += reportInAccount[t].Bonus;
        //}
        //Log.Info("UserId:" + UserId + ",获取代理工资分红数据结束时间：" + DateTime.Now.ToString());
        // 代理工资
        AgentWages = AgencyHenders_new.AgentWages;
        //代理分红
        AgentDividends = AgencyHenders_new.AgentDividends;

        //注册人数
        //string reg_str = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_users_register] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid and father_id = @userid  ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate  and father_id is not null    group by v2.father_id OPTION (MAXDOP 1)";
        string reg_str = "select count(1)as'count' from [dt_users_register] with(index([ix_IdentityidAddtime])) where identityid = @identityid AND addtime >= @strStartDate AND addtime<@strEndDate AND USERID in (select USER_ID from [dt_users_class] where father_id = @userid and identityid = @identityid)OPTION(maxdop 1) ";
        DataTable regDataTable = DBcon.DbHelperSQL.GetQueryFromShare(reg_str, sqlcmdpar_online2);
        //if (regDataTable.Select("father_id = '" + UserId + "'").Count() > 0)
        //{
        //    RegisterNum = Convert.ToInt32(regDataTable.Select("father_id = '" + UserId + "'")[0]["count"]);
        //}
        RegisterNum = Convert.ToInt32(regDataTable.Rows[0]["count"]);
        //团队人数
        TeamNum = ClassListss.Count;
        //Log.Info("UserId:" + UserId + ",获取团队人数结束时间：" + DateTime.Now.ToString());
        //团队余额
        //Log.Info("UserId:" + UserId + ",获取团队余额开始时间：" + DateTime.Now.ToString());
        //List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
        //sqlcmdlist.Add(new SqlParameter("@identityid", IdentityId));
        //sqlcmdlist.Add(new SqlParameter("@strStartDate", StartTime));
        //sqlcmdlist.Add(new SqlParameter("@strEndDate", EndTime));
        //SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
        List<SqlParameter> sqlcmdlist = new List<SqlParameter>
        {
           new SqlParameter("@identityid", SqlDbType.VarChar,50){ Value = IdentityId},
           new SqlParameter("@fatherid", UserId),
           new SqlParameter("@userid",UserId)
         };
        DataTable TMoney = DBcon.DbHelperSQL.GetQueryFromCongku("select SUM(use_money) from dt_user_capital" +
            " where  flog=1 and (user_id in(select user_id from dt_users_class where identityid=@identityid and father_id=@fatherid ) or user_id = @userid)",DBtype, sqlcmdlist.ToArray());
        if (TMoney.Rows.Count > 0)
        {
            if (TMoney.Rows[0][0] != DBNull.Value)
            {
                TeamBalance = Convert.ToDecimal(TMoney.Rows[0][0]);
            }
        }
        //Log.Info("UserId:" + UserId + ",获取团队余额结束时间：" + DateTime.Now.ToString());
        #region 赋值
        AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
        AgencyHenders.Bonus = Bonus.ToString("#0.00");
        AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
        AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
        AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
        AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
        AgencyHenders.BetNum = BetNum.ToString();
        AgencyHenders.FirstChargeNum = FirstChargeNum.ToString();
        AgencyHenders.TeamNum = TeamNum.ToString();
        AgencyHenders.RegisterNum = RegisterNum.ToString();
        AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
        AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
        AgencyHenders.AgentRebate = AgentRebate.ToString("#0.00");
        AgencyHenders.AgentWages = AgentWages.ToString("#0.00");
        AgencyHenders.AgentDividends = AgentDividends.ToString("#0.00");
        #endregion
        //是否有分红。没有传null
        //Log.Info("UserId:" + UserId + ",是否有分红。没有传null开始时间：" + DateTime.Now.ToString());
        //DataTable BounsDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_bonus where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
        //if (BounsDataTable.Rows.Count < 1)
        //{
        //    AgencyHenders.AgentDividends = null;
        //}
        ////Log.Info("UserId:" + UserId + ",是否有分红。没有传null结束时间：" + DateTime.Now.ToString());
        ////是否有工资。没有传null
        ////Log.Info("UserId:" + UserId + ",是否有工资。没有传null开始时间：" + DateTime.Now.ToString());
        //DataTable WageDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_wageday where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
        //if (WageDataTable.Rows.Count < 1)
        //{
        //    AgencyHenders.AgentWages = null;
        //}
        //Log.Info("UserId:" + UserId + ",是否有工资。没有传null结束时间：" + DateTime.Now.ToString());
        #endregion
        //  }
        ResultInfo.Code = 1;
        ResultInfo.DataCount = 1;
        ResultInfo.BackData = AgencyHenders;
        string sss = LitJson.JsonMapper.ToJson(ResultInfo);
        //Log.Info("结束：" + sss);
        return sss;
    }


    //private string GetTestAgencyData(int IsAgent, string IdentityId, int UserId, DateTime StartTime, DateTime EndTime, ResultInfoT<AgentHender> ResultInfo)
    //{
    //    ReportHelper reporthelper = new ReportHelper();
    //    //connectstring
    //    string agent_in_conn = DBcon.DBcontring.Agent_In_Conn;
    //    string agent_out_conn = DBcon.DBcontring.Agent_Out_Conn;
    //    string agent_order_conn = DBcon.DBcontring.Agent_Order_Conn;
    //    string report_order_conn = DBcon.DBcontring.Report_Order_Conn;

    //    //報表
    //    List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
    //    sqlcmdlist.Add(new SqlParameter("@identityid", IdentityId));
    //    sqlcmdlist.Add(new SqlParameter("@strStartDate", StartTime));
    //    sqlcmdlist.Add(new SqlParameter("@strEndDate", EndTime));
    //    SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();

    //    //站
    //    List<SqlParameter> sqlcmdlist_identityid = new List<SqlParameter>();
    //    sqlcmdlist_identityid.Add(new SqlParameter("@identityid", IdentityId));
    //    SqlParameter[] sqlcmdpar_identityid = sqlcmdlist_identityid.ToArray();

    //    //線上
    //    List<SqlParameter> sqlcmdlist_online = new List<SqlParameter>();
    //    sqlcmdlist_online.Add(new SqlParameter("@identityid", IdentityId));
    //    sqlcmdlist_online.Add(new SqlParameter("@strStartDate", StartTime));
    //    sqlcmdlist_online.Add(new SqlParameter("@strEndDate", Convert.ToDateTime(EndTime).AddDays(1).ToString("yyyy-MM-dd")));
    //    SqlParameter[] sqlcmdpar_online = sqlcmdlist_online.ToArray();

    //    //datatable
    //    DataTable agentUser = new DataTable();
    //    DataTable listUsers = new DataTable();
    //    DataTable usersDataTable = new DataTable();
    //    DataTable dt_agentin = new DataTable();
    //    DataTable dt_agentwin = new DataTable();
    //    DataTable dt_agentorder = new DataTable();
    //    DataTable dt_agentout = new DataTable();
    //    DataTable regDataTable = new DataTable();
    //    DataTable congkutable = new DataTable();
    //    DataTable report_betnumtable = new DataTable();
    //    DataTable totalDataTable = new DataTable();
    //    DataTable dataTable = new DataTable();

    //    List<ReportAgent> listAgentReport = new List<ReportAgent>();

    //    #region 初始化
    //    decimal BetMoney = 0m;
    //    decimal Bonus = 0m;
    //    decimal RebateMoney = 0m;
    //    decimal RechargeMoney = 0m;
    //    decimal WithdrawMoney = 0m;
    //    decimal ActivityMoney = 0m;
    //    int BetNum = 0;
    //    int FirstChargeNum = 0;
    //    int RegisterNum = 0;
    //    int TeamNum = 0;
    //    decimal TeamBalance = 0m;
    //    decimal ProfitMoney = 0m;
    //    decimal AgentRebate = 0m;
    //    decimal AgentWages = 0m;
    //    decimal AgentDividends = 0m;
    //    #endregion
    //    AgentHender AgencyHenders = new AgentHender();
    //    //if (IsAgent > 0)//大于0表示是代理
    //    //{
    //    #region 代理数据获取
    //    //Log.Info("UserId:"+UserId+",获取代理数据开始时间：" + DateTime.Now.ToString());
    //    //List<dt_report_agent> reportAgentList = redisClient.GetAll<dt_report_agent>().Where(m => m.identityid == IdentityId && m.user_id == UserId && m.AddTime >= StartTime && m.AddTime <= EndTime).ToList();
    //    //List<dt_report_agent> reportAgentList = redisHelper.Search<dt_report_agent>(redisClient, redisClientKey, StartTime, EndTime, UserId.ToString(), "").Where(m => m.identityid == IdentityId).ToList();

    //    string agent_in_str = "select identityid,user_id as UserId,isnull(sum(hdljmoney),0) as TotalDiscountAccount,isnull(sum(tdfdmoney),0) as TotalRebateAccount,isnull(sum(czmoney),0) as TotalInMoney,isnull(sum(dlfdmoney),0) as RebateAccount,isnull(sum(dlgzmoney),0) as TotalWages,isnull(sum(dlfhmoney),0) as TotalBonus    from [srv_lnk_agent_in].[dafacloud].[dbo].[dt_user_in_proxy_report] with(nolock)   where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
    //    dt_agentin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_in_str, report_share, sqlcmdpar);
    //    string agent_win_str = "select identityid,user_id as UserId,isnull(sum(zjmoney),0) as TotalWinningAccount   from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_lottery_winning_proxy_report] with(nolock)  where identityid = @identityid and add_date >= @strStartDate and add_date<= @strEndDate group by identityid,user_id ";
    //    dt_agentwin = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_win_str, report_share, sqlcmdpar);
    //    string agent_order_str = "select identityid,user_id as UserId,isnull(sum(tzmoney),0) as TotalBettingAccount from [srv_lnk_agent_order].[dafacloud].[dbo].[dt_lottery_proxy_report] with(nolock)  where identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
    //    dt_agentorder = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_order_str, report_share, sqlcmdpar);
    //    string agent_out_str = "select identityid,user_id as UserId,isnull(sum(txmoney),0) as TotalOutMoney from [srv_lnk_agent_out].[dafacloud].[dbo].[dt_user_out_proxy_report] with(nolock)  where identityid= @identityid and add_date >=@strStartDate and add_date <=@strEndDate  group by identityid,user_id  ";
    //    dt_agentout = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(agent_out_str, report_share, sqlcmdpar);
    //    for (int i = 0; i < listAgentReport.Count; i++)
    //    {
    //        string listAgentId_str = listAgentReport[i].UserId;
    //        if (dt_agentin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
    //        {
    //            reporthelper.ConvertDataRowToModel_sql(dt_agentin.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
    //        }
    //        if (dt_agentwin.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
    //        {
    //            reporthelper.ConvertDataRowToModel_sql(dt_agentwin.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
    //        }
    //        if (dt_agentorder.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
    //        {
    //            reporthelper.ConvertDataRowToModel_sql(dt_agentorder.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
    //        }
    //        if (dt_agentout.Select("UserId = '" + listAgentId_str + "'").Count() > 0)
    //        {
    //            reporthelper.ConvertDataRowToModel_sql(dt_agentout.Select("UserId = '" + listAgentId_str + "'")[0], listAgentReport[i]);
    //        }
    //    }





    //    //Log.Info("UserId:"+UserId+",获取代理数据结束时间：" + DateTime.Now.ToString());
    //    //Log.Info("UserId:" + UserId + ",统计代理数据开始时间：" + DateTime.Now.ToString());
    //    for (var i = 0; i < listAgentReport.Count; i++)
    //    {
    //        //投注总额
    //        BetMoney += listAgentReport[i].TotalBettingAccount;
    //        //BetMoney += listAgentReport[i].Lower_Out_BettingAccount;
    //        //中奖总额
    //        Bonus += listAgentReport[i].TotalWinningAccount;
    //        // Bonus += listAgentReport[i].Lower_Out_WinningAccount;
    //        //团队返点总额
    //        RebateMoney += listAgentReport[i].TotalRebateAccount;
    //        // RebateMoney += reportAgentList[i].Lower_Out_RebateAccount;
    //        //充值总额
    //        RechargeMoney += listAgentReport[i].TotalInMoney;
    //        // RechargeMoney += reportAgentList[i].Lower_InMoney;
    //        //提现总额
    //        WithdrawMoney += listAgentReport[i].TotalOutMoney;
    //        // WithdrawMoney += reportAgentList[i].Lower_Out_Account;
    //        //活动总额
    //        ActivityMoney += listAgentReport[i].TotalDiscountAccount;
    //        // ActivityMoney += reportAgentList[i].Lower_ActivityDiscountMoney;
    //        // ActivityMoney += reportAgentList[i].OtherDiscountMoney;
    //        // ActivityMoney += reportAgentList[i].Lower_OtherDiscountMoney;
    //        //代理返点总额
    //        AgentRebate += listAgentReport[i].RebateAccount;
    //    }
    //    //Log.Info("UserId:" + UserId + ",统计代理数据结束时间：" + DateTime.Now.ToString());
    //    //盈利金额
    //    ProfitMoney = Bonus - BetMoney + ActivityMoney + RebateMoney;
    //    //查询首充人数
    //    //Log.Info("UserId:" + UserId + ",获取下级代理数据开始时间：" + DateTime.Now.ToString());
    //    string strSql = "select user_id from dt_users_class where identityid='" + IdentityId + "' and father_id=" + UserId;
    //    DataTable userClassDataTable = DBcon.DbHelperSQL.Query(strSql).Tables[0];
    //    //Log.Info("UserId:" + UserId + ",获取下级代理数据结束时间：" + DateTime.Now.ToString());
    //    //Log.Info("UserId:" + UserId + ",缓存下级代理开始时间：" + DateTime.Now.ToString());
    //    Dictionary<int, int> ClassListss = new Dictionary<int, int>();
    //    ClassListss.Add(UserId, UserId);
    //    for (int i = 0; i < userClassDataTable.Rows.Count; i++)
    //    {
    //        string strUserId = userClassDataTable.Rows[i]["user_id"].ToString();
    //        ClassListss.Add(int.Parse(strUserId), int.Parse(strUserId));
    //    }
    //    List<dt_report_newprepaid> reportNewprepaid = new List<dt_report_newprepaid>();

    //    string congkustr = "select  v2.father_id,count(distinct(v2.user_id))count  from (select identityid,userid,addtime from [dbo].[dt_user_newpay] where identityid =@identityid and addtime>=@strStartDate and addtime<@strEndDate  ) as v1 left outer join  ( select identityid,father_id,user_id,addtime from [dbo].[dt_users_class] where identityid =@identityid   ) as v2 on v1.userid = v2.user_id    where  v1.identityid =@identityid  and v1.addtime>=@strStartDate and v1.addtime<@strEndDate and father_id is not null      group by v2.father_id  ";

    //    congkutable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(congkustr, report_share, sqlcmdpar_online);
    //    for (int i = 0; i < congkutable.Rows.Count; i++)
    //    {
    //        //dt_report_newprepaid report_newprepaid = new dt_report_newprepaid();
    //        //reporthelper.ConvertDataRowToModel_sql(congkutable.Rows[i], report_newprepaid);
    //        //newPrepaidList.Add(report_newprepaid);
    //        string strUserId = congkutable.Rows[i]["father_id"].ToString();
    //        int strUsercount = Convert.ToInt32(congkutable.Rows[i]["count"]);
    //        // ComputeNum(dicNewPrepaid, strUserId, dicAgentData);
    //        // dicNewPrepaid.Add(strUserId, strUsercount);

    //    }
    //    FirstChargeNum = congkutable.Rows.Count;
    //    List<dt_report_inoutaccount> reportInAccount = new List<dt_report_inoutaccount>();
    //    for (int t = 0; t < reportInAccount.Count; t++)
    //    {
    //        //代理工资
    //        AgentWages += reportInAccount[t].Wages;
    //        //代理分红
    //        AgentDividends += reportInAccount[t].Bonus;
    //    }

    //    //注册人数

    //    DataSet Register = DBcon.DbHelperSQL.Query(string.Format("select count(id) from dt_users_class where identityid='{0}' and father_id={1} and addtime>='{2}' and addtime<='{3}'", IdentityId, UserId, StartTime, EndTime));
    //    if (Register.Tables.Count > 0)
    //    {
    //        if (Register.Tables[0].Rows[0][0] != DBNull.Value)
    //        {
    //            RegisterNum = Convert.ToInt32(Register.Tables[0].Rows[0][0]);
    //        }
    //    }

    //    TeamNum = ClassListss.Count;

    //    //团队余额

    //    List<SqlParameter> paras = new List<SqlParameter>()
    //    {
    //        new SqlParameter("@identityid",IdentityId),
    //        new SqlParameter("@fatherId",UserId),
    //        new SqlParameter("@userId",UserId)
    //    };
    //    DataSet TMoney = DBcon.DbHelperSQL.Query("select SUM(use_money) from dt_user_capital " +
    //        "where   flog=1 and ( user_id in(select user_id from dt_users_class" +
    //        " where identityid=@identityid and father_id=@fatherId) or user_id=@userId)");
    //    if (TMoney.Tables.Count > 0)
    //    {
    //        if (TMoney.Tables[0].Rows[0][0] != DBNull.Value)
    //        {
    //            TeamBalance = Convert.ToDecimal(TMoney.Tables[0].Rows[0][0]);
    //        }
    //    }
    //    //Log.Info("UserId:" + UserId + ",获取团队余额结束时间：" + DateTime.Now.ToString());
    //    #region 赋值
    //    AgencyHenders.BetMoney = BetMoney.ToString("#0.00");
    //    AgencyHenders.Bonus = Bonus.ToString("#0.00");
    //    AgencyHenders.RebateMoney = RebateMoney.ToString("#0.00");
    //    AgencyHenders.ActivityMoney = ActivityMoney.ToString("#0.00");
    //    AgencyHenders.RechargeMoney = RechargeMoney.ToString("#0.00");
    //    AgencyHenders.WithdrawMoney = WithdrawMoney.ToString("#0.00");
    //    AgencyHenders.BetNum = BetNum.ToString();
    //    AgencyHenders.FirstChargeNum = FirstChargeNum.ToString();
    //    AgencyHenders.TeamNum = TeamNum.ToString();
    //    AgencyHenders.RegisterNum = RegisterNum.ToString();
    //    AgencyHenders.TeamBalance = TeamBalance.ToString("#0.00");
    //    AgencyHenders.ProfitMoney = ProfitMoney.ToString("#0.00");
    //    AgencyHenders.AgentRebate = AgentRebate.ToString("#0.00");
    //    AgencyHenders.AgentWages = AgentWages.ToString("#0.00");
    //    AgencyHenders.AgentDividends = AgentDividends.ToString("#0.00");
    //    #endregion
    //    //是否有分红。没有传null
    //    //Log.Info("UserId:" + UserId + ",是否有分红。没有传null开始时间：" + DateTime.Now.ToString());
    //    DataTable BounsDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_bonus where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
    //    if (BounsDataTable.Rows.Count < 1)
    //    {
    //        AgencyHenders.AgentDividends = null;
    //    }
    //    //Log.Info("UserId:" + UserId + ",是否有分红。没有传null结束时间：" + DateTime.Now.ToString());
    //    //是否有工资。没有传null
    //    //Log.Info("UserId:" + UserId + ",是否有工资。没有传null开始时间：" + DateTime.Now.ToString());
    //    DataTable WageDataTable = DBcon.DbHelperSQL.Query(string.Format("select top 1 id from dt_agent_wageday where identityid='{0}' and UserId={1}", IdentityId, UserId)).Tables[0];
    //    if (WageDataTable.Rows.Count < 1)
    //    {
    //        AgencyHenders.AgentWages = null;
    //    }
    //    //Log.Info("UserId:" + UserId + ",是否有工资。没有传null结束时间：" + DateTime.Now.ToString());
    //    #endregion
    //    //}

    //    ResultInfo.Code = 1;
    //    ResultInfo.DataCount = 1;
    //    ResultInfo.BackData = AgencyHenders;
    //    string sss = LitJson.JsonMapper.ToJson(ResultInfo);
    //    //Log.Info("结束：" + sss);
    //    return sss;
    //}






    /// <summary>
    /// 获取会员的成長值
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="IdentityId"></param>

    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public string GetRechargeMoney(int UserId, string IdentityId)
    {
        string RechargeMoney = "0";
        Stopwatch st = new Stopwatch();
        LogMsg logmsg = new LogMsg();
        string para = "UserId=" + UserId + " IdentityId=" + IdentityId;
        try
        {
            st.Reset();
            st.Start();
            dt_month_all_record dar = new dt_month_all_record();
            ReportHelper reporthelper = new ReportHelper();

            //查询in表汇总
            List<SqlParameter> paras = new List<SqlParameter>();
            SqlParameter pa_identityid = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
            pa_identityid.Value = IdentityId;
            SqlParameter pa_userid = new SqlParameter("@UserId", SqlDbType.Int);
            pa_userid.Value = UserId;
            paras.Add(pa_identityid);
            paras.Add(pa_userid);

            int DBtype = SqlHelper.SwitchDBtype(IdentityId);

            string instr = "select  identityid,user_id,sum(In_AlipayPrepaid+In_ArtificialDeposit+In_BankPrepaid+In_FastPrepaid+In_WeChatPrepaid+In_QQ+In_FourthPrepaid+In_UnionpayPrepaid) AS czmony from [srv_lnk_total_in].[dafacloud].[dbo].[dt_user_in_historyRecord] where identityid=@IdentityId and user_id = @UserId  group by identityid,user_id ";
            DataTable intable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(instr, DBtype, "report_share", paras.ToArray());

            if(intable != null && intable.Rows.Count>0)
            {
                dt_user_summary dt_usersummary = new dt_user_summary();
                reporthelper.ConvertDataRowToModel(intable.Rows[0], dar);
                decimal fInMoney = dar.czmony;
                RechargeMoney = Math.Floor(fInMoney).ToString("#0");
                st.Stop();
                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("Before_GetRechargeMoney", "GetRechargeMoney", st.ElapsedMilliseconds.ToString(), para);
                }
            }
            else
            {
                RechargeMoney = "0";
            }
            

        }
        catch (Exception ex)
        {
            RechargeMoney = "0";
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetRechargeMoney", st.ElapsedMilliseconds.ToString(), para);
            }
        }
        return RechargeMoney;
    }
}
