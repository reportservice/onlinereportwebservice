﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBcon;

/// <summary>
///EncryptionSoapHeader 的摘要说明
/// </summary>
public class EncryptionSoapHeader : System.Web.Services.Protocols.SoapHeader
{
    /// <summary>
    /// webservice访问用户名  
    /// </summary>
    private string _userName = string.Empty;
    /// <summary>
    ///webservice访问用户名  
    /// </summary>
    public string UserName
    {
        get { return _userName; }
        set { _userName = value; }
    }
    /// <summary>
    /// webservice访问密码  
    /// </summary>
    private string _password = string.Empty;
    /// <summary>
    /// webservice访问密码  
    /// </summary>
    public string Password
    {
        get { return _password; }
        set { _password = value; }
    }
    /// <summary>
    /// 构造函数
    /// </summary>
    public EncryptionSoapHeader()
    {
        //  
        //TODO: 在此处添加构造函数逻辑  
        //  
    }
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="uname"></param>
    /// <param name="upass"></param>
    public EncryptionSoapHeader(string username, string password)
    {
        Init(username, password);
    }
    private void Init(string userName, string password)
    {
        this._password = password;
        this._userName = userName;
    }
    /// <summary>
    /// 验证用户是否有权访问内部接口  
    /// </summary>
    /// <param name="userName">账号</param>
    /// <param name="password">密码</param>
    /// <param name="msg">返回消息</param>
    /// <returns></returns>
    private bool IsValid(string userName, string password, out string msg)
    {
        msg = "";
        if (userName == DBcontring.reportUserName && password == DBcontring.reportPassword)
        {
            return true;
        }
        else
        {
            msg = "对不起！您无权调用此WebService！";
            return false;
        }
    }
    /// <summary>
    /// 验证用户是否有权访问外部接口 
    /// </summary>
    /// <param name="msg">返回消息</param>
    /// <returns></returns>
 
    public bool IsValid(out string msg)
    {
        return IsValid(this._userName, this._password, out msg);
    }
}