﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 代理报表xt
/// </summary>
public class AgentHender_New
{
    /// <summary>
    /// 投注额
    /// </summary>
    public decimal BetMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 奖金
    /// </summary>
    public decimal Bonus
    {
        get;
        set;
    }
    /// <summary>
    /// 返点金额
    /// </summary>
    public decimal RebateMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 活动金额
    /// </summary>
    public decimal ActivityMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 充值金额
    /// </summary>
    public decimal RechargeMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 提现金额
    /// </summary>
    public decimal WithdrawMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 投注人数
    /// </summary>
    public decimal BetNum
    {
        get;
        set;
    }
    /// <summary>
    /// 首充人数
    /// </summary>
    public decimal FirstChargeNum
    {
        get;
        set;
    }
    /// <summary>
    /// 注册人数
    /// </summary>
    public decimal RegisterNum
    {
        get;
        set;
    }
    /// <summary>
    /// 团队人数
    /// </summary>
    public decimal TeamNum
    {
        get;
        set;
    }
    /// <summary>
    /// 团队余额
    /// </summary>
    public decimal TeamBalance
    {
        get;
        set;
    }
    /// <summary>
    /// 盈利金额
    /// </summary>
    public decimal ProfitMoney
    {
        get;
        set;
    }
    /// <summary>
    /// 代理返点
    /// </summary>
    public decimal AgentRebate
    {
        get;
        set;
    }
    /// <summary>
    /// 代理工资
    /// </summary>
    public decimal AgentWages
    {
        get;
        set;
    }
    /// <summary>
    /// 代理工资
    /// </summary>
    public decimal AgentDividends
    {
        get;
        set;
    }
}