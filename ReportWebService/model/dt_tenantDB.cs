﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

/// <summary>
/// 代理报表实体类
/// </summary>
public class dt_tenantDB
{
    /// <summary>
    /// ID
    /// </summary>
    [Description("id")]
    public int id { get; set; }
    /// <summary>
    /// 站
    /// </summary>
    [Description("identityid")]
    public string identityid { get; set; }
    /// <summary>
    /// 分庫类型
    /// </summary>
    [Description("db")]
    public int DBtype { get; set; }
   
}