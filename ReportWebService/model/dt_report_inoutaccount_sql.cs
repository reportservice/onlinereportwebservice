﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/**************************************************************************

作者: zengzh

日期:2016-08-26

说明:会员出入款统计表

**************************************************************************/
namespace EasyFrame.NewCommon.Model
{
    /// <summary>
    /// 会员出入款统计表
    /// </summary>
    public class dt_report_inoutaccount_sql 
    {
        
        public virtual decimal TotalArtificialOut_Peo { get; set; }
        public virtual decimal TotalInMoney_Peo { get; set; }
        public virtual decimal TotalDiscount_Peo { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public virtual DateTime AddTime { get; set; }
        /// <summary>
        /// 上级代理ID
        /// </summary>
        public virtual int agent_id { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public virtual string group_name { get; set; }
        /// <summary>
        /// 主键ID
        /// </summary>
        public virtual int id { get; set; }
        /// <summary>
        /// 站长ID
        /// </summary>
        public virtual string identityid { get; set; }
        /// <summary>
        /// 支付宝充值
        /// </summary>
        public virtual decimal In_AlipayPrepaid { get; set; }
        public virtual decimal In_AlipayPrepaid_Peo { get; set; }
        /// <summary>
        /// 支付宝充值次數
        /// </summary>
        public virtual decimal In_AlipayPrepaid_Num { get; set; }
        /// <summary>
        /// 人工存款
        /// </summary>
        public virtual decimal In_ArtificialDeposit { get; set; }
        public virtual decimal In_ArtificialDeposit_Peo { get; set; }
        /// <summary>
        /// 人工存款次數
        /// </summary>
        public virtual decimal In_ArtificialDeposit_Num { get; set; }
        /// <summary>
        /// 银行充值
        /// </summary>
        public virtual decimal In_BankPrepaid { get; set; }
        public virtual decimal In_BankPrepaid_Peo { get; set; }
        /// <summary>
        /// 银行充值次數
        /// </summary>
        public virtual decimal In_BankPrepaid_Num { get; set; }
        /// <summary>
        /// 快捷充值
        /// </summary>
        public virtual decimal In_FastPrepaid { get; set; }
        public virtual decimal In_FastPrepaid_Peo { get; set; }
        /// <summary>
        /// 快捷充值次數
        /// </summary>
        public virtual decimal In_FastPrepaid_Num { get; set; }
        /// <summary>
        /// 微信充值
        /// </summary>
        public virtual decimal In_WeChatPrepaid { get; set; }
        public virtual decimal In_WeChatPrepaid_Peo { get; set; }
        /// <summary>
        /// 微信充值次數
        /// </summary>
        public virtual decimal In_WeChatPrepaid_Num { get; set; }
        /// <summary>
        /// 出款
        /// </summary>
        public virtual decimal Out_Account { get; set; }
        public virtual decimal Out_Account_Peo { get; set; }
        /// <summary>
        /// 出款筆數
        /// </summary>
        public virtual decimal Out_Account_Num { get; set; }
        public virtual decimal Out_Account_Num_Peo { get; set; }
        /// <summary>
        /// 活动优惠
        /// </summary>
        public virtual decimal Out_ActivityDiscountAccount { get; set; }
        public virtual decimal Out_ActivityDiscountAccount_Peo { get; set; }
        /// <summary>
        /// 行政提出
        /// </summary>
        public virtual decimal Out_AdministrationAccount { get; set; }
        public virtual decimal Out_AdministrationAccount_Peo { get; set; }
        /// <summary>
        /// 投注
        /// </summary>
        public virtual decimal Out_BettingAccount { get; set; }
        public virtual decimal Out_BettingAccount_Peo { get; set; }
        /// <summary>
        /// 投注筆數
        /// </summary>
        public virtual decimal Out_BettingAccount_Num { get; set; }
        public virtual decimal Out_BettingAccount_Num_Peo { get; set; }
        /// <summary>
        /// 撤单
        /// </summary>
        public virtual decimal Out_CancelAccount { get; set; }
        public virtual decimal Out_CancelAccount_Peo { get; set; }
        /// <summary>
        /// 误存提出
        /// </summary>
        public virtual decimal Out_MistakeAccount { get; set; }
        public virtual decimal Out_MistakeAccount_Peo { get; set; }
        /// <summary>
        /// 其他优惠
        /// </summary>
        public virtual decimal Out_OtherDiscountAccount { get; set; }
        public virtual decimal Out_OtherDiscountAccount_Peo { get; set; }
        /// <summary>
        /// 返点
        /// </summary>
        public virtual decimal Out_RebateAccount { get; set; }
        public virtual decimal Out_RebateAccount_Peo { get; set; }
        /// <summary>
        /// 拒绝出款
        /// </summary>
        public virtual decimal Out_RefuseAccount { get; set; }
        public virtual decimal Out_RefuseAccount_Peo { get; set; }
        /// <summary>
        /// 中奖
        /// </summary>
        public virtual decimal Out_WinningAccount { get; set; }
        public virtual decimal Out_WinningAccount_Peo { get; set; }
        /// <summary>
        /// 盈利
        /// </summary>
        public virtual decimal ProfitLoss { get; set; }
        /// <summary>
        /// 终端名称
        /// </summary>
        public virtual string SourceName { get; set; }
        /// <summary>
        /// 会员ID
        /// </summary>
        public virtual int user_id { get; set; }
        /// <summary>
        /// 羸率
        /// </summary>
        public virtual decimal WinRate { get; set; }
        /// <summary>
        /// 分红
        /// </summary>
        public virtual decimal Bonus { get; set; }
        public virtual decimal Bonus_Peo { get; set; }
        /// <summary>
        /// 工资
        /// </summary>
        public virtual decimal Wages { get; set; }
        public virtual decimal Wages_Peo { get; set; }
        /// <summary>
        /// 入款时间（除了快捷支付，其他充值方式都属于入款，提交订单到客服操作时间差的平均值，精确到天）
        /// </summary>
        public virtual double InMoneyTime { get; set; }
        /// <summary>
        /// 入款笔数
        /// </summary>
        public virtual double InMoneyNum { get; set; }
        public virtual double InMoneyNum_Peo { get; set; }
        /// <summary>
        /// 出款时间（提交订单到客服操作时间差的平均值，精确到天）
        /// </summary>
        public virtual double OutMoneyTime { get; set; }
        /// <summary>
        /// 出款时间（提交订单到客服操作时间差的平均值，精确到天）
        /// </summary>
        public virtual double OutMoneyNum { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
