﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///代理报表
/// </summary>
public class dt_user_in_sum
{
    public decimal czmony { get; set; }//充值金额
    public int cznumber { get; set; }//充值人数
    public decimal fastpaymoney { get; set; }//快捷支付
    public int fastnumber { get; set; } //快捷支付人数
    public decimal bankmony { get; set; }//银行转账金额
    public int banknumber { get; set; } //银行转账人数
    public decimal aliypaymony { get; set; }//支付宝金额
    public int aliypaynumber { get; set; } //支付宝人数
    public decimal wechatmony { get; set; }//微信支付
    public int wechatnumber { get; set; } //微信支付人数
    public decimal rgckmony { get; set; }//人工存款
    public int rgcknumber { get; set; } //人工存款人数
    public decimal rebatemoney { get; set; }//返点金额
    public int rebatenumber { get; set; } //返点人数
    public decimal hdljmony { get; set; }//活动礼金
    public int hdljnumber { get; set; } //活动礼金人数
    public decimal xthdmony { get; set; }//系统活动
    public int xthdnumber { get; set; } //系统活动人数
    public decimal qthdmony { get; set; }//其他活动
    public int qthdnumber { get; set; } //其他活动人数
    public decimal zjmoney { get; set; }//中奖金额
    public int zjnumber { get; set; } //中奖人数
    public decimal cdmony { get; set; }//撤单金额
    public int cdnumber { get; set; } //撤单人数
    public decimal dlgzmony { get; set; }//代理工资
    public int dlgznumber { get; set; } //代理工资人数
    public decimal dlfhmony { get; set; }//代理分红
    public int dlfhnumber { get; set; } //代理分红人数
    public decimal operattime { get; set; }//充值操作
    public decimal czcount { get; set; }//充值笔数
}