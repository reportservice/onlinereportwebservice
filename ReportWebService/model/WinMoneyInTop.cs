﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

/// <summary>
/// 投注总额排行榜
/// </summary>
public class WinMoneyInTop
{
    /// <summary>
    /// 会员ID
    /// </summary>
    [Description("user_id")]
    public int UserId { get; set; }
    /// <summary>
    /// 站长ID
    /// </summary>
    [Description("identityid")]
    public string IdentityId { get; set; }
    /// <summary>
    /// 投注总额
    /// </summary>
    [Description("Out_BettingAccount")]
    public decimal BetMoney { get; set; }
    /// <summary>
    /// 投注总额
    /// </summary>
    [Description("Out_WinningAccount")]
    public decimal WinMoney { get; set; }
    /// <summary>
    /// 盈利总额
    /// </summary>
    [Description("Out_ProfitLoss")]
    public decimal ProfitLoss { get; set; }
}