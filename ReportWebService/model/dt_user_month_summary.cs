﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/**************************************************************************

作者: zengzh

日期:2016-08-26

说明:会员出入款统计表

**************************************************************************/
namespace EasyFrame.NewCommon.Model
{
    /// <summary>
    /// 会员出入款统计表
    /// </summary>
    public class dt_user_month_summary
    {
        public decimal Wages { get; set; }
        public decimal Wages_Peo { get; set; }
        public decimal Bonus { get; set; }
        public decimal Bonus_Peo { get; set; }
        public decimal WinRate { get; set; }
        public int user_id { get; set; }
        public string SourceName { get; set; }
        public decimal ProfitLoss { get; set; }
        public decimal ProfitLoss_Peo { get; set; }
        public decimal Out_WinningAccount { get; set; }
        public decimal Out_WinningAccount_Peo { get; set; }
        public decimal Out_RefuseAccount { get; set; }
        public decimal Out_RefuseAccount_Peo { get; set; }
        public decimal Out_RebateAccount { get; set; }
        public decimal Out_RebateAccount_Peo { get; set; }
        public decimal Out_OtherDiscountAccount { get; set; }
        public decimal Out_OtherDiscountAccount_Peo { get; set; }
        public decimal Out_MistakeAccount { get; set; }
        public decimal Out_CancelAccount { get; set; }
        public decimal Out_BettingAccount { get; set; }
        public decimal Out_BettingAccount_Peo { get; set; }
        public decimal Out_AdministrationAccount { get; set; }
        public decimal Out_AdministrationAccount_Peo { get; set; }
        public decimal Out_ActivityDiscountAccount { get; set; }
        public decimal Out_ActivityDiscountAccount_Peo { get; set; }
        public decimal Out_Account { get; set; }
        public decimal In_WeChatPrepaid { get; set; }
        public decimal In_FastPrepaid { get; set; }
        public decimal In_BankPrepaid { get; set; }
        public decimal In_ArtificialDeposit { get; set; }
        public decimal In_AlipayPrepaid { get; set; }
        public string identityid { get; set; }
        public int id { get; set; }
        public int Grade { get; set; }
        public int agent_id { get; set; }
        public string YearMonth { get; set; }
        public DateTime AddTime { get; set; }
        public double InMoneyTime { get; set; }
        public double OutMoneyTime { get; set; }
    }
}
