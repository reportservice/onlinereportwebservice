﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

/// <summary>
///LiveAfterWebService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
// [System.Web.Script.Services.ScriptService]
public class LiveAfterWebService : System.Web.Services.WebService
{
    public EncryptionSoapHeader encryptionSoapHeader = new EncryptionSoapHeader();
    [WebMethod]
    public string HelloWorld()
    {

        return "Hello World";
    }
    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public return_anchor_tip_record getAnchorReport(string anchorId, int PageSize, int PageIndex)

    {

        string para = "anchorId=" + anchorId + " PageSize=" + PageSize + " PageIndex=" + PageIndex;
        LogMsg logmsg = new LogMsg();
        ObjectCache cache = MemoryCache.Default;
        string filecontents = anchorId + PageSize + PageIndex;
        Stopwatch st = new Stopwatch();
        try
        {
            st.Reset();
            st.Start();
            //会员
            return_anchor_tip_record strResult = cache[filecontents] as return_anchor_tip_record;

            if (strResult == null)
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(30);//只讓快取存活多久時間

                dt_anchor_tip_record TipModel = new dt_anchor_tip_record();
                List<dt_anchor_tip_record> TipModelList = new List<dt_anchor_tip_record>();
                string tipToday = DateTime.Now.ToString("yyyy-MM-dd");
                string tipTomorrow = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                string tipThisMonth = DateTime.Now.ToString("yyyy-MM-01");
                string tipLastMonth = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01");

                //string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
                string outstr = "";
                string whereQuery = "";
                List<SqlParameter> sqlcmdlist = new List<SqlParameter>();
                sqlcmdlist.Add(new SqlParameter("@tipToday", tipToday));
                sqlcmdlist.Add(new SqlParameter("@tipTomorrow", tipTomorrow));
                sqlcmdlist.Add(new SqlParameter("@tipThisMonth", tipThisMonth));
                sqlcmdlist.Add(new SqlParameter("@tipLastMonth", tipLastMonth));
                if (!string.IsNullOrEmpty(anchorId))
                {
                    sqlcmdlist.Add(new SqlParameter("@anchorid", anchorId));
                    whereQuery = " where anchor_id = @anchorid and";
                }
                else
                {
                    whereQuery = " where ";
                }
                DataTable outtable = null;
                SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
                ReportHelper reporthelper = new ReportHelper();

                System.Threading.Tasks.Task out_task = new System.Threading.Tasks.Task(() =>
                {
                    //total
                    outstr = "select anchor_id,sum(tipmoney_total) as tipmoney_total from  (select anchor_id,sum(dsmoney) as tipmoney_total from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date< @tipTomorrow group by anchor_id union select anchor_id, sum(dsmoney) as tipmoney_total from[srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord  " + whereQuery + " add_date<@tipTomorrow group by anchor_id )   as v1 group by anchor_id";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
                    lock (TipModel)
                        reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable, TipModel, TipModelList);
                    //lastMonth
                    outstr = "select anchor_id,sum(tipmoney_lastmonth) as tipmoney_lastmonth from  (select anchor_id,sum(dsmoney) as tipmoney_lastmonth from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord  " + whereQuery + " add_date< @tipThisMonth and add_date>=@tipLastMonth group by anchor_id union select anchor_id, sum(dsmoney) as tipmoney_lastmonth from[srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord  " + whereQuery + " add_date< @tipThisMonth and add_date>=@tipLastMonth group by anchor_id )   as v1 group by anchor_id";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
                    lock (TipModel)
                    {
                        for (int i = 0; i < outtable.Rows.Count; i++)
                        {
                            int sAnchorId = Convert.ToInt32(outtable.Rows[i]["anchor_id"]);
                            var mm = TipModelList.Where(o => o.anchor_id == sAnchorId).FirstOrDefault();
                            if (mm != null)
                                reporthelper.ConvertToMode(outtable.Rows[i], mm);
                            else
                                reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable.Rows[i], TipModel, TipModelList);
                        }
                    }
                    //thisMonth
                    outstr = "select anchor_id,sum(tipmoney_thismonth) as tipmoney_thismonth from  (select anchor_id,sum(dsmoney) as tipmoney_thismonth from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + "  add_date>=@tipThisMonth group by anchor_id union select anchor_id,sum(dsmoney) as tipmoney_thismonth from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + "  add_date>=@tipThisMonth group by anchor_id )   as v1 group by anchor_id";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
                    lock (TipModel)
                    {
                        for (int i = 0; i < outtable.Rows.Count; i++)
                        {
                            int sAnchorId = Convert.ToInt32(outtable.Rows[i]["anchor_id"]);
                            var mm = TipModelList.Where(o => o.anchor_id == sAnchorId).FirstOrDefault();
                            if (mm != null)
                                reporthelper.ConvertToMode(outtable.Rows[i], mm);
                            else
                                reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable.Rows[i], TipModel, TipModelList);
                        }
                    }
                    //today
                    outstr = "select anchor_id,sum(tipmoney_today) as tipmoney_today from  (select anchor_id,sum(dsmoney) as tipmoney_today from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date=@tipToday group by anchor_id union select anchor_id,sum(dsmoney) as tipmoney_today from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date=@tipToday group by anchor_id )   as v1 group by anchor_id";
                    outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
                    lock (TipModel)
                    {
                        for (int i = 0; i < outtable.Rows.Count; i++)
                        {
                            int sAnchorId = Convert.ToInt32(outtable.Rows[i]["anchor_id"]);
                            var mm = TipModelList.Where(o => o.anchor_id == sAnchorId).FirstOrDefault();
                            if (mm != null)
                                reporthelper.ConvertToMode(outtable.Rows[i], mm);
                            else
                                reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable.Rows[i], TipModel, TipModelList);
                        }
                    }
                });
                out_task.Start();
                out_task.Wait();
                int anchorCount = TipModelList.Count;
                List<dt_anchor_tip_record> sortList = TipModelList.OrderBy(o => o.anchor_id).Skip(PageSize * PageIndex).Take(PageSize).ToList();

                strResult = new return_anchor_tip_record()
                {
                    anchor_list = sortList,
                    anchor_count = anchorCount,
                    code = 1
                };

                lock (logmsg)
                {
                    logmsg.CreateErrorLogTxt("After_GetAnchorReporta", "GetAnchorReport", st.ElapsedMilliseconds.ToString(), para);
                }
                cache.Set(filecontents, strResult, policy);
                return strResult;
            }
            else
            {
                st.Stop();
                lock (logmsg)
                {
                    if (st.ElapsedMilliseconds > 1800)
                        logmsg.CreateErrorLogTxt("After_GetAnchorReporta_cache", "GetAnchorReport", st.ElapsedMilliseconds.ToString(), para);
                }
                return strResult;
            }
        }
        catch (Exception ex)
        {
            st.Stop();
            lock (logmsg)
            {
                para += " \r\n錯誤訊息:" + ex;
                logmsg.CreateErrorLogTxt("Before_Error", "GetAnchorReport", st.ElapsedMilliseconds.ToString(), para);
            }
            return_anchor_tip_record catchModel = new return_anchor_tip_record();
            return catchModel;
        }





    }



    [WebMethod]
    [SoapHeader("encryptionSoapHeader")]
    public List<dt_anchor_tip_record> getAnchorInfo(string IdentityId, string anchorId, int PageSize, int PageIndex)
    {
        dt_anchor_tip_record TipModel = new dt_anchor_tip_record();
        List<dt_anchor_tip_record> TipModelList = new List<dt_anchor_tip_record>();
        string tipToday = DateTime.Now.ToString("yyyy-MM-dd");
        string tipTomorrow = DateTime.Now.AddHours(1).ToString("yyyy-MM-dd");
        string tipThisMonth = DateTime.Now.ToString("yyyy-MM-01");
        string tipLastMonth = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01");

        //string report_out_conn = DBcon.DBcontring.Report_Out_Conn;
        string outstr = "";
        string whereQuery = "";
        List<SqlParameter> sqlcmdlist = new List<SqlParameter>();

        sqlcmdlist.Add(new SqlParameter("@tipToday", tipToday));
        sqlcmdlist.Add(new SqlParameter("@tipTomorrow", tipTomorrow));
        sqlcmdlist.Add(new SqlParameter("@tipThisMonth", tipThisMonth));
        sqlcmdlist.Add(new SqlParameter("@tipLastMonth", tipLastMonth));
        if (!string.IsNullOrEmpty(IdentityId) && !string.IsNullOrEmpty(anchorId))
        {
            sqlcmdlist.Add(new SqlParameter("@identityid", IdentityId));
            sqlcmdlist.Add(new SqlParameter("@anchorid", anchorId));
            whereQuery = " where identityid = @identityid and anchor_id = @anchorid and";
        }
        else if (string.IsNullOrEmpty(IdentityId) && !string.IsNullOrEmpty(anchorId))
        {
            sqlcmdlist.Add(new SqlParameter("@anchorid", anchorId));
            whereQuery = " where anchor_id = @anchorid and";
        }
        else if (!string.IsNullOrEmpty(IdentityId) && string.IsNullOrEmpty(anchorId))
        {
            sqlcmdlist.Add(new SqlParameter("@identityid", IdentityId));
            whereQuery = " where identityid = @identityid and";
        }
        else
        {
            whereQuery = " where ";
        }
        DataTable outtable = null;
        SqlParameter[] sqlcmdpar = sqlcmdlist.ToArray();
        ReportHelper reporthelper = new ReportHelper();

        System.Threading.Tasks.Task out_task = new System.Threading.Tasks.Task(() =>
        {

            if (whereQuery != " where ")
            {
                outstr = "select identityid,anchor_id,sum(tipmoney_total) as tipmoney_total from (select identityid,anchor_id,sum(dsmoney) as tipmoney_total from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date< @tipTomorrow group by identityid,anchor_id union select identityid,anchor_id,sum(dsmoney) as tipmoney_total from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date< @tipTomorrow group by identityid,anchor_id ) as v1 group by identityid,anchor_id";
            }
            else
            {
                outstr = "select identityid,anchor_id,sum(tipmoney_total) as tipmoney_total from (select identityid,anchor_id,sum(dsmoney) as tipmoney_total from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord  group by identityid,anchor_id union select identityid,anchor_id,sum(dsmoney) as tipmoney_total from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord  group by identityid,anchor_id) as v1 group by identityid,anchor_id";
            }
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);

            lock (TipModel)
                reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable, TipModel, TipModelList);


            //lastMonth
            outstr = "select identityid,anchor_id,sum(tipmoney_lastmonth) as tipmoney_lastmonth from(select identityid,anchor_id,sum(dsmoney) as tipmoney_lastmonth from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord  " + whereQuery + " add_date< @tipThisMonth and add_date>=@tipLastMonth group by identityid,anchor_id union select identityid,anchor_id,sum(dsmoney) as tipmoney_lastmonth from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord  " + whereQuery + " add_date< @tipThisMonth and add_date>=@tipLastMonth group by identityid,anchor_id) as v1 group by identityid,anchor_id";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
            lock (TipModel)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    int sAnchorId = Convert.ToInt32(outtable.Rows[i]["anchor_id"]);
                    string sidentityid = outtable.Rows[i]["identityid"].ToString();
                    var mm = TipModelList.Where(o => o.identityid == sidentityid && o.anchor_id == sAnchorId).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable.Rows[i], TipModel, TipModelList);
                }
            }

            //thisMonth
            outstr = "select identityid,anchor_id,sum(tipmoney_thismonth) as tipmoney_thismonth from(select identityid,anchor_id,sum(dsmoney) as tipmoney_thismonth from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + "  add_date>=@tipThisMonth group by identityid,anchor_id union select identityid,anchor_id,sum(dsmoney) as tipmoney_thismonth from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + "  add_date>=@tipThisMonth group by identityid,anchor_id) as v1 group by identityid,anchor_id";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
            lock (TipModel)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    int sAnchorId = Convert.ToInt32(outtable.Rows[i]["anchor_id"]);
                    string sidentityid = outtable.Rows[i]["identityid"].ToString();
                    var mm = TipModelList.Where(o => o.identityid == sidentityid && o.anchor_id == sAnchorId).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable.Rows[i], TipModel, TipModelList);
                }
            }
            //today
            outstr = "select identityid,anchor_id,sum(tipmoney_today) as tipmoney_today from(select identityid,anchor_id,sum(dsmoney) as tipmoney_today from [srv_lnk_total_out].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date=@tipToday group by identityid,anchor_id union select identityid,anchor_id,sum(dsmoney) as tipmoney_today from [srv_lnk_total_out2].[dafacloud].[dbo].dt_reward_anchorrecord " + whereQuery + " add_date=@tipToday group by identityid,anchor_id) as v1 group by identityid,anchor_id";
            outtable = DBcon.DbHelperSQL.GetQueryFromReportSwitchbei(outstr, 1, "report_share", sqlcmdpar);
            lock (TipModel)
            {
                for (int i = 0; i < outtable.Rows.Count; i++)
                {
                    int sAnchorId = Convert.ToInt32(outtable.Rows[i]["anchor_id"]);
                    string sidentityid = outtable.Rows[i]["identityid"].ToString();
                    var mm = TipModelList.Where(o => o.identityid == sidentityid && o.anchor_id == sAnchorId).FirstOrDefault();
                    if (mm != null)
                        reporthelper.ConvertToMode(outtable.Rows[i], mm);
                    else
                        reporthelper.ConvertToMode<dt_anchor_tip_record>(outtable.Rows[i], TipModel, TipModelList);
                }
            }


        });
        out_task.Start();
        out_task.Wait();


        return TipModelList;
    }
}

